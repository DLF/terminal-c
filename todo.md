# ToDo's
- [x] add connector cutouts to wall
- [x] re-locate the wall-screws
- [x] create switch-plate spacer element
- [x] add cutout for reset switch
- [x] round outer corners of wall footprint
- [ ] add increased outer thumb-wall-diameter to outer wall footprint to get the screw out of the way of the switch
- [ ] add more wall to inner thumb area to get screws out of the way for the thumb-row-2 switches and to have enough space for the top plate
- [ ] add outer cylinder to jack cutout

## Before Production
- [ ] check switch width again when done with 3d-printed prototypes / restore to 14mm
- [ ] big_switch_plate_2d(self) shall use the non-extended screw diameter in the final version


# Notes

## Pins
### Valuable for extension
* 1 & 0 (PD2 & PD3): TX and RX and provides interrupts; PD2 is mandatory for USART PS/2 support
* 2 & 3 (PD1 & PD0): SDA & SCL (I2C), e.g. for display
* PD5 is mandatory for PS/2 support but not available as pinout; one of the LEDs is connected to it
* 5 (PC5): PWM and separate port power
* 10 (PB6): PCINT and an ADC

### Candidates for Std. Usage
* 6 columns: 21, 20, 19, 18, 15, 14
* 4 + 1 rows: 8, 7, 6, 4 + 5 
* LEDs: 16
* free: 10, 9, 2, 3, 1, 0

Pinned out: free ones (6) + 5th row + 6 columns + GND + LED continuation

### Currently free and fine
* PS/2 via Interrupt: needs one INT or PCINT and one of any pin.
  At least two PCINT's are free (PB5 and PB6) and several INTs (PD2, PD1, PD0)
* PS/2 via USART if we connect to the Promics LED pin for PD5. PD2 is also needed and free.
* I2C (e.g. Display or Pimoroni trackball): PD0 & PD1

### Pin Count
* Total: 18
* needed for std.: 11
* 7 remain, plus GND and VCC makes 9 for the extensions; maybe let's use only 6 + 2 for an even nuber and reserve one for the "extra row"

## Snippets
```
edge = pcbnew.DRAWSEGMENT(board)
board.Add(edge)
edge.SetStart(pcbnew.wxPoint(arrayCenter.x - arrayWidth/2 - HORIZONTAL_EDGE_RAIL_WIDTH*SCALE, arrayCenter.y - arrayHeight/2 - VERTICAL_EDGE_RAIL_WIDTH*SCALE))
edge.SetEnd(pcbnew.wxPoint(arrayCenter.x + arrayWidth/2 + HORIZONTAL_EDGE_RAIL_WIDTH*SCALE, arrayCenter.y - arrayHeight/2 - VERTICAL_EDGE_RAIL_WIDTH*SCALE))
edge.SetLayer(layertable["Edge.Cuts"])
```

## Possible 2d libs
* https://shapely.readthedocs.io/en/latest/manual.html
* https://www.sympy.org/en/index.html

## Thumb Geometry Comparison

             | r1 | r2 | Δα | α0  |  p_peak |
-------------|----|----|----|-----|---------| 
 term-b beta | 62 | 40 | 18 | -5.5|         | 
 term-b 1.0  | 72 | 50 | 16 | -7  |         | 
 term-c t1   | 67 | 45 | 17 | -7  | (90, 58)|
 term-c t2   |    |    |    |     | (93, 56)| 
