import os
import pathlib
import sys

try:
    import terminal_c
except ImportError:
    this_path = pathlib.Path(__file__).parent.resolve()
    pylib_path = pathlib.Path(os.path.join(this_path, "lib")).resolve()
    sys.path.append(str(pylib_path))

from terminal_c import schematic

from terminal_c.configuration.default import Configuration

c = Configuration()

schematic.build_schematics(c)
