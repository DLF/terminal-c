from solid import (
    polygon,
    square,
    circle,
    translate,
    rotate,
    mirror,
    linear_extrude,
    offset,
    color,
    intersection,
    cube,
    cylinder,
    OpenSCADObject,
    scad_render_to_file,
)

from solid.utils import (
    down,
    up,
    back,
    forward,
    left,
    right
)

from terminal_c import (
    MagicNumbers as m,
    geometry,
    Polygon,
    Vector,
)

from terminal_c.geometry import SwitchDiodeArrangement

from terminal_c import scad_part_models

wall_width = 1.2


def factor_polygon(p: Polygon) -> polygon:
    return polygon(
        points=[v.as_tuple for v in p.points],
    )


class Board:
    def __init__(self, board: geometry.Board):
        self.c = board.c
        self._board = board

        c = self.c.SCAD

        self.pcb_to_switch_plate_height = (
                c.Housing.pcb_thickness
                +
                c.Housing.pcb_switch_plate_distance
                +
                c.Housing.switch_plate_thickness
        )

        self.bottom_to_switch_plate_height = (
            c.Housing.bottom_plate_thickness
            +
            c.Housing.pcb_bottom_plate_distance
            +
            self.pcb_to_switch_plate_height
        )

        self.space_between_bottom_and_switch_plate = (
            c.Housing.pcb_thickness
            +
            c.Housing.pcb_switch_plate_distance
            +
            c.Housing.pcb_bottom_plate_distance
        )

        self.z_level_pcb_front = (
            c.Housing.bottom_plate_thickness
            +
            c.Housing.pcb_bottom_plate_distance
            +
            c.Housing.pcb_thickness
        )

        print("Bottom-Switch-plate-spacing: {}".format(self.space_between_bottom_and_switch_plate))
        print("Bottom-Switch-plate-thickness: {}".format(self.bottom_to_switch_plate_height))
        print("PCB-Switch-plate-thickness: {}".format(self.pcb_to_switch_plate_height))
        print("Nut needs 1.55 mm".format(self.bottom_to_switch_plate_height))

    @property
    def controller_section_cutout_2d(self):
        dim = Vector.from_tuple(m.SCAD.promic_cutout_dimensions)
        return (
            translate(
                (self._board.left_board.controler_section.promic.pos - dim / 2).as_tuple
            )(
                square(dim.as_tuple)
            )
        )

    @property
    def switch_screw_positions(self) -> list[tuple]:
        return [c.pos.as_tuple for c in self._board.left_holes.switch_field_screws]

    @property
    def controller_screw_positions(self) -> list[tuple]:
        return [c.pos.as_tuple for c in self._board.left_holes.controller_field_screws]

    @property
    def switch_field_screw_holes(self) -> OpenSCADObject:
        return sum(
            [
                translate(pos)(
                    circle(d=self.c.SCAD.LayoutTest.screw_hole_diameter, segments=60)
                )
                for pos in self.switch_screw_positions
            ]
        )

    @property
    def _controller_field_screw_holes(self):
        return sum(
            [
                translate(pos)(
                    circle(d=self.c.SCAD.LayoutTest.screw_hole_diameter, segments=60)
                )
                for pos in self.controller_screw_positions
            ]
        )

    @property
    def pcb_polygon_2d(self) -> OpenSCADObject:
        return polygon(
            points=[v.as_tuple for v in self._board.left_board.pcb_outline],
        )

    @property
    def wall_outline_2d(self) -> OpenSCADObject:
        c = self.c.SCAD.Housing
        return (
            offset(r=c.border_rounding_radius, segments=30)(
                offset(delta=c.border_width - c.border_rounding_radius)(
                    self.pcb_polygon_2d
                )
            )
        )
    
    @property
    def switch_cells(self) -> list[SwitchDiodeArrangement]:
        result: list[SwitchDiodeArrangement] = []
        result.extend([cell for col in self._board.left_board.finger.columns for cell in col])
        result.extend(self._board.left_board.thumb.cells)
        return result

    @property
    def _switch_holes(self) -> OpenSCADObject:
        return sum(
            [
                translate(cell.position.as_tuple)(
                    square(size=self.c.SCAD.LayoutTest.switch_hole_width, center=True)
                )
                for col in self._board.left_board.finger.columns for cell in col
            ]
            +
            [
                translate(cell.position.as_tuple)(
                    rotate(cell.angle)(
                        square(size=self.c.SCAD.LayoutTest.switch_hole_width, center=True)
                    )
                ) for cell in self._board.left_board.thumb.cells
            ]
        )

    @property
    def wall_shape(self):
        reduced_outline = offset(delta=-wall_width)(
            self.pcb_polygon_2d
        )
        return self.pcb_polygon_2d - reduced_outline

    @property
    def wall_screw_positions(self) -> list[Vector]:
        o = self._board.left_board.pcb_outline
        d = self.c.SCAD.Housing.screw_distance
        usb = self.usb_plug_entry_point[:2]
        jack = Vector.from_tuple(self.jack_entry_point[:2])
        return [
            o[0] + Vector(0, -d),
            o[3] + Vector(-d, -(o[4] - o[3]).length / 1.5),
            o[6] + (o[7] - o[6])/2.0 + Vector(0, -d),
            o[10] + Vector(+d * 1.2, -d),
            o[11] + Vector(0, -d),
            jack + Vector(d, 7),
            o[13] + Vector(0, -d).rotate(self._board.left_board.thumb.cells[-1].angle),
            o[16] + Vector(0, d).rotate(self._board.left_board.thumb.cells[1].angle),
            o[19] + Vector(0, d).rotate(self._board.left_board.thumb.cells[0].angle),
            o[24] + ((o[23]-o[24])/2) + Vector(-d, 0).rotate((o[23]-o[24]).angle),
            o[25] + ((o[26]-o[25])/1) + Vector(d, 0).rotate((o[23]-o[24]).angle),
            o[30] + Vector(0, +d),
            ]
        #return [
        #    o[0] + Vector(0, -d),
        #    o[3] + Vector(-d, -(o[4] - o[3]).length / 2),
        #    o[6] + Vector(-d, +d),
        #    o[7] + Vector(+d, +d),
        #    o[10] + Vector(+d, -d),
        #    o[11] + Vector(0, -d),
        #    o[13] + Vector(0, -d).rotate(self._board.left_board.thumb.cells[-1].angle),
        #    o[16] + Vector(0, d).rotate(self._board.left_board.thumb.cells[1].angle),
        #    o[19] + Vector(0, d).rotate(self._board.left_board.thumb.cells[0].angle),
        #    o[24] + ((o[23]-o[24])/2) + Vector(-d, 0).rotate((o[23]-o[24]).angle),
        #    o[25] + ((o[26]-o[25])/2) + Vector(d, 0).rotate((o[23]-o[24]).angle),
        #    o[27] + Vector(+d, +d),
        #    o[30] + Vector(0, +d),
        #]

    def get_wall_screws_holes(self, extended_diameter: bool = False) -> OpenSCADObject:
        diameter = (
            self.c.SCAD.Housing.screw_hole_diameter_extended
            if extended_diameter else
            self.c.SCAD.Housing.screw_hole_diameter
        )
        return sum([
            translate(p.as_tuple)(
                circle(d=diameter, segments=self.c.SCAD.Housing.screw_hole_segments)
            )
            for p in self.wall_screw_positions
        ])

    @property
    def switch_plate_controller_screw_nose_2d(self) -> OpenSCADObject:
        screw = self.controller_screw_positions[2]
        return translate(screw)(
            circle(r=2.7, segments=20)
            +
            back(2.7)(
                left(10)(square((10, 2.7 * 2)))
            )
        )

    @property
    def big_switch_plate_2d(self) -> OpenSCADObject:
        return (
            self.wall_outline_2d
            -
            self.get_wall_screws_holes(extended_diameter=True)
            -
            self.get_controller_section_cover_area_2d(inner_overlap=True)
            +
            self.switch_plate_controller_screw_nose_2d
            -
            self._switch_holes
            -
            self.switch_field_screw_holes
            -
            self._controller_field_screw_holes
        )

    @property
    def small_switch_plate_2d(self) -> OpenSCADObject:
        return (
            self.pcb_polygon_2d
            -
            self._switch_holes
            -
            self.switch_field_screw_holes
            -
            self._controller_field_screw_holes
        )

    @property
    def small_bottom_plate_2d(self) -> OpenSCADObject:
        return (
                self.pcb_polygon_2d
                -
                self.switch_field_screw_holes
                -
                self._controller_field_screw_holes
        )

    @property
    def reset_switch_cutout_2d(self):
        return translate(self._board.left_board.controler_section.reset_switch.pos.as_tuple)(
            square(
                (self.c.SCAD.Housing.reset_switch_cutout_width, self.c.SCAD.Housing.reset_switch_cutout_width),
                center=True
            )
        )

    @property
    def big_bottom_plate_2d(self) -> OpenSCADObject:
        return (
                self.wall_outline_2d
                -
                self.get_wall_screws_holes()
                -
                self.reset_switch_cutout_2d
        )

    @property
    def pcb_2d(self) -> OpenSCADObject:
        return (
            self.pcb_polygon_2d
            -
            self.switch_field_screw_holes
            -
            self._controller_field_screw_holes
            -
            translate(self._board.left_board.controler_section.encoder.pos.as_tuple)(circle(d=7.3, segments=60))
        )

    @property
    def small_bottom_plate_3d(self) -> OpenSCADObject:
        return linear_extrude(self.c.SCAD.Housing.bottom_plate_thickness)(
            self.small_bottom_plate_2d
        )

    @property
    def big_bottom_plate_3d(self) -> OpenSCADObject:
        return linear_extrude(self.c.SCAD.Housing.bottom_plate_thickness)(
            self.big_bottom_plate_2d
        )

    def get_controller_section_cover_area_2d(self, inner_overlap: bool) -> OpenSCADObject:
        x_1 = (
            self._board.left_board.controler_section.promic.pos.x - m.SCAD.promic_cutout_dimensions[0] / 2
            if inner_overlap else
            self._board.left_board.finger.columns[-1][0].position.x + m.switch_width / 2
        )
        y_1 = self._board.left_board.inner_upper_corner.y - self.c.SCAD.Housing.border_width
        x_2 = self._board.left_board.inner_upper_corner.x + self.c.SCAD.Housing.border_width
        y_2 = self._board.left_board.controler_section.encoder.pos.y + 20

        d_x = x_2 - x_1
        d_y = y_2 - y_1

        circle_center = self._board.left_board.thumb.get_radial_point(0, 0)
        circle_radius = self.c.SCAD.Housing.controller_cover_cutout_radius

        if inner_overlap:
            circle_radius += self.c.SCAD.Housing.controller_cover_overlap
            y_2 = self.controller_screw_positions[1][1] - 3
            d_y = y_2 - y_1

        return (
            translate((x_1, y_1))(
                square((d_x, d_y))
            )
            -
            translate(circle_center.as_tuple)(
                circle(r=circle_radius, segments=200)
            )
        )

    @property
    def top_plate_2d(self) -> OpenSCADObject:
        return (
            self.wall_outline_2d
            -
            offset(delta=-4)(
                self.wall_outline_2d
            )
            -
            self.get_wall_screws_holes()
        )

    @property
    def top_plate_3d(self) -> OpenSCADObject:
        return linear_extrude(self.c.SCAD.Housing.top_plate_thickness)(
            self.top_plate_2d
        )

    @property
    def controller_cover_plate_2d(self):
        return (
            self.get_controller_section_cover_area_2d(inner_overlap=False)
            -
            self.get_wall_screws_holes()
        )

    @property
    def controller_cover_plate_3d(self) -> OpenSCADObject:
        return linear_extrude(self.c.SCAD.Housing.cover_plate_thickness)(
            self.controller_cover_plate_2d
        )

    @property
    def usb_plug_entry_point(self) -> tuple[float, float, float]:
        return (
            self._board.left_board.controler_section.promic.pos.x,
            self._board.left_board.inner_upper_corner.y,
            self.z_level_pcb_front
        )

    @property
    def usb_plug(self) -> OpenSCADObject:
        return (
            translate(self.usb_plug_entry_point)(
                scad_part_models.USBPlugCutout()()
            )
        )

    @property
    def jack_entry_point(self) -> tuple[float, float, float]:
        p = self._board.left_board.controler_section.jack.pos
        return (
            p.x,
            p.y,
            self.z_level_pcb_front
        )

    @property
    def jack(self) -> OpenSCADObject:
        return (
            translate(self.jack_entry_point)(
                scad_part_models.JackCutoutUpwardsOpen()()
            )
        )

    @property
    def wall_segment_bottom(self) -> OpenSCADObject:
        wall_2d = (
            self.wall_outline_2d
            -
            offset(delta=self.c.SCAD.Housing.wall_to_pcb_clearance)(
                self.pcb_polygon_2d
            )
            -
            self.get_wall_screws_holes()
        )
        return (
            up(self.c.SCAD.Housing.bottom_plate_thickness)(
                linear_extrude(self.space_between_bottom_and_switch_plate)(
                    wall_2d
                )
                +
                up(self.space_between_bottom_and_switch_plate)(
                    linear_extrude(self.c.SCAD.Housing.switch_plate_thickness)(
                        intersection()(
                            wall_2d,
                            self.get_controller_section_cover_area_2d(inner_overlap=True)
                        )
                    )
                )
            )
            -
            self.usb_plug
            -
            self.jack
        )

    @property
    def switch_plate_3d(self) -> OpenSCADObject:
        return color("grey")(linear_extrude(self.c.SCAD.Housing.switch_plate_thickness)(
            self.big_switch_plate_2d
        ))

    @property
    def switch_plate_spacer(self) -> OpenSCADObject:
        c = self.c.SCAD.Housing
        height = c.pcb_switch_plate_distance
        feet_height = c.spacer_feet_height
        switch_blocks = sum([
            translate(cell.position.as_tuple)(
                rotate(cell.angle)(
                    square((25, 25), center=True)
                )
            )
            for cell in self.switch_cells
        ])
        switch_holes = sum([
            translate(cell.position.as_tuple)(
                rotate(cell.angle)(
                    square((15, 16.5), center=True)
                )
            )
            for cell in self.switch_cells
        ])
        screw_holes = sum([
            translate(pos)(
                circle(d=c.screw_hole_diameter_extended, segments=50)
            )
            for pos in self.switch_screw_positions + [self.controller_screw_positions[2]]
        ])
        spacer_feed = sum([
            translate(pos)(
                circle(d=4.4, segments=50)
            )
            for pos in self.switch_screw_positions + [self.controller_screw_positions[2]]
        ])
        return (
            up(feet_height)(
                linear_extrude(height - feet_height)(
                    intersection()(
                        switch_blocks
                        -
                        self.switch_field_screw_holes
                        -
                        self.get_controller_section_cover_area_2d(inner_overlap=False)
                        +
                        self.switch_plate_controller_screw_nose_2d
                        -
                        switch_holes
                        -
                        screw_holes
                        ,
                        offset(delta=-0.5)(self.pcb_polygon_2d)
                    )
                )
            )
            +
            linear_extrude(feet_height)(
                spacer_feed
                -
                screw_holes
            )
        )

    @property
    def model(self) -> OpenSCADObject:
        c = self.c.SCAD
        return (
            # color("green")(
            #     linear_extrude(14)(
            #         self.get_controller_section_cover_area_2d(True)
            #     )
            # )
            # +
            # color("pink")(
            #     linear_extrude(15)(
            #         self.controller_section_cutout_2d
            #     )
            # )
            # +

            color("DodgerBlue")(
                self.big_bottom_plate_3d
            )
            +
            self.wall_segment_bottom
            +
            up(c.Housing.bottom_plate_thickness + c.Housing.pcb_bottom_plate_distance)(
                color("orange")(
                    linear_extrude(c.Housing.pcb_thickness)(
                        self.pcb_2d
                    )
                )
                +
                up(c.Housing.pcb_thickness)(
                    #color("red")(
                    #    sum(
                    #        [
                    #            translate(cell.position.as_tuple)(
                    #                scad_part_models.Switch()()
                    #            )
                    #            for col in self._board.left_board.finger.columns for cell in col
                    #        ]
                    #        +
                    #        [
                    #            translate(cell.position.as_tuple)(
                    #                rotate(cell.angle)(
                    #                    scad_part_models.Switch()()
                    #                )
                    #            ) for cell in self._board.left_board.thumb.cells
                    #        ]
                    #    )
                    #)
                    #+
                    translate(self._board.left_board.controler_section.promic.pos.as_tuple)(
                        color("red")(scad_part_models.Promic()())
                    )
                    +
                    color("blue")(self.switch_plate_spacer)
                    +
                    #translate(self._board.left_board.controler_section.jack.pos.as_tuple)(
                    #    color("red")(scad_part_models.Jack()())
                    #)
                    #+
                    #self.encoder_3d
                    #+
                    up(c.Housing.pcb_switch_plate_distance)(
                        #self.switch_plate_3d
                        #+
                        up(c.Housing.switch_plate_top_plate_distance + c.Housing.switch_plate_thickness)(
                            # color("hotpink")(self.top_plate_3d)
                            # +
                            up(c.Housing.top_plate_cover_plate_distance + c.Housing.top_plate_thickness)(
                                # color("green")(self.controller_cover_plate_3d)
                            )
                        )
                    )
                )
            )
        )

    @property
    def encoder_3d(self) -> OpenSCADObject:
        return translate(self._board.left_board.controler_section.encoder.pos.as_tuple)(
            rotate((0, 0, 180))(scad_part_models.RotaryEncoder()())
        )

    @property
    def extension_2d(self) -> OpenSCADObject:
        return translate(self._board.left_board.controler_section.encoder.pos.as_tuple)(
            scad_part_models.RotaryEncoder().cutout_2d
        )

    @property
    def layout_test(self):
        return rotate((180, 0, 0))(mirror((0, 1, 0))(
            linear_extrude(self.c.SCAD.top_plate_thickness)(
                self.pcb_polygon_2d
                -
                self._switch_holes
                -
                self.switch_field_screw_holes
                -
                self._controller_field_screw_holes
                -
                translate(self._board.left_board.controler_section.encoder.pos.as_tuple)(circle(d=7.3, segments=60))
            )
            +
            down(8.6)(linear_extrude(8.6)(
                self.wall_shape
            ))
        ))


if __name__ == "__main__":
    pass
