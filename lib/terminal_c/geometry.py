from __future__ import annotations
from typing import Optional
import math
from terminal_c.configuration.default import Configuration
from terminal_c import (
    Sides,
    Placement,
    Via,
    Track,
    TrackPath,
    DrawPolygon,
    Layer,
    Vector,
    VectorPath,
    VectorPathGroup,
    Line,
    Polygon,
    Arrangement,
    ComposedArrangement,
    factor_circular_arc,
    MagicNumbers as m
)
from terminal_c.parts import (
    Switch,
    Diode,
    SwitchLed,
    BackLed,
    Promic,
    Encoder,
    Jack,
    Pin,
    SMDSwitch
)


class SwitchDiodeArrangement:
    row_track_radius = 2.58

    def __init__(
            self,
            c: Configuration,
            side: Sides,
            col_ix: int,
            row_ix: int,
            position: Vector,
            angle: float,
            led_col_override: int = None
    ):
        self.left_conns = CellRowConnections()
        self.right_conns = CellRowConnections()
        self.c = c
        self.side = side
        self.col_ix = col_ix
        self.row_ix = row_ix
        self.position = position
        self.angle = angle
        # non-rotated cathode point of the diode
        self.cathode_point = Diode.relative_coordinates["k"].rotate(m.diode_orientation)\
                             +\
                             self.position\
                             +\
                             m.diode_offset

        switch_ref = "{s}S{c}{r}".format(
            s=self.side.letter,
            c=self.col_ix,
            r=self.c.row_ref(self.row_ix)
        )
        diode_ref = "{s}D{c}{r}".format(
            s=self.side.letter,
            c=self.col_ix,
            r=self.c.row_ref(self.row_ix)
        )
        led_ref = "{s}L{c}{r}".format(
            s=self.side.letter,
            c=self.col_ix if led_col_override is None else led_col_override,
            r=self.c.row_ref(self.row_ix)
        )

        diode_p = m.diode_offset.rotate(self.angle)
        switch_led_p = m.switch_led_offset.rotate(self.angle)

        self.switch = Switch(switch_ref, position, angle)
        self.diode = Diode(diode_ref, position + diode_p, angle + m.diode_orientation)
        self.led = SwitchLed(
            led_ref,
            position + switch_led_p,
            angle
            +
            m.switch_led_orientation
            +
            (180 if row_ix == 1 else 0)
            +
            (180 if self.side == Sides.right else 0)
            +
            (180 if row_ix == 3 and col_ix > 1 else 0)
        )

        self.row_signal_connection_point = Vector(
            self.position.x - 3.6,
            self.cathode_point.y + 0.2
        )

    @property
    def reversed_row(self) -> bool:
        return self.row_ix % 2 == 1

    @property
    def _left_extra_border(self) -> int:
        return 0 if self.col_ix == 0 else self.c.column_extra_gaps[self.col_ix - 1]

    @property
    def _right_extra_border(self) -> int:
        return 0 if self.col_ix == self.c.number_of_columns - 1 else self.c.column_extra_gaps[self.col_ix]

    @property
    def _left_side_height_delta(self) -> int:
        return self.c.column_offsets[self.col_ix - 1] - self.c.column_offsets[self.col_ix] if self.col_ix > 0 else 0

    @property
    def _right_side_height_delta(self) -> int:
        return self.c.column_offsets[self.col_ix + 1] - self.c.column_offsets[self.col_ix] if self.col_ix < 5 else 0

    @property
    def left_extra_border(self) -> int:
        if self.side == Sides.left:
            return self._left_extra_border
        else:
            return self._right_extra_border

    @property
    def right_extra_border(self) -> int:
        if self.side == Sides.left:
            return self._right_extra_border
        else:
            return self._left_extra_border

    @property
    def left_side_height_delta(self) -> int:
        if self.side == Sides.left:
            return self._left_side_height_delta
        else:
            return self._right_side_height_delta

    @property
    def right_side_height_delta(self) -> int:
        if self.side == Sides.left:
            return self._right_side_height_delta
        else:
            return self._left_side_height_delta

    @property
    def left_extra_space(self) -> int:
        return 0 if self.left_extra_border == 0 or self.left_side_height_delta > 0 else self.left_extra_border

    @property
    def right_extra_space(self) -> int:
        return 0 if self.right_extra_border == 0 or self.right_side_height_delta > 0 else self.right_extra_border

    @property
    def row_path(self) -> VectorPath:
        # construct path without rotation
        radius = SwitchDiodeArrangement.row_track_radius
        arc = factor_circular_arc(
            center=self.position,
            radius=radius,
            start=90,
            end=270,
            segments=10
        )[::-1]
        y_base = self.row_signal_connection_point.y
        p = VectorPath()
        p.append(self.cathode_point)
        p.append(Vector(p[-1].x, y_base))
        p.append(p[-1] + Vector(-3, 0))
        p.extend(arc)
        p.append(self.row_signal_connection_point)
        # return path with rotation
        return p.rotate_around(self.angle, self.position)

    @property
    def half_row_path(self) -> VectorPath:
        # construct path without rotation
        radius = SwitchDiodeArrangement.row_track_radius
        arc = factor_circular_arc(
            center=self.position,
            radius=radius,
            start=180,
            end=270,
            segments=5
        )[::-1]
        y_base = self.row_signal_connection_point.y
        p = VectorPath()
        p.append(self.cathode_point)
        p.append(Vector(p[-1].x, y_base))
        p.append(p[-1] + Vector(-3, 0))
        p.extend(arc)
        # return path with rotation
        return p.rotate_around(self.angle, self.position)

    @property
    def column_net(self):
        result = "Col{s}{c}".format(
            s=self.side.letter,
            c=self.col_ix,
        )
        return result

    @property
    def placements(self) -> list[Placement]:
        return [
            self.switch,
            self.led,
            self.diode
        ]  # + [] if self.back_led is None else [self.back_led]


class CellRowConnections:
    def __init__(self):
        self.power: Optional[Vector] = None

        """
        `power_lower` is only used for the thumb cells
        """
        self.power_lower: Optional[Vector] = None
        self.led_signal: Optional[Vector] = None
        self.row_signal: Optional[Vector] = None


class Finger(Arrangement):

    def __init__(self, c: Configuration, side: Sides):
        self.c = c
        self.side = side
        self._placements = []
        self._vias = []
        self._tracks = []
        self.colum_signal_vias_per_col: list[list[Via]] = []
        self.columns: list[list[SwitchDiodeArrangement]] = []
        self._calc_x_and_dy_for_next_neighbor()
        self._calc_placements()
        self._calc_back_led_placements()
        self._wire_backlight_leds_locally()
        for col_ix in range(self.c.number_of_columns):
            for row_ix in range(self.c.number_of_rows):
                self._add_per_cell_wiring(col_ix, row_ix)
        for col_ix in range(self.c.number_of_columns - 1):
            for row_ix in range(self.c.number_of_rows):
                self._add_adjacent_cell_row_wiring(col_ix, row_ix)
        self._calc_column_signal_vias_and_tracks()
        self._calc_columnar_power_vias_and_tracks()
        self._calc_exceptional_led_signal()
        self._placements.append(Placement(
            "{s}Logo".format(s=self.side.letter),
            pos=self.columns[3][-1].position + Vector(0, 15),
            angle=0
        ))

    def get_x_for_column(self, col_ix) -> float:
        return m.switch_width / 2 \
               + col_ix * self.c.switch_x_width \
               + sum(self.c.column_extra_gaps[:col_ix])

    @property
    def x_and_dy_for_next_neighbor(self):
        """
        The returned list contains the x coordinate where the board has a stagging offset to the next column,
        and the length of the offset as a tuple for each column except the last. (Because the last does
        not have a next neighnor anymore.)
        """
        return self._x_and_dy_for_next_neighbor

    def _calc_exceptional_led_signal(self):
        direction_f = 1 if self.side == Sides.left else -1
        for l_out, l_in, net_name, d_vec in [
            (
                self.columns[-1][1].led["out"],
                self.columns[-1][2].led["in"],
                self.columns[-1][1].led.ref + "out",
                Vector(5.6 * direction_f, 0)
            ),
            (
                self.columns[0][0].led["out"],
                self.columns[0][1].led["in"],
                self.columns[0][0].led.ref + "out",
                Vector(-5.2 * direction_f, 0)
            )
        ]:
            l_out_con = l_out + d_vec
            l_in_con = l_in + d_vec
            net_name = self.columns[-1][1].led.ref + "out"
            self._tracks.append(Track(
                a=l_out,
                b=l_out_con,
                net=net_name,
                layer=Layer.back,
                width=0.3
            ))
            self._tracks.append(Track(
                a=l_in,
                b=l_in_con,
                net=net_name,
                layer=Layer.back,
                width=0.3
            ))
            self._tracks.append(Track(
                a=l_out_con,
                b=l_in_con,
                net=net_name,
                layer=Layer.front,
                width=0.3
            ))
            self._vias.append(Via(
                pos=l_out_con,
                net=net_name,
            ))
            self._vias.append(Via(
                pos=l_in_con,
                net=net_name,
            ))

    def _calc_x_and_dy_for_next_neighbor(self):
        self._x_and_dy_for_next_neighbor = []
        h = m.switch_width / 2  # half switch width
        for ix in range(len(self.c.column_extra_gaps)):
            if self.c.column_offsets[ix] < self.c.column_offsets[ix + 1]:
                x = self.get_x_for_column(ix) + h
            else:
                x = self.get_x_for_column(ix + 1) - h
            dy = self.c.column_offsets[ix + 1] - self.c.column_offsets[ix]
            self._x_and_dy_for_next_neighbor.append((x, dy))

    def _get_switch_diode_pair(self, col_ix, row_ix) -> SwitchDiodeArrangement:
        center = Vector(
            x=self.get_x_for_column(col_ix) if self.side == Sides.left else m.x0r - self.get_x_for_column(col_ix),
            y=sum(self.c.switch_y_widths[:row_ix])
              + self.c.switch_y_widths[row_ix] / 2
              + self.c.column_offsets[col_ix]
        )

        return SwitchDiodeArrangement(
            self.c,
            self.side,
            col_ix,
            row_ix,
            center,
            0
        )

    def _calc_back_led_placements(self):
        self.back_leds: list[BackLed] = []
        self.back_leds_by_cell: dict[tuple[int, int], BackLed] = {}
        for cell in m.finger_keys_with_back_led:
            back_led_ref = "{s}B{c}{r}".format(
                s=self.side.letter,
                c=cell[0],
                r=self.c.row_ref(cell[1])
            )
            position = self.columns[cell[0]][cell[1]].position + m.finger_back_led_offset
            back_led = BackLed(
                ref=back_led_ref,
                pos=position,
                angle=m.back_led_orientation + (180 if self.side == Sides.right else 0)
            )
            self.back_leds.append(back_led)
            self._placements.append(back_led)
            assert cell not in self.back_leds_by_cell
            self.back_leds_by_cell[cell] = back_led

    def _calc_placements(self):
        # switch arrangements
        for col_ix in range(self.c.number_of_columns):
            col_cells: list[SwitchDiodeArrangement] = []
            for row_ix in range(self.c.number_of_rows):
                sd_pair = self._get_switch_diode_pair(col_ix, row_ix)
                col_cells.append(sd_pair)
                self._placements.extend(sd_pair.placements)
                self._tracks.append(Track(
                    a=sd_pair.switch["b"],
                    b=sd_pair.diode["a"],
                    net="SD{s}{c}{r}".format(s=self.side.letter, c=col_ix, r=self.c.row_ref(row_ix)),
                    layer=Layer.back,
                    width=0.7
                ))
            self.columns.append(col_cells)

    def _wire_backlight_leds_locally(
            self,
    ):
        cells = [self.columns[c][r] for (c, r) in m.finger_keys_with_back_led]
        power_net = "RGND" if self.side == Sides.right else "LVCC"
        power_pin_name = "GND" if self.side == Sides.right else "VCC"
        counter_power_net = "LGND" if self.side == Sides.left else "RVCC"
        counter_pin_name = "GND" if self.side == Sides.left else "VCC"
        # if there is a backlight led, it's on "next" for the left board and on "this" for the right board
        for cell in cells:
            back_led = self.back_leds_by_cell[(cell.col_ix, cell.row_ix)]
            switch_led = cell.led
            # counter-power
            self._tracks.append(Track(
                a=switch_led[counter_pin_name],
                b=back_led[counter_pin_name],
                net=counter_power_net,
                layer=Layer.back,
                width=0.6
            ))
            # power
            p1 = back_led[power_pin_name] + Vector(-2, 0)
            p2 = switch_led[power_pin_name] + Vector(-2, 0)
            self._tracks.append(Track(
                a=back_led[power_pin_name],
                b=p1,
                net=power_net,
                layer=Layer.back,
                width=1
            ))
            self._tracks.append(Track(
                a=switch_led[power_pin_name],
                b=p2,
                net=power_net,
                layer=Layer.back,
                width=1
            ))
            self._tracks.append(Track(
                a=p1,
                b=p2,
                net=power_net,
                layer=Layer.front,
            ))
            self._vias.append(Via(
                pos=p1,
                net=power_net,
            ))
            self._vias.append(Via(
                pos=p2,
                net=power_net,
            ))
            # LED signal
            net = switch_led.ref + "out"
            if self.side == Sides.left:
                path = VectorPath()
                path.append(switch_led["out"])
                path.append(path[-1] + Vector(0, 1.35))
                path.append(path[-1] + Vector(3, 0))
                path.append(path[-1] + Vector(0, 2))
                path.append(back_led["in"] + Vector(-0.5, 0))
                self._tracks.extend(TrackPath(
                    vectors=path,
                    net=net,
                    layer=Layer.back,
                    width=0.3
                ).tracks)
            else:
                path = VectorPath()
                path.append(switch_led["out"])
                path.append_relative(Vector(0.5, 0))
                path.append_relative(Vector(1.8, 0.2))
                path.extend(factor_circular_arc(
                    center=path[-1] + Vector(0, 1.2),
                    radius=1.2,
                    start=190,
                    end=300,
                    segments=10
                ))
                path.append(back_led.pos + Vector(4, 0))
                path.append(back_led.pos)
                path.append(back_led["in"])
                self._tracks.extend(TrackPath(
                    vectors=path,
                    net=net,
                    layer=Layer.back,
                    width=0.3
                ).tracks)

    def __left_led_net_for_cell(self, cell: SwitchDiodeArrangement) -> Optional[str]:
        col_ix = cell.col_ix
        row_ix = cell.row_ix
        next_cell = self.columns[col_ix + 1][row_ix] if col_ix < 5 else None
        former_cell = self.columns[col_ix - 1][row_ix] if col_ix > 0 else None
        if self.side == Sides.left:
            if cell.reversed_row:
                left_led_net_cell = None if former_cell is None else former_cell
            else:
                left_led_net_cell = cell
        else:
            assert self.side == Sides.right
            if cell.reversed_row:
                left_led_net_cell = cell
            else:
                left_led_net_cell = None if next_cell is None else next_cell
        if left_led_net_cell is None:
            return None
        else:
            if (left_led_net_cell.col_ix, left_led_net_cell.row_ix) in self.back_leds_by_cell:
                return self.back_leds_by_cell[(left_led_net_cell.col_ix, left_led_net_cell.row_ix)].ref + "out"
            else:
                return left_led_net_cell.led.ref + "out"

    def __right_led_net_for_cell(self, cell: SwitchDiodeArrangement) -> Optional[str]:
        col_ix = cell.col_ix
        row_ix = cell.row_ix
        next_cell = self.columns[col_ix + 1][row_ix] if col_ix < 5 else None
        former_cell = self.columns[col_ix - 1][row_ix] if col_ix > 0 else None
        if self.side == Sides.left:
            if cell.reversed_row:
                right_led_net_cell = cell
            else:
                right_led_net_cell = None if next_cell is None else next_cell
        else:
            assert self.side == Sides.right
            if cell.reversed_row:
                right_led_net_cell = None if former_cell is None else former_cell
            else:
                right_led_net_cell = cell
        if right_led_net_cell is None:
            return None
        else:
            if (right_led_net_cell .col_ix, right_led_net_cell .row_ix) in self.back_leds_by_cell:
                return self.back_leds_by_cell[(right_led_net_cell .col_ix, right_led_net_cell .row_ix)].ref + "out"
            else:
                return right_led_net_cell .led.ref + "out"

    def _add_per_cell_wiring(self, col_ix: int, row_ix: int):
        cell: SwitchDiodeArrangement = self.columns[col_ix][row_ix]
        power_net = "RGND" if self.side == Sides.right else "LVCC"
        power_pin_name = "GND" if self.side == Sides.right else "VCC"
        back_led = self.back_leds_by_cell[(col_ix, row_ix)] if (col_ix, row_ix) in self.back_leds_by_cell else None
        row_net = "Row{s}{r}".format(s=self.side.letter, r=self.c.row_ref(row_ix))
        left_led_net = self.__left_led_net_for_cell(cell)
        right_led_net = self.__right_led_net_for_cell(cell)
        left_x_base = cell.position.x - (6.5 if cell.left_side_height_delta > 0 else 7.0)
        right_x_base = cell.position.x + (6.9 if cell.right_side_height_delta > 0 else 8.8)

        if self.side == Sides.left and col_ix == 1:
            right_x_base = cell.position.x + 6.8

        if self.side == Sides.left and col_ix == 2:
            left_x_base = cell.position.x - 6.4

        if self.side == Sides.right and col_ix == 2:
            right_x_base = cell.position.x + 6.4

        if self.side == Sides.right and col_ix == 1:
            left_x_base = cell.position.x - 6.0

        if self.side == Sides.left:
            if (col_ix, row_ix) == (2, 1):
                right_x_base = cell.position.x + 7.5
        else:
            if (col_ix, row_ix) == (2, 1):
                left_x_base = cell.position.x - 5.5

        # Here we go...

        # Row signal
        if not (self.side == Sides.left and col_ix == 0):
            if self.side == Sides.right and row_ix == 2 and col_ix == 5:
                row_path = cell.half_row_path
            else:
                row_path = cell.row_path
                row_path.append(Vector(left_x_base, row_path[-1].y))
                if cell.left_extra_space != 0:
                    row_path.append_relative(Vector(-cell.left_extra_space, 0))
            self._tracks.extend(TrackPath(
                vectors=row_path,
                net=row_net,
                layer=Layer.back,
            ).tracks)
            cell.left_conns.row_signal = row_path[-1]
        if not (
            (self.side == Sides.right and col_ix == 0)
            or
            (self.side == Sides.left and col_ix == 5)
        ):
            cell.right_conns.row_signal = Vector(right_x_base, cell.diode["k"].y) + Vector(cell.right_extra_space, 0)
            self._tracks.append(Track(
                a=cell.diode["k"],
                b=cell.right_conns.row_signal,
                net=row_net,
                layer=Layer.back,
            ))

        # LED connections
        pin1, pin2 = ("out", "in")
        if cell.reversed_row:
            pin1, pin2 = pin2, pin1
        if self.side == Sides.right:
            pin1, pin2 = pin2, pin1
        # left
        if not (
            (self.side == Sides.left and col_ix == 0)
            or
            (self.side == Sides.right and col_ix == 5)
        ):
            p = VectorPath()
            p.append((cell.led if self.side == Sides.right or back_led is None else back_led)[pin1])
            p.append(Vector(left_x_base, p[-1].y))
            if cell.left_extra_space != 0:
                p.append_relative(Vector(-cell.left_extra_space, 0))
            self._tracks.extend(TrackPath(
                vectors=p,
                net=left_led_net,
                layer=Layer.back,
            ).tracks)
            cell.left_conns.led_signal = p[-1]
        # right
        if not (
                (self.side == Sides.left and col_ix == 5)
                or
                (self.side == Sides.right and col_ix == 0)
        ):
            p = VectorPath()
            p.append((cell.led if self.side == Sides.left or back_led is None else back_led)[pin2])
            p.append(Vector(right_x_base, p[-1].y + 0.5 * 0))
            if cell.right_extra_space != 0:
                p.append_relative(Vector(cell.right_extra_space, 0))
            self._tracks.extend(TrackPath(
                vectors=p,
                net=right_led_net,
                layer=Layer.back,
            ).tracks)
            cell.right_conns.led_signal = p[-1]

        # power connection (gnd on right, vcc on left)
        if cell.reversed_row:
            if not (
                (self.side == Sides.left and col_ix == 5)
                or
                (self.side == Sides.right and col_ix == 0)
            ):
                p = VectorPath()
                p.append(cell.led[power_pin_name])
                p.append(Vector(right_x_base + cell.right_extra_space, p[-1].y))
                self._tracks.extend(TrackPath(
                    vectors=p,
                    net=power_net,
                    layer=Layer.back,
                ).tracks)
                cell.right_conns.power = p[-1]
            if not (
                (self.side == Sides.left and col_ix == 0)
                or
                (self.side == Sides.right and col_ix == 5)
            ):
                p = VectorPath()
                p.append(cell.led[power_pin_name])
                p.append_relative(Vector(0, 1.5))
                p.append(Vector(left_x_base - cell.left_extra_space, p[-1].y))
                self._tracks.extend(TrackPath(
                    vectors=p,
                    net=power_net,
                    layer=Layer.back,
                ).tracks)
                cell.left_conns.power = p[-1]
        else:
            if not (
                    (self.side == Sides.left and col_ix == 5)
                    or
                    (self.side == Sides.right and col_ix == 0)
            ):
                p = VectorPath()
                p.append(cell.led[power_pin_name])
                p.append_relative(Vector(0, -1.5))
                p.append(Vector(right_x_base + cell.right_extra_space, p[-1].y))
                self._tracks.extend(TrackPath(
                    vectors=p,
                    net=power_net,
                    layer=Layer.back,
                ).tracks)
                cell.right_conns.power = p[-1]
            if not (
                    (self.side == Sides.left and col_ix == 0)
                    or
                    (self.side == Sides.right and col_ix == 5)
            ):
                p = VectorPath()
                p.append(cell.led[power_pin_name])
                p.append(Vector(left_x_base - cell.left_extra_space, p[-1].y))
                self._tracks.extend(TrackPath(
                    vectors=p,
                    net=power_net,
                    layer=Layer.back,
                ).tracks)
                cell.left_conns.power = p[-1]

    def _add_adjacent_cell_row_wiring(self, col_ix: int, row_ix: int):
        col_ix_left = col_ix if self.side == Sides.left else col_ix + 1
        col_ix_right = col_ix + 1 if self.side == Sides.left else col_ix
        cell_left: SwitchDiodeArrangement = self.columns[col_ix_left][row_ix]
        cell_right: SwitchDiodeArrangement = self.columns[col_ix_right][row_ix]
        power_net = "RGND" if self.side == Sides.right else "LVCC"
        row_net = "Row{s}{r}".format(s=self.side.letter, r=self.c.row_ref(row_ix))
        led_net = self.__left_led_net_for_cell(cell_right)
        assert led_net == self.__right_led_net_for_cell(cell_left)

        tracks_from_left = VectorPathGroup()
        tracks_from_right = VectorPathGroup()
        for i in range(3):
            tracks_from_left.append(VectorPath())
            tracks_from_right.append(VectorPath())
        nets = (row_net, led_net, power_net) if cell_left.reversed_row else (row_net, power_net, led_net)
        track_widths = (0.35, 0.35, 0.4) if cell_left.reversed_row else (0.35, 0.4, 0.35)

        # Here we go...

        # Initialize end points
        tracks_from_left[0].append(cell_left.right_conns.row_signal)
        tracks_from_right[0].append(cell_right.left_conns.row_signal)
        tracks_from_left[1].append(cell_left.right_conns.led_signal if cell_left.reversed_row else cell_left.right_conns.power)
        tracks_from_right[1].append(cell_right.left_conns.led_signal if cell_right.reversed_row else cell_right.left_conns.power)
        tracks_from_left[2].append(cell_left.right_conns.power if cell_left.reversed_row else cell_left.right_conns.led_signal)
        tracks_from_right[2].append(cell_right.left_conns.power if cell_right.reversed_row else cell_right.left_conns.led_signal)

        # Add 45° segments
        max_width = 0.6  # x and y width of the 45° segement
        addition = 0.8  # extra x and y width for each segement which is one step more on the outside
        up_count = ([], [])
        down_count = ([], [])
        for i in range(3):
            if tracks_from_left[i][-1].y > tracks_from_right[i][-1].y:
                # left lower than right
                down_count[0].append(0)
                up_count[1].append(0)
                if i == 0:
                    down_count[1].append(1)
                    up_count[0].append(1)
                else:
                    down_count[1].append(down_count[1][-1] + 1)
                    up_count[0].append(up_count[0][-1] + 1)
            else:
                # right lower than left
                up_count[0].append(0)
                down_count[1].append(0)
                if i == 0:
                    up_count[1].append(1)
                    down_count[0].append(1)
                else:
                    up_count[1].append(up_count[1][-1] + 1)
                    down_count[0].append(down_count[0][-1] + 1)
        down_count = [3 - x for x in down_count[0]], [3 - x for x in down_count[1]]
        for i in range(3):
            delta = tracks_from_left[i][-1].y - tracks_from_right[i][-1].y
            if delta > 0:
                # going up from left to right
                left_w = max_width + addition * up_count[0][i]
                left_w = min(left_w, delta/2)
                right_w = max_width + addition * down_count[1][i]
                right_w = min(right_w, delta/2)
                tracks_from_left[i].append_relative(Vector(left_w, -left_w))
                tracks_from_right[i].append_relative(Vector(-right_w, right_w))
            else:
                # going down from left right
                left_w = max_width + addition * down_count[0][i]
                left_w = min(left_w, -delta/2)
                right_w = max_width + addition * up_count[1][i]
                right_w = min(right_w, -delta/2)
                tracks_from_left[i].append_relative(Vector(left_w, left_w))
                tracks_from_right[i].append_relative(Vector(-right_w, -right_w))

        # Extra segments for the extreme pinky stagger neighborhood
        if (self.side == Sides.left and col_ix == 1) or (self.side == Sides.right and col_ix == 1):
            direction = 1 if self.side == Sides.right else -1
            intersection_vector = Vector(0, 1).rotate(45) if self.side == Sides.right else Vector(1, 0).rotate(45)
            down_y_offset = m.switch_width / 2
            up_y_offset = 6
            anchor_left = cell_left.position + Vector(m.switch_width/2, (down_y_offset - 1.8) if self.side == Sides.right else -up_y_offset)
            anchor_right = cell_right.position + Vector(-m.switch_width/2, down_y_offset if self.side == Sides.left else -up_y_offset)
            left_line = Line(
                a=anchor_left,
                b=anchor_left + intersection_vector
            )
            right_line = Line(
                a=anchor_right,
                b=anchor_right + intersection_vector
            )
            tracks_from_left.append_relative_to_all(Vector(0, 1 * direction))
            tracks_from_right.append_relative_to_all(Vector(0, -1 * direction))
            tracks_from_left.continue_to_interception(left_line)
            tracks_from_right.continue_to_interception(right_line)

        # Patch individual connections
        if self.side == Sides.left:
            if (col_ix_left, row_ix) == (4, 0):
                tracks_from_right[-1].insert(1, tracks_from_right[-1][0] + Vector(-1, -2.5))
                del(tracks_from_right[1][-1])
                del(tracks_from_right[0][-1])
        else:
            # right side
            if (col_ix_left, row_ix) == (5, 0):
                del(tracks_from_right[2][1:])
                del(tracks_from_left[2][1:])
                tracks_from_left[2].append_relative(Vector(3.2, 0))
                del(tracks_from_right[1][-1])
                del(tracks_from_right[0][-1])
            if (col_ix_left, row_ix) == (3, 0):
                del(tracks_from_right[2][1:])
                del(tracks_from_left[2][1:])
                tracks_from_left[2].append_relative(Vector(0.4, -2))
                tracks_from_left[2].append_relative(Vector(0.5, -0.5))
                tracks_from_left[2].append_relative(Vector(2, 0))
            if (col_ix_left, row_ix) == (1, 0):
                del(tracks_from_right[2][1:])
                del(tracks_from_left[2][1:])
                tracks_from_left[2].append_relative(Vector(0.3, -2.3))
                tracks_from_left[2].append_relative(Vector(1, -1))
                tracks_from_left[2].append_relative(Vector(2, 0))
            if (col_ix_left, row_ix) == (2, 2):
                del(tracks_from_left[2][1:4])
                tracks_from_right[2][-1] += Vector(0, 2)
            if (col_ix_left, row_ix) == (3, 1):
                del(tracks_from_left[2][1])
                del(tracks_from_left[1][1])
                del(tracks_from_right[0][1])
            if (col_ix_left, row_ix) in [(4, 1), (4, 0), (4, 2)]:
                del(tracks_from_left[2][1])
                del(tracks_from_left[1][1])
                del(tracks_from_right[2][1])
                del(tracks_from_right[1][1])

        # Add the tracks
        for i in range(3):
            p: VectorPath = tracks_from_left[i]
            for v in tracks_from_right[i][::-1]:
                p.append(v)
            self._tracks.extend(TrackPath(
                vectors=p,
                net=nets[i],
                layer=Layer.back,
                width=track_widths[i]
            ).tracks)

    def _calc_column_signal_vias_and_tracks(self):
        for col in self.columns:
            col_vias = []
            for cell in col:
                # Column Via
                via_p = cell.position + Vector(-6.9, -0.5)
                via = Via(
                    via_p,
                    cell.column_net
                )
                self.vias.append(via)
                col_vias.append(via)

                # Short Track from Column Via to Switch
                self._tracks.append(
                    Track(
                        a=via_p,
                        b=cell.switch["a"],
                        net=cell.column_net,
                        width=0.5,
                        layer=Layer.back
                    )
                )
            self.colum_signal_vias_per_col.append(col_vias)
            # Long track connecting the column switches
            self._tracks.append(Track(
                a=col_vias[0].pos,
                b=col_vias[-1].pos,
                layer=Layer.front,
                width=0.3,
                net=col_vias[0].net
            ))

    def _calc_columnar_power_vias_and_tracks(self):
        power_net = "LGND" if self.side == Sides.left else "RVCC"
        pin_name = "GND" if self.side == Sides.left else "VCC"
        even_row_via_indent = 1.3
        upper_end_points = []
        self.power_columns_lower_endpoints = []
        # columnar tracks
        for col in self.columns:
            first = None
            last = None
            for ix, cell in enumerate(col):
                p1 = cell.led[pin_name]
                if ix % 2 == 0:
                    p_via = p1 + Vector(3.7 - even_row_via_indent, 0)
                    p_con = p_via + Vector(even_row_via_indent, 0)
                    path = TrackPath(vectors=[p_via, p_con], net=power_net, layer=Layer.front, width=0.5)
                    self._tracks.extend(path.tracks)
                else:
                    p_via = p1 + Vector(-1.5, 0)
                    p2 = p_via + Vector(0, -1.4)
                    p3 = Vector(first.x + even_row_via_indent, p2.y)
                    path = TrackPath(vectors=[p_via, p2, p3], net=power_net, layer=Layer.front)
                    self._tracks.extend(path.tracks)
                self._tracks.append(Track(
                    a=p1,
                    b=p_via,
                    net=power_net,
                    layer=Layer.back
                ))
                self.vias.append(Via(
                    pos=p_via,
                    net=power_net
                ))
                if first is None:
                    first = p_via
                last = p_via
            upper_end_point = first + Vector(even_row_via_indent, -m.finger_gnd_column_track_overlength)
            lower_end_point = last + Vector(even_row_via_indent, 0)
            self._tracks.append(Track(
                a=upper_end_point,
                b=lower_end_point,
                net=power_net,
                layer=Layer.front,
                width=0.7
            ))
            upper_end_points.append(upper_end_point)
            self.power_columns_lower_endpoints.append(lower_end_point)
        # connection between columns
        for ix in range(len(upper_end_points) - 1):
            p1 = upper_end_points[ix]
            p2 = upper_end_points[ix + 1]
            x_offset, _ = self.x_and_dy_for_next_neighbor[ix]
            d_minus = 1
            d_plus = 1
            path = self.factor_stagging_path(p1, p2, x_offset, d_minus, d_plus)
            self._tracks.extend(TrackPath(
                path,
                power_net,
                layer=Layer.front,
                width=0.7
            ).tracks)
        self.power_columns_upper_endpoints = upper_end_points

    @staticmethod
    def factor_stagging_path(
            p1: Vector,
            p2: Vector,
            x_offset: float,
            d_minus: float,
            d_plus: float
    ) -> list[Vector]:
        if p2.x < p1.x:
            # right board
            x_offset = m.x0r - x_offset
            xx = x_offset + d_minus if p1.y < p2.y else x_offset - d_plus
        else:
            # left board
            xx = x_offset - d_minus if p1.y < p2.y else x_offset + d_plus
        path = [
            p1,
            Vector(xx, p1.y),
            Vector(xx, p2.y),
            p2
        ]
        return path

    @property
    def placements(self) -> list[Placement]:
        return self._placements

    @property
    def vias(self) -> list[Via]:
        return self._vias

    @property
    def tracks(self) -> list[Track]:
        return self._tracks

    @property
    def polygons(self) -> list[DrawPolygon]:
        return []


class Thumb(Arrangement):

    led_col_overrides = [5, 4, 0, 1, 2, 3]

    def __init__(self, c: Configuration, side: Sides):
        self.c = c
        self.side = side
        self.row_net = "Row{s}T".format(s=self.side.letter)
        self._placements = []
        self._tracks = []
        self._vias = []
        self.cells: list[SwitchDiodeArrangement] = []
        self._calculate_placement()
        self._calculate_local_cell_tracks()
        self._calculate_neighbor_connection_tracks()
        self._calculate_intra_connections()

    def get_radial_point(self, angle, radius) -> Vector:
        x = math.sin(math.radians(angle)) * radius \
            + self.c.thumb_outer_circle_peak_point[0]
        y = 1 * (
                self.c.thumb_outer_circle_peak_point[1]
                +
                self.c.thumb_outer_circle_radius
                -
                math.cos(math.radians(angle)) * radius
        )
        return Vector(x, y)

    def _calculate_intra_connections(self):
        direction_f = 1 if self.side == Sides.left else -1
        start_cell = self.cells[1]
        end_cell = self.cells[-1]
        start_angle = start_cell.angle
        end_angle = end_cell.angle

        # inner power line
        net = self.cells[1].upper_power_net
        start = start_cell.right_conns.power if self.side == Sides.left else start_cell.left_conns.power
        end = end_cell.right_conns.power_lower if self.side == Sides.left else end_cell.left_conns.power_lower
        assert start is not None
        assert end is not None
        path = VectorPath()
        path.append(start)
        path.append(start_cell.led.pos + Vector(8.5 * direction_f, -2.2).rotate(start_angle))
        path.append_relative(Vector(0.8 * direction_f, -0.8).rotate(start_angle))
        path.append_relative(Vector(0 * direction_f, -8).rotate(start_angle))
        path.append(end)
        self._tracks.extend(TrackPath(
            vectors=path,
            net=net,
            layer=Layer.back,
        ).tracks)

        # switch-led signal line
        net = self.cells[-1].led.ref + "out"
        start = start_cell.led["in"]
        end = end_cell.led["out"]
        path = VectorPath()
        path.append(start)
        path.append_relative(Vector(1 * direction_f, 0).rotate(start_angle))
        path.append(start_cell.led.pos + Vector(5 * direction_f, 0).rotate(start_angle))
        path.append(start_cell.led.pos + Vector(8.5 * direction_f, 0).rotate(start_angle))
        path.append_relative(Vector(1.6 * direction_f, -1.6).rotate(start_angle))
        path.append_relative(Vector(1 * direction_f, -9).rotate(start_angle))
        path.append(end + Vector(2 * direction_f, 3).rotate(end_angle))
        path.append(end + Vector(2 * direction_f, 1).rotate(end_angle))
        path.append(end + Vector(1 * direction_f, 0).rotate(end_angle))
        path.append(end)
        self._tracks.extend(TrackPath(
            vectors=path,
            net=net,
            layer=Layer.back,
        ).tracks)

        # upper power line
        net = self.cells[1].lower_power_net
        start = start_cell.right_conns.power_lower if self.side == Sides.left else start_cell.left_conns.power_lower
        end = end_cell.right_conns.power if self.side == Sides.left else end_cell.left_conns.power
        path = VectorPath()
        path.append(start)
        path.append(start_cell.led.pos + Vector(7.8 * direction_f, 2.2).rotate(start_angle))
        path.append_relative(Vector(3.1 * direction_f, -3.1).rotate(start_angle))
        path.append_relative(Vector(3 * direction_f, -11).rotate(start_angle))
        path.append(end_cell.led.pos + Vector((5 + 2) * direction_f, 4).rotate(start_angle))
        path.append(end_cell.led.pos + Vector((5 + 2) * direction_f, -1.55 + 2).rotate(start_angle))
        path.append(end_cell.led.pos + Vector(5 * direction_f, -1.55).rotate(start_angle))
        path.append(end)
        self._tracks.extend(TrackPath(
            vectors=path,
            net=net,
            layer=Layer.back,
        ).tracks)

        # signal to backlight led
        path = VectorPath()
        path.append(self.cells[0].led["out"])
        path.append_relative(Vector(-2 * direction_f, 0).rotate(self.cells[0].angle))
        path.append(Vector(-10 * direction_f, 4).rotate(self.cells[0].angle) + self.cells[0].position)
        path.append_relative(Vector(0 * direction_f, -11.5).rotate(self.cells[0].angle))
        path.append_relative(Vector(17 * direction_f, -0.8).rotate(self.cells[0].angle))
        path.append(self.back_led["in"])
        self._tracks.extend(TrackPath(
            vectors=path,
            net=self.cells[0].led.ref + "out",
            layer=Layer.back
        ).tracks)

        # backlight lower power (upper is added by HalfBoard)
        net = self.cells[1].upper_power_net
        self._tracks.append(Track(
            a=self.back_led[net[1:]],
            b=self.back_led[net[1:]] + Vector(0, -2).rotate(self.back_led.angle + (180 if self.side == Sides.left else 0)),
            net=net,
            layer=Layer.back,
            width=0.7
        ))

    def _calculate_neighbor_connection_tracks(self):
        def _connect_cell_pair(ix1, ix2, swap_led_direction=False):
            c1 = self.cells[ix1]
            c2 = self.cells[ix2]
            if self.side == Sides.right:
                c1, c2 = c2, c1
            l1 = c1.led
            l2 = c2.led
            led_con1, led_con2 = ("in", "out") if swap_led_direction else ("out", "in")
            if self.side == Sides.right:
                led_con1, led_con2 = led_con2, led_con1

            def _factor_led_path(start, end, angle_start, angle_end) -> list[Vector]:
                p_start = VectorPath()
                p_start.append(start)
                p_start.append(p_start[-1] + Vector(1.2, 0).rotate(angle_start))
                p_start.append(p_start[-1] + Vector(1.2, 0.7).rotate(angle_start))

                p_end = VectorPath()
                p_end.append(end)
                p_end.append(p_end[-1] + Vector(-1.2, 0).rotate(angle_end))
                p_end.append(p_end[-1] + Vector(-1.2, -0.7).rotate(angle_end))

                return p_start + p_end[::-1]

            self._tracks.extend(TrackPath(
                vectors=_factor_led_path(l1[led_con1], l2[led_con2], c1.angle, c2.angle),
                net="{r}out".format(r=c1.led.ref),
                layer=Layer.back
            ).tracks)
            self._tracks.append(Track(
                a=c1.right_conns.power_lower,
                b=c2.left_conns.power_lower,
                net=c1.lower_power_net,
                layer=Layer.back,
            ))
            self._tracks.append(Track(
                a=c1.right_conns.power,
                b=c2.left_conns.power,
                net=c1.upper_power_net,
                layer=Layer.back,
            ))
            if c1.col_ix in {3, 4} and c2.col_ix in {3, 4}:
                self.center_upper_power_line = Line(
                    a=c1.right_conns.power,
                    b=c2.left_conns.power,
                )
            row_track = VectorPath()
            row_track.append(c1.right_conns.row_signal)  # direction of the cell row path is inverted
            row_track.append(c2.left_conns.row_signal + Vector(-4.5, 0).rotate(c2.angle))
            row_track.append(c2.left_conns.row_signal)
            self._tracks.extend(TrackPath(
                vectors=row_track,
                net=self.row_net,
                layer=Layer.back,
                width=0.4
            ).tracks)
            # save the row track if it's one of those which connect to the promic track
            if 0 in {c1.col_ix, c2.col_ix}:
                self.lower_row_connection_path = row_track
            if 5 in {c1.col_ix, c2.col_ix}:
                self.upper_row_connection_path = row_track

        _connect_cell_pair(2, 3)
        _connect_cell_pair(3, 4)
        _connect_cell_pair(4, 5)
        _connect_cell_pair(0, 1, swap_led_direction=True)

    def _calculate_local_cell_tracks(self):
        for cell in self.cells:
            lower_net, upper_net = ("LGND", "LVCC") if self.side == Sides.left else ("RVCC", "RGND")
            lower_pin_name, upper_pin_name = ("GND", "VCC") if self.side == Sides.left else ("VCC", "GND")
            if cell.col_ix > 1:
                lower_net, upper_net = upper_net, lower_net
                lower_pin_name, upper_pin_name = upper_pin_name, lower_pin_name
            cell.lower_power_net = lower_net
            cell.upper_power_net = upper_net

            # lower power
            v_pow_lower_led_connection = cell.led[lower_pin_name] + Vector(0, 1.5).rotate(cell.angle)
            self._tracks.append(Track(
                a=cell.led[lower_pin_name],
                b=v_pow_lower_led_connection,
                net=lower_net,
                layer=Layer.back,
                width=0.6
            ))
            if not (
                (self.side == Sides.left and cell.col_ix in [0, 2])
                or
                (self.side == Sides.right and cell.col_ix in [1, 5])
            ):
                cell.left_conns.power_lower = v_pow_lower_led_connection + Vector(-8, 0).rotate(cell.angle)
                self._tracks.append(Track(
                    a=cell.left_conns.power_lower,
                    b=v_pow_lower_led_connection,
                    net=lower_net,
                    layer=Layer.back,
                    width=0.4
                ))
            else:
                cell.left_conns.power_lower = v_pow_lower_led_connection
            if not (
                    (self.side == Sides.left and cell.col_ix in [1, 5])
                    or
                    (self.side == Sides.right and cell.col_ix in [0, 2])
            ):
                cell.right_conns.power_lower = v_pow_lower_led_connection + Vector(2, 0).rotate(cell.angle)
                self._tracks.append(Track(
                    a=cell.right_conns.power_lower,
                    b=v_pow_lower_led_connection,
                    net=lower_net,
                    layer=Layer.back,
                    width=0.4
                ))
            else:
                cell.right_conns.power_lower = v_pow_lower_led_connection

            # upper net
            v_pow_upper_led_connection = cell.led[upper_pin_name] + Vector(0, -1.5).rotate(cell.angle)
            v_power_left = v_pow_upper_led_connection + Vector(-2, 0).rotate(cell.angle)
            v_power_right = v_pow_upper_led_connection + Vector(8, 0).rotate(cell.angle)
            self._tracks.append(Track(
                a=cell.led[upper_pin_name],
                b=v_pow_upper_led_connection,
                net=upper_net,
                layer=Layer.back,
                width=0.6
            ))
            if not (
                    (self.side == Sides.left and cell.col_ix in [0, 2])
                    or
                    (self.side == Sides.right and cell.col_ix in [1, 5])
            ):
                cell.left_conns.power = v_pow_upper_led_connection + Vector(-3.5, 0.8).rotate(cell.angle)
                self._tracks.extend(TrackPath(
                    vectors=[v_pow_upper_led_connection, v_power_left, cell.left_conns.power],
                    net=upper_net,
                    layer=Layer.back,
                    width=0.4
                ).tracks)
            else:
                cell.left_conns.power = v_pow_upper_led_connection
            if not (
                    (self.side == Sides.left and cell.col_ix in [1, 5])
                    or
                    (self.side == Sides.right and cell.col_ix in [0, 2])
            ):
                cell.right_conns.power = v_pow_upper_led_connection + Vector(9.5, 0.8).rotate(cell.angle)
                self._tracks.extend(TrackPath(
                    vectors=[v_pow_upper_led_connection, v_power_right, cell.right_conns.power],
                    net=upper_net,
                    layer=Layer.back,
                    width=0.4
                ).tracks)
            else:
                cell.right_conns.power = v_pow_upper_led_connection

            # row signal
            cell_row_path = cell.row_path
            cell.left_conns.row_signal = cell_row_path[-1]
            cell.right_conns.row_signal = cell_row_path[0]
            if not (
                (self.side == Sides.left and cell.col_ix in [0, 2])
                or
                (self.side == Sides.right and cell.col_ix in [1, 5])
            ):
                self._tracks.extend(TrackPath(
                    vectors=cell_row_path,
                    net=self.row_net,
                    layer=Layer.back,
                    width=0.4
                ).tracks)

            # Anode connections
            self._tracks.append(Track(
                a=cell.diode["a"],
                b=cell.switch["b"],
                net="SD{s}{c}T".format(s=self.side.letter, c=cell.col_ix),
                layer=Layer.back,
                width=0.7
            ))

    def _calculate_placement(self):
        num_keys_second_row = 2
        for ix in range(len(self.c.thumb_row1_radial_positions) + len(self.c.thumb_row2_radial_positions)):
            if ix < num_keys_second_row:
                angle = self.c.thumb_row2_radial_positions[ix]
                p = self.get_radial_point(
                    angle=angle,
                    radius=self.c.thumb_row2_circle_radius
                )
            else:
                angle = self.c.thumb_row1_radial_positions[ix - num_keys_second_row]
                p = self.get_radial_point(
                    angle=angle,
                    radius=self.c.thumb_row1_circle_radius
                )
            if self.side == Sides.right:
                p = Vector(m.x0r - p.x, p.y)
            sd_pair = SwitchDiodeArrangement(
                self.c,
                self.side,
                col_ix=ix,
                row_ix=self.c.number_of_rows,
                position=p,
                angle=angle if self.side == Sides.left else -angle,
                led_col_override=self.led_col_overrides[ix]
            )
            self.cells.append(sd_pair)
            self._placements.extend(sd_pair.placements)
        # Backlight LED
        back_led_angle = 1 * (
                self.c.thumb_row2_radial_positions[0]
                +
                (self.c.thumb_row2_radial_positions[-1] - self.c.thumb_row2_radial_positions[0])
                / 2
        )
        self.back_led = BackLed(
            ref="{s}Bt".format(s=self.side.letter),
            pos=self.get_radial_point(
                angle=back_led_angle,
                radius=1 * (
                        self.c.thumb_row2_circle_radius
                        +
                        (self.c.thumb_row1_circle_radius - self.c.thumb_row2_circle_radius)
                        / 2
                )
            ),
            angle=back_led_angle + 180 if self.side == Sides.left else -back_led_angle,
        )
        if self.side == Sides.right:
            self.back_led.pos.x = m.x0r - self.back_led.pos.x
        self._placements.append(self.back_led)

    @property
    def vias(self) -> list[Via]:
        return self._vias

    @property
    def placements(self) -> list[Placement]:
        return self._placements

    @property
    def tracks(self) -> list[Track]:
        return self._tracks

    @property
    def polygons(self) -> list[DrawPolygon]:
        return []


class ControlerSection(Arrangement):
    def __init__(self, c: Configuration, side: Sides, b: HalfBoard):
        self.direction_f = 1 if side == Sides.left else -1
        self.c = c
        self.side = side
        self.b = b
        self.promic = Promic(
            ref="{s}Promic".format(s=self.side.letter),
            pos=self._promic_p,
            angle=0
        )
        self.encoder = Encoder(
            ref="{s}Encoder".format(s=self.side.letter),
            pos=self._encoder_p,
            angle=180 if self.side == Sides.left else 0
        )
        self.jack = Jack(
            ref="{s}Jack".format(s=self.side.letter),
            pos=self._jack_p,
            angle=m.jack_orientation if self.side == Sides.left else m.jack_orientation + 180
        )
        self.reset_switch = SMDSwitch(
            ref="{s}RstSw".format(s=self.side.letter),
            pos=self.promic.pos + Vector(3.1 * self.direction_f, -13.6),
            angle=90 + 180*self.direction_f
        )
        self._pin_x0_inner = self.encoder["sw1"].x
        self._pin_x0_outer = self.encoder["GND"].x
        self._pin_y0 = self.encoder.pos.y - 3 * m.r
        self.col_pins = [
            Pin(
                ref="{s}C{c}Pin".format(s=self.side.letter, c=c),
                pos=Vector(self._pin_x0_inner, self._pin_y0 + offset * m.r),
                angle=0
            ) for c, offset in zip(m.pin_cols, [0, 1, 3, 5, 6])
        ]
        self.extension_pins = [
            Pin(
                ref="{s}{n}".format(s=self.side.letter, n=name),
                pos=Vector(self._pin_x0_outer, self.encoder.pos.y + i * m.r),
                angle=0
            )
            for name, i in [("PD1", -2), ("PD0", -3), ("PC6", 2), ("VCC", 3)]
        ]
        self._placements: list[Placement] = [
            self.promic,
            self.encoder,
            self.jack,
            self.reset_switch
        ]
        self._placements.extend(self.col_pins)
        self._placements.extend(self.extension_pins)
        self._vias = []
        self._tracks = []
        self._wire_reset_switch()

    def _wire_reset_switch(self):
        pass

    @property
    def _promic_p(self) -> Vector:
        usb_pos = self.b.usb_position
        promic_p = usb_pos + m.promic_connector_to_footprint_offset
        if self.side == Sides.right:
            return Vector(m.x0r - promic_p.x, promic_p.y)
        else:
            return promic_p

    @property
    def _encoder_p(self) -> Vector:
        usb_pos = self.b.usb_position
        encoder_p = usb_pos + m.encoder_from_promic_connector
        if self.side == Sides.right:
            return Vector(m.x0r - encoder_p.x, encoder_p.y)
        else:
            return encoder_p

    @property
    def _jack_p(self) -> Vector:
        y = self.b.usb_position.y + m.promic_dimensions.y + m.jack_distance_from_promic
        x = self.b.x_inner_border if self.side == Sides.left else m.x0r - self.b.x_inner_border
        return Vector(x, y)

    @property
    def vias(self) -> list[Via]:
        return self._vias

    @property
    def placements(self) -> list[Placement]:
        return self._placements

    @property
    def tracks(self) -> list[Track]:
        return self._tracks

    @property
    def polygons(self) -> list[DrawPolygon]:
        return []


class HalfBoard(ComposedArrangement):

    def add_layer_bridge(self, pos: Vector, angle: float, net: str, length: float = 1.5, width: float = 0.5):
        p = VectorPath()
        p.append(pos)
        p.append_relative(Vector(0, length).rotate(angle))
        self._tracks.extend(TrackPath(
            vectors=p,
            net=net,
            layer=Layer.back,
            width=width
        ).tracks)
        self._tracks.extend(TrackPath(
            vectors=p,
            net=net,
            layer=Layer.front,
            width=width
        ).tracks)
        self._vias.append(Via(
            pos=p[-1],
            net=net
        ))

    def __init__(self, c: Configuration, side: Sides, left_board: HalfBoard = None):
        """
        :param left_board: None if we are the left board, or the left board if we are the right board.
        """
        ComposedArrangement.__init__(self)
        assert (left_board is None) == (side == Sides.left)
        self.left_board = self if left_board is None else left_board
        self.c = c
        self.side = side
        self.finger = Finger(self.c, self.side)
        self.thumb = Thumb(self.c, self.side)
        self.controler_section = ControlerSection(self.c, self.side, self)
        self.add(self.finger)
        self.add(self.thumb)
        self.add(self.controler_section)
        self._tracks: list[Track] = []
        self._polys: list[DrawPolygon] = []
        self._vias: list[Via] = []
        self._calculate_borders()
        self._calculate_finger_to_thumb_led_wiring()
        if self.side == Sides.left:
            self._calculate_left_power_wire()
        else:
            self._calculate_right_power_wire()
        self._calc_thumb_power_wire()
        self._calc_column_wire()
        if self.side == Sides.left:
            self._calc_left_finger_row_wiring()
            self._calc_left_thumb_row_wiring()
            self._calc_left_led_wiring()
            self._calc_serial_left()
            self._calc_left_extra_pins_wiring()
            self._calc_left_reset_switch()
            self._calc_jack_wiring_left()
            # self._calc_left_throughhole_layer_connections()
        else:
            self._calc_right_finger_row_wiring()
            self._calc_right_thumb_row_wiring()
            self._calc_right_led_wiring()
            self._calc_serial_right()
            self._calc_right_extra_pins_wiring()
            self._calc_right_reset_switch()
            self._calc_jack_wiring_right()
            # self._calc_right_throughhole_layer_connections()

    @property
    def tracks(self) -> list[Track]:
        return [p for sub_arrangement in self.sub_arrangements for p in sub_arrangement.tracks] + self._tracks

    @property
    def polygons(self) -> list[DrawPolygon]:
        return [p for sub_arrangement in self.sub_arrangements for p in sub_arrangement.polygons] + self._polys

    @property
    def vias(self) -> list[Via]:
        return [p for sub_arrangement in self.sub_arrangements for p in sub_arrangement.vias] + self._vias

    @property
    def x_inner_max_switch(self) -> float:
        return self.c.number_of_columns * m.switch_width + sum(self.c.column_extra_gaps)

    @property
    def usb_position(self) -> Vector:
        x = self.x_inner_max_switch + m.promic_switch_distance + 0.5 * m.promic_dimensions.x
        y = 0 + self.c.column_offsets[-1] + m.promic_y_indent
        return Vector(x, y)

    @property
    def x_inner_border(self) -> float:
        return self.usb_position.x + m.promic_dimensions.x / 2 + m.extra_inner_border

    def _calc_left_throughhole_layer_connections(self):
        promic = self.controler_section.promic

        # Promic
        # ## GND
        self.add_layer_bridge(
            pos=promic["GND3"],
            angle=90,
            net="LGND",
            length=1.9,
            width=1.6,
        )
        self.add_layer_bridge(
            pos=promic["GND3"],
            angle=90,
            net="LGND",
            length=2.9,
            width=1.1,
        )
        # ## F4
        self.add_layer_bridge(
            pos=promic["f4"],
            angle=90,
            net="LLED",
        )
        # ## B6
        self.add_layer_bridge(
            pos=promic["b6"],
            angle=0,
            net="LPB6",
        )
        # ## B6
        self.add_layer_bridge(
            pos=promic["b4"],
            angle=-60,
            net="RowLT",
            length=1.4,
            width=0.7
        )
        # ## D7 & E6 & D3
        for pin, net in [("d7", "RowLB"), ("e6", "RowLC"), ("d3", "LSer")]:
            self.add_layer_bridge(
                pos=promic[pin],
                angle=90,
                net=net,
            )
        # ## D1 & D2
        for pin, net in [("d1", "LPD1"), ("d2", "LPD2")]:
            self.add_layer_bridge(
                pos=promic[pin],
                angle=-90,
                net=net,
            )

        # Jack
        self.add_layer_bridge(
            pos=self.controler_section.jack["1"],
            angle=-85,
            net="LGND",
            length=2
        )

        # Jack & Extension pins & Encoder
        for v, angle, net in [
            (self.controler_section.col_pins[1].pos, -90, "ColL1"),
            (self.controler_section.col_pins[2].pos, -90, "ColL2"),
            (self.controler_section.col_pins[3].pos, -90, "ColL5"),
            (self.controler_section.col_pins[4].pos, 0, "ColL4"),
            (self.controler_section.encoder["sw2"], -90, "ColL3"),

            (self.controler_section.extension_pins[0].pos, 90, "LPD1"),
            (self.controler_section.encoder["left"], 90, "LPD2"),
            (self.controler_section.encoder["GND"], 90, "LGND"),
            (self.controler_section.encoder["right"], 90, "LPB6"),
        ]:
            self.add_layer_bridge(
                pos=v,
                angle=angle,
                net=net,
            )

    def _calc_right_throughhole_layer_connections(self):
        promic = self.controler_section.promic
        jack = self.controler_section.jack

        # Promic VCC
        self.add_layer_bridge(
            pos=promic["VCC"],
            angle=-90,
            net="RVCC",
            length=1.7,
            width=1.6,
        )
        self.add_layer_bridge(
            pos=promic["VCC"],
            angle=-90,
            net="RVCC",
            length=2.7,
            width=1.1,
        )

        # Jack VCC
        self.add_layer_bridge(
            pos=self.controler_section.jack["2-1"],
            angle=0,
            net="RVCC",
            length=1.5,
            width=1.4
        )
        self.add_layer_bridge(
            pos=self.controler_section.jack["2-2"],
            angle=180,
            net="RVCC",
            length=1.5,
            width=1.4
        )

        # Jack & Extension pins & Encoder
        for v, angle, net in [
            (self.controler_section.col_pins[1].pos, 90, "ColR1"),
            (self.controler_section.col_pins[2].pos, 90, "ColR2"),
            (self.controler_section.col_pins[3].pos, 90, "ColR5"),
            (self.controler_section.col_pins[4].pos, 90, "ColR4"),
            (self.controler_section.encoder["sw1"], 90, "ColR3"),
            (self.controler_section.encoder["sw2"], 90, "RPB5"),

            (self.controler_section.extension_pins[0].pos, 110, "RPD1"),
            (self.controler_section.extension_pins[2].pos, -90, "RPC6"),
            (self.controler_section.encoder["right"], -90, "RPD2"),
            (self.controler_section.encoder["left"], -90, "RPB6"),
        ]:
            self.add_layer_bridge(
                pos=v,
                angle=angle,
                net=net,
            )

    def _calc_left_reset_switch(self):
        # rst
        self._tracks.append(Track(
            a=self.controler_section.promic["rst"],
            b=self.controler_section.reset_switch["1L"],
            net="LRst",
            layer=Layer.back
        ))
        # gnd
        p = VectorPath()
        p.append(self.controler_section.promic["GND1"])
        p.append(self.controler_section.reset_switch["2L"])
        p.append_relative(Vector(0, -1))
        p.append(Vector(p[-1].x, self.controler_section.promic["GND3"].y + 1))
        p.append_relative(Vector(-1, -1))
        p.append(self.controler_section.promic["GND3"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="LGND",
            layer=Layer.back
        ).tracks)

    def _calc_right_reset_switch(self):
        # rst
        self._tracks.append(Track(
            a=self.controler_section.promic["rst"],
            b=self.controler_section.reset_switch["1L"],
            net="LRst",
            layer=Layer.back
        ))
        # gnd
        v1 = self.controler_section.promic["GND1"] + Vector(-3.8, 0)
        v2 = self.controler_section.reset_switch["2L"] + Vector(0, 2.5)
        self._tracks.append(Track(
            a=self.controler_section.promic["GND1"],
            b=v1,
            net="RGND",
            layer=Layer.back
        ))
        p = VectorPath()
        p.append(v2)
        p.append(self.controler_section.reset_switch["2L"])
        p.append_relative(Vector(0, -1))
        p.append(Vector(p[-1].x, self.controler_section.promic["GND3"].y + 1))
        p.append_relative(Vector(-1, -1))
        p.append(self.controler_section.promic["GND3"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="RGND",
            layer=Layer.back
        ).tracks)
        self._tracks.append(Track(
            a=v1,
            b=v2,
            net="RGND",
            layer=Layer.front
        ))
        self._vias.append(Via(
            pos=v1,
            net="RGND"
        ))
        self._vias.append(Via(
            pos=v2,
            net="RGND"
        ))

    def _calc_left_extra_pins_wiring(self):
        ex_pins = self.controler_section.extension_pins
        encoder = self.controler_section.encoder

        # GND
        p = VectorPath()
        p.append(self.controler_section.jack["1"])
        p.append_relative(Vector(2, 0))
        p.append_relative(Vector(1, 1))
        p.append(Vector(p[-1].x, encoder["GND"].y - 1))
        p.append_relative(Vector(1, 1))
        p.append(encoder["GND"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="LGND",
            layer=Layer.front,
            width=0.4
        ).tracks)

        # VCC
        p = VectorPath()
        p.append(self.controler_section.jack["2-2"])
        p.append_relative(Vector(0, 2))
        p.append_relative(Vector(1.5, 1))
        p.append(Vector(p[-1].x, ex_pins[3].pos.y - 1))
        p.append_relative(Vector(-1, 1))
        p.append(ex_pins[3].pos)
        self._tracks.extend(TrackPath(
            vectors=p,
            net="LVCC",
            layer=Layer.back,
            width=0.4
        ).tracks)

        # D2
        p = VectorPath()
        p.append(self.controler_section.promic["d2"])
        p.append_relative(Vector(2, 0))
        p.append_relative(Vector(3.7, 3.7))
        p.append(Vector(p[-1].x, self.controler_section.encoder["left"].y - 1))
        p.append_relative(Vector(-1, 1))
        p.append(self.controler_section.encoder["left"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="LPD2",
            layer=Layer.front,
            width=0.26
        ).tracks)

        # B6
        p = VectorPath()
        p.append(self.controler_section.promic["b6"])
        p.append_relative(Vector(0, 3))
        p.append_relative(Vector(-1, 1))
        p.append(Vector(p[-1].x, self.controler_section.col_pins[0].pos.y - 3))
        p.append_relative(Vector(1, 1))
        p.append(Vector(self.controler_section.encoder.pos.x - 5, p[-1].y))
        p.append_relative(Vector(1, 1))
        p.append(Vector(p[-1].x, self.controler_section.encoder["right"].y - 1))
        p.append_relative(Vector(1, 1))
        p.append(self.controler_section.encoder["right"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="LPB6",
            layer=Layer.front
        ).tracks)

        # D1
        p = VectorPath()
        p.append(self.controler_section.promic["d1"])
        p.append_relative(Vector(2, 0))
        p.append_relative(Vector(3.1, 3.1))
        p.append(Vector(p[-1].x, self.controler_section.extension_pins[0].pos.y - 1))
        p.append_relative(Vector(-1, 1))
        p.append(self.controler_section.extension_pins[0].pos)
        self._tracks.extend(TrackPath(
            vectors=p,
            net="LPD1",
            layer=Layer.front,
            width=0.26
        ).tracks)

        # D0
        p = VectorPath()
        p.append(self.controler_section.promic["d0"])
        p.append_relative(Vector(1.5, 0))
        p.append_relative(Vector(1.3, 1.3))
        p.append(Vector(p[-1].x, self.controler_section.jack["2-1"].y - 2.7))
        p.append_relative(Vector(-1.55, 1.55))
        p.append(Vector(p[-1].x, self.controler_section.extension_pins[1].pos.y - 1))
        p.append(self.controler_section.extension_pins[1].pos)
        self._tracks.extend(TrackPath(
            vectors=p,
            net="LPD0",
            layer=Layer.back,
            width=0.26
        ).tracks)

        # C6
        p = VectorPath()
        p.append(self.controler_section.promic["c6"])
        p.append_relative(Vector(1.1, 0))
        p.append_relative(Vector(1, 1))
        p.append(Vector(p[-1].x, self.controler_section.jack["2-1"].y - 3.3))
        p.append_relative(Vector(-1.5, 1.5))
        p.append(Vector(p[-1].x, self.controler_section.extension_pins[2].pos.y - 1))
        p.append(self.controler_section.extension_pins[2].pos)
        self._tracks.extend(TrackPath(
            vectors=p,
            net="LPC6",
            layer=Layer.back,
            width=0.26
        ).tracks)

        # B5
        p = VectorPath()
        p.append(self.controler_section.promic["b5"])
        p.append(Vector(p[-1].x - 1.5, self.controler_section.jack["1"].y - 1))
        p.append_relative(Vector(0, 9))
        p.append_relative(Vector(-2, 2))
        p.append(p[-1] + Vector(-2.5, 0))
        p.append_relative(Vector(-2, 2))
        p.append(Vector(p[-1].x, self.controler_section.encoder["sw1"].y - 1))
        p.append_relative(Vector(-1, 1))
        p.append(self.controler_section.encoder["sw1"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="LPB5",
            layer=Layer.back,
            width=0.26
        ).tracks)

    def _calc_right_extra_pins_wiring(self):
        # B5
        p = VectorPath()
        p.append(self.controler_section.promic["b5"])
        p.append_relative(Vector(1, 0))
        p.append_relative(Vector(1, 1))
        p.append(Vector(p[-1].x, self.controler_section.encoder["sw2"].y - 1))
        p.append_relative(Vector(-1, 1))
        p.append(self.controler_section.encoder["sw2"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="RPB5",
            layer=Layer.front,
            width=0.26
        ).tracks)

        #D1 & D2
        pd1 = VectorPath()
        pd1.append(self.controler_section.promic["d1"])
        pd2 = VectorPath()
        g = VectorPathGroup()
        g.append(pd1)
        g.append(pd2)
        pd2.append(self.controler_section.promic["d2"])
        g.append_relative_to_all(Vector(-1, 0))
        g.append_relative_to_all(Vector(-m.r/2, m.r/2))
        pd2.append_relative(Vector(0, 1.5*m.r))
        pd2.append_relative(Vector(-0.5*m.r, 0.5*m.r))
        g.append_relative_to_all(Vector(-m.r, m.r))
        g.append_relative_to_all(Vector(-1, 0))
        a = Vector(self.controler_section.promic["raw"].x - 4.7, pd2[-1].y)
        g.continue_to_interception(Line(
            a=a,
            b=a + Vector(0, -1).rotate(-45)
        ))
        g.append_relative_to_all(Vector(-math.sqrt(2), 0).rotate(-45))
        pd2.append_relative(Vector(0, 1))
        pd1.continue_to_interception(pd2.lines[-1].translate(Vector(0.7, 0)))
        pd1.append(Vector(pd1[-1].x, self.controler_section.extension_pins[0].pos.y - 1))
        pd1.append_relative(Vector(1, 1))
        pd1.append(self.controler_section.extension_pins[0].pos)
        pd2.append(Vector(pd2[-1].x, self.controler_section.encoder["right"].y - 1))
        pd2.append_relative(Vector(1, 1))
        pd2.append(self.controler_section.encoder["right"])

        #B6
        pb6 = VectorPath()
        pb6.append(self.controler_section.promic["b6"])
        pb6.append_relative(Vector(0, 1))
        pb6.append_relative(Vector(1.4, 1))
        pb6.append(Vector(pb6[-1].x, self.controler_section.encoder["left"].y - 1))
        pb6.append_relative(Vector(-1, 1))
        pb6.append(self.controler_section.encoder["left"])
        g.append(pb6)

        #D0
        pd0 = VectorPath()
        pd0.append(self.controler_section.promic["d0"])
        pd0.append_relative(Vector(-1, 0))
        pd0.append_relative(Vector(-1.5*m.r, 1.5*m.r))
        pd0.append(Vector(self.controler_section.promic["raw"].x - 1.5, pd0[-1].y))
        pd0.append_relative(Vector(-1, 1))
        pd0.append(Vector(pd0[-1].x, self.controler_section.jack["2-2"].y - 3))
        pd0.append_relative(Vector(1.8, 1.8))
        pd0.append(Vector(pd0[-1].x, self.controler_section.extension_pins[1].pos.y - 1))
        pd0.append_relative(Vector(-1, 1))
        pd0.append(self.controler_section.extension_pins[1].pos)
        g.append(pd0)

        #C6
        pc6 = VectorPath()
        pc6.append(self.controler_section.promic["c6"])
        pc6.append(Vector(self.controler_section.promic["raw"].x + 3.2, pc6[-1].y))
        pc6.append_relative(Vector(-1, 1))
        pc6.append(Vector(pc6[-1].x, self.controler_section.extension_pins[2].pos.y - 1))
        pc6.append_relative(Vector(-1, 1))
        pc6.append(self.controler_section.extension_pins[2].pos)
        g.append(pc6)

        #VCC
        pvcc = VectorPath()
        pvcc.append(self.controler_section.jack["1"] + Vector(-1, 0))
        pvcc.append_relative(Vector(0, 1))
        pvcc.append_relative(Vector(-3, 3))
        pvcc.append(Vector(pvcc[-1].x, self.controler_section.encoder["GND"].y - 1))
        pvcc.append_relative(Vector(-1, 1))
        pvcc.append(self.controler_section.encoder["GND"])

        # GND
        pgnd = VectorPath()
        pgnd.append(self.controler_section.jack["2-1"])
        pgnd.append_relative(Vector(0, 2))
        pgnd.append_relative(Vector(-2, 1))
        pgnd.append(Vector(pgnd[-1].x, self.controler_section.extension_pins[3].pos.y - 1))
        pgnd.append_relative(Vector(1, 1))
        pgnd.append(self.controler_section.extension_pins[3].pos)

        for p, net in zip(g, ["RPD1", "RPD2", "RPB6", "RPD0", "RPC6"]):
            self._tracks.extend(TrackPath(
                vectors=p,
                net=net,
                layer=Layer.front,
                width=0.26
            ).tracks)

        self._tracks.extend(TrackPath(
            vectors=pvcc,
            net="RVCC",
            layer=Layer.back,
            width=0.4
        ).tracks)

        self._tracks.extend(TrackPath(
            vectors=pgnd,
            net="{s}GND".format(s=self.side.letter),
            layer=Layer.back,
            width=0.4
        ).tracks)

    def _calc_left_thumb_row_wiring(self):
        promic = self.controler_section.promic
        lower: VectorPath = self.thumb.lower_row_connection_path
        upper: VectorPath = self.thumb.upper_row_connection_path
        a = Line(lower[-3], lower[-2]).waypoint(0.7)
        b = Line(upper[0], upper[1]).waypoint(0.5)
        switch_pass_line = Line(
            b,
            b + Vector(0, -1).rotate(self.thumb.cells[-2].angle)
        )
        outer_lower_screw = self.inner_upper_corner + m.upper_inner_to_h9_vector
        self._vias.append(Via(
            pos=a,
            net="RowLT"
        ))
        self._vias.append(Via(
            pos=b,
            net="RowLT"
        ))
        pf = VectorPath()
        pf.append(a)
        pf.append_relative(Vector(0, -8).rotate(self.thumb.cells[1].angle))
        pf.append_relative(Vector(5, 0).rotate(self.thumb.cells[1].angle - 45))
        pf.append(pf.lines[-1].intersection_with(switch_pass_line))
        pf.append_relative(Vector(0, -18).rotate(self.thumb.cells[-2].angle))

        pb = VectorPath()
        pb.append(pf[-1])
        pb.append(outer_lower_screw + Vector(-2.8, 0))
        pb.extend(factor_circular_arc(outer_lower_screw, 2.8, 90, 180)[1:])
        pb.append_relative(Vector(1.8, -1.8))
        pb.append(Vector(pb[-1].x, promic["b4"].y + 1))
        pb.append_relative(Vector(-1.7, 0))
        self._tracks.append(Track(
            a=promic["b4"],
            b=pb[-1],
            net="RowLT",
            layer=Layer.front
        ))
        self._tracks.extend(TrackPath(
            vectors=pf,
            net="RowLT",
            layer=Layer.front
        ).tracks)
        self._tracks.extend(TrackPath(
            vectors=pb,
            net="RowLT",
            layer=Layer.back
        ).tracks)
        self._vias.append(Via(
            pos=pb[0],
            net="RowLT",
        ))
        self._vias.append(Via(
            pos=pb[-1],
            net="RowLT",
        ))

    def _calc_right_thumb_row_wiring(self):
        lower: VectorPath = self.thumb.lower_row_connection_path
        upper: VectorPath = self.thumb.upper_row_connection_path
        a = self.thumb.cells[1].position + Vector(9.6, -9).rotate(self.thumb.cells[1].angle)
        o = Line(lower[1], lower[0]).waypoint(0.8)
        b = Line(upper[1], upper[0]).waypoint(0.3)
        self._vias.append(Via(
            pos=a,
            net="RowRT"
        ))
        self._vias.append(Via(
            pos=b,
            net="RowRT"
        ))
        self._tracks.append(Track(
            a=o,
            b=a,
            net="RowRT",
            layer=Layer.back
        ))
        p = VectorPath()
        p.append(a)
        p.append_relative(Vector(0, -6).rotate(-50 + self.thumb.cells[1].angle))
        p.append(b)
        p.append_relative(Vector(0, -8).rotate(self.thumb.cells[-2].angle))
        p.append(self.controler_section.col_pins[4].pos + Vector(-3, 3))
        p.append(Vector(p[-1].x, self.controler_section.promic["b4"].y + 2))
        p.append_relative(Vector(2, -2))
        p.append(self.controler_section.promic["b4"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="RowRT",
            layer=Layer.front
        ).tracks)

    def _calc_serial_left(self):
        x_offset = 1.2
        p = VectorPath()
        p.append(self.controler_section.promic["d3"])
        p.append(Vector(self.controler_section.promic.pos.x + x_offset + 1, p[-1].y))
        p.append_relative(Vector(-1, 1))
        p.append(self.controler_section.promic.pos + Vector(x_offset, -3))
        self._tracks.extend(TrackPath(
            vectors=p,
            net="{}Ser".format(self.side.letter),
            layer=Layer.front,
            width=0.4
        ).tracks)
        self._vias.append(Via(
            pos=p[-1],
            net="{}Ser".format(self.side.letter),
        ))
        p2 = VectorPath()
        p2.append(p[-1])
        p2.append(Vector(p2[-1].x, self.controler_section.jack["2"].y - 1))
        p2.append(self.controler_section.jack["2"])
        self._tracks.extend(TrackPath(
            vectors=p2,
            net="{}Ser".format(self.side.letter),
            layer=Layer.back,
            width=0.4
        ).tracks)

    def _calc_serial_right(self):
        x_offset = 0.5
        p = VectorPath()
        p.append(self.controler_section.promic["d3"])
        p.append(Vector(self.controler_section.promic.pos.x + x_offset + 1, p[-1].y))
        p.append_relative(Vector(-1, 1))
        p.append(self.controler_section.promic.pos + Vector(x_offset, 0))
        p.append_relative(Vector(-3, 3))
        p.append_relative(Vector(0, 2))
        self._tracks.extend(TrackPath(
            vectors=p,
            net="{}Ser".format(self.side.letter),
            layer=Layer.back,
            width=0.4
        ).tracks)
        self._vias.append(Via(
            pos=p[-1],
            net="{}Ser".format(self.side.letter),
        ))
        p2 = VectorPath()
        p2.append(p[-1])
        p2.append(Vector(p2[-1].x, self.controler_section.jack["2"].y))
        p2.append(self.controler_section.jack["2"])
        self._tracks.extend(TrackPath(
            vectors=p2,
            net="{}Ser".format(self.side.letter),
            layer=Layer.front,
            width=0.4
        ).tracks)

    def _calc_left_led_wiring(self):
        p = VectorPath()
        p.append(self.finger.columns[-1][0].led["in"])
        p.append_relative(Vector(4.3, 0))
        p.append(Vector(p[-1].x + 1, self.controler_section.promic["f4"].y))
        self._vias.append(Via(
            pos=p[-1],
            net="LLED"
        ))
        self._tracks.extend(TrackPath(
            vectors=p,
            net="LLED",
            layer=Layer.back,
            width=0.4
        ).tracks)
        self._tracks.append(Track(
            a=p[-1],
            b=self.controler_section.promic["f4"],
            net="LLED",
            layer=Layer.front,
            width=0.4
        ))

    def _calc_right_led_wiring(self):
        p = VectorPath()
        p.append(self.finger.columns[-1][0].led["in"])
        p.append_relative(Vector(-5.5, 0))
        self._vias.append(Via(
            pos=p[-1],
            net="RLED"
        ))
        self._tracks.extend(TrackPath(
            vectors=p,
            net="RLED",
            layer=Layer.back,
            width=0.4
        ).tracks)
        p2 = VectorPath()
        p2.append(p[-1])
        p2.append_relative(Vector(-1, -1))
        p2.append(Vector(p2[-1].x, self.controler_section.promic["d3"].y))
        p2.append_relative(Vector(-2, -2))
        p2.append(Vector(self.controler_section.promic.pos.x - 2, p2[-1].y))
        p2.append_relative(Vector(-1, 1))
        p2.append(Vector(p2[-1].x, self.controler_section.promic["f4"].y - 1))
        p2.append_relative(Vector(-1, 1))
        p2.append(self.controler_section.promic["f4"])
        self._tracks.extend(TrackPath(
            vectors=p2,
            net="RLED",
            layer=Layer.front,
            width=0.4
        ).tracks)

    def _calc_jack_wiring_left(self):
        promic = self.controler_section.promic
        jack = self.controler_section.jack

        # GND
        p = VectorPath()
        p.append(promic["GND1"])
        p.append(promic["GND2"])
        p.append_relative(Vector(-1, 0))
        p.append_relative(Vector(-2.4, 2.4))
        p.append(Vector(p[-1].x, jack["1"].y - 2))
        p.append(jack["1"] + Vector(0.7, 0.2))
        self._tracks.extend(TrackPath(
            vectors=p,
            net="LGND",
            layer=Layer.back,
            width=1.1
        ).tracks)

        # VCC
        pb = VectorPath()
        pb.append(promic["VCC"])
        pb.append_relative(Vector(3.1, 0))
        pb.append_relative(Vector(4, 4))
        pb.append(Vector(pb[-1].x, promic["b2"].y + 0.6))

        pf = VectorPath()
        pf.append(pb[-1] + Vector(0, -1.7))
        pf.append(Vector(pf[-1].x, jack["1"].y - 5.2))
        pf.append_relative(Vector(3, 3))
        pf.append(Vector(jack["2-1"].x - 2, pf[-1].y))
        pf.append(jack["2-1"] + Vector(-0.7, 0))
        self._tracks.extend(TrackPath(
            vectors=pb,
            net="LVCC",
            layer=Layer.back,
            width=1.1
        ).tracks)
        self._tracks.extend(TrackPath(
            vectors=pf,
            net="LVCC",
            layer=Layer.front,
            width=1.1
        ).tracks)
        #for pos in [pb[-1], pf[0], (pb[-1]+pf[0])/2] + [jack["2-1"] + (jack["2-2"] - jack["2-1"]) * f for f in [0.25, 0.5, 0.75]]:
        for pos in [pb[-1], pf[0], (pb[-1] + pf[0]) / 2]:
                self._vias.append(Via(
                pos=pos,
                net="LVCC"
            ))
        for lay in [Layer.back, Layer.front]:
            self._tracks.append(Track(
                a=jack["2-1"],
                b=jack["2-2"],
                net="LVCC",
                layer=lay,
                width=1.4
            ))

    def _calc_jack_wiring_right(self):
        jack = self.controler_section.jack
        promic = self.controler_section.promic
        main_x = jack["2-1"].x
        # GND
        p = VectorPath()
        p.append(promic["GND3"])
        p.append(Vector(main_x - 2.4, p[-1].y + 1))
        p.append(Vector(p[-1].x, jack["2-2"].y + 2))
        p.append_relative(Vector(1, 1))
        p.append(Vector(jack["1"].x - 3.5, p[-1].y))
        p.append(jack["1"] + Vector(-1, 0))
        self._tracks.extend(TrackPath(
            vectors=p,
            net="{}GND".format(self.side.letter),
            layer=Layer.back,
            width=1.1
        ).tracks)

        #VNC
        p = VectorPath()
        p.append(promic["VCC"])
        p.append(Vector(main_x, p[-1].y + 1))
        p.append(jack["2-2"] + Vector(0, 0))
        self._tracks.extend(TrackPath(
            vectors=p,
            net="{}VCC".format(self.side.letter),
            layer=Layer.back,
            width=1.1
        ).tracks)
        self._tracks.append(Track(
            a=jack["2-2"],
            b=jack["2-1"],
            net="{}VCC".format(self.side.letter),
            layer=Layer.front,
            width=1.1
        ))

    def _calc_left_finger_row_wiring(self):
        promic = self.controler_section.promic

        # row 1
        p = VectorPath()
        p.append(self.controler_section.promic["d4"])
        p.append_relative(Vector(-8, 0))
        p.append_relative(Vector(-2, -2))
        p.append(Vector(p[-1].x, promic["VCC"].y - m.r / 2 + 2))
        p.append_relative(Vector(-2, -2))
        p.append(Vector(promic["VCC"].x - 1.5, p[-1].y))
        p.append(self.finger.columns[-1][0].diode["k"] + Vector(2.5, 0))
        self._tracks.extend(TrackPath(
            vectors=p,
            net="RowLA",
            layer=Layer.front,
            width=0.26
        ).tracks)
        self._tracks.append(Track(
            a=p[-1],
            b=self.finger.columns[-1][0].diode["k"],
            net="RowLA",
            layer=Layer.back,
            width=0.4
        ))
        self._vias.append(Via(
            pos=p[-1],
            net="RowLA",
        ))

        # row 2
        p_v1 = self.finger.columns[-1][1].diode["k"] + Vector(1.9, 0)
        p_target = self.controler_section.promic["d7"]
        self._tracks.append(Track(
            a=self.finger.columns[-1][1].diode["k"],
            b=p_v1,
            net="RowLB",
            layer=Layer.back,
            width=0.4
        ))
        self._vias.append(Via(
            pos=p_v1,
            net="RowLB",
        ))
        p = VectorPath()
        p.append(p_v1)
        p.append(Vector(p[-1].x, p_target.y + 1 + m.r / 2))
        p.append_relative(Vector(1, -1))
        p.append_relative(Vector(5, 0))
        p.append_relative(Vector(m.r / 2, -m.r / 2))
        p.append(p_target)
        self._tracks.extend(TrackPath(
            vectors=p,
            net="RowLB",
            layer=Layer.front,
            width=0.26
        ).tracks)

        # row 3
        p = VectorPath()
        p_target = self.controler_section.promic["e6"]
        p.append(self.finger.columns[-1][2].position + Vector(0, -SwitchDiodeArrangement.row_track_radius))
        p.append(Vector(p[-1].x, self.second_power_arc.y + 1))
        p.append_relative(Vector(1, -1))
        p.append(self.second_power_arc + Vector(-0.2, 0))
        self._tracks.extend(TrackPath(
            vectors=p,
            net="RowLC",
            layer=Layer.back,
            width=0.3
        ).tracks)
        self._vias.append(Via(
            pos=p[-1],
            net="RowLC",
        ))
        p2 = VectorPath()
        p2.append(p[-1])
        p2.append_relative(Vector(1, -1))
        p2.append(Vector(p2[-1].x, p_target.y + 1 + m.r / 2))
        p2.append_relative(Vector(1, -1))
        p2.append_relative(Vector(5, 0))
        p2.append_relative(Vector(m.r / 2, -m.r / 2))
        p2.append(p_target)
        self._tracks.extend(TrackPath(
            vectors=p2,
            net="RowLC",
            layer=Layer.front,
            width=0.26
        ).tracks)

    def _calc_right_finger_row_wiring(self):
        base_line = Line(
            a=self.controler_section.promic["d2"],
            b=self.controler_section.promic["d3"]
        ).translate(Vector(-5, 0))

        # row 1
        p = VectorPath()
        p.append(self.finger.columns[-1][0].left_conns.row_signal)
        p.append_relative(Vector(-2, 0))
        p.append_relative(Vector(-1, -1))
        p.append(Vector(p[-1].x, self.controler_section.promic["d2"].y - m.r / 2 + 1))
        p.append_relative(Vector(-1, -1))
        p.append_relative(Vector(-1, 0))
        p.continue_to_interception(base_line)
        p.append_relative(Vector(-1, 1))
        p.append(Vector(p[-1].x, self.controler_section.promic["d4"].y - 1))
        p.append_relative(Vector(1, 1))
        p.append(self.controler_section.promic["d4"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="RowRA",
            layer=Layer.back,
            width=0.26
        ).tracks)

        # row 2
        p = VectorPath()
        p.append(self.finger.columns[-1][1].left_conns.row_signal)
        p.append_relative(Vector(-1, 0))
        self._tracks.extend(TrackPath(
            vectors=p,
            net="RowRB",
            layer=Layer.back,
            width=0.4
        ).tracks)
        self._vias.append(Via(
            pos=p[-1],
            net="RowRB",
        ))
        p2 = VectorPath()
        p2.append(p[-1])
        p2.append(Vector(p2[-1].x, self.controler_section.promic["d7"].y + 1))
        p2.append_relative(Vector(-1, -1))
        p2.append(self.controler_section.promic["d7"])
        self._tracks.extend(TrackPath(
            vectors=p2,
            net="RowRB",
            layer=Layer.front,
            width=0.26
        ).tracks)

        # row 3
        p = VectorPath()
        p_target = self.controler_section.promic["e6"]
        p.append(self.finger.columns[-1][2].left_conns.row_signal)
        p.append(Vector(p[-1].x, self.second_power_arc.y + 1))
        p.append_relative(Vector(-1, -1))
        p.append(self.second_power_arc + Vector(+0.2, 0))
        self._tracks.extend(TrackPath(
            vectors=p,
            net="RowRC",
            layer=Layer.back,
            width=0.4
        ).tracks)
        self._vias.append(Via(
            pos=p[-1],
            net="RowRC",
        ))
        p2 = VectorPath()
        p2.append(p[-1])
        p2.append(Vector(p2[-1].x, p_target.y + 1))
        p2.append_relative(Vector(-1, -1))
        p2.append(p_target)
        self._tracks.extend(TrackPath(
            vectors=p2,
            net="RowRC",
            layer=Layer.front,
            width=0.26
        ).tracks)

    def _calc_column_wire(self):
        track_d = 1.2
        track_d_lower = 0.38 + 0.3  # gap + track width

        direction_f = 1 if self.side == Sides.left else -1
        g = VectorPathGroup()
        g.append(VectorPath())
        g[0].append(self.finger.colum_signal_vias_per_col[0][-1].pos)
        g[0].append(Vector(g[0][-1].x, self.finger.columns[0][-1].position.y + 6))
        if self.side == Sides.left:
            g[0].append_relative(Vector(0, 2.5))
            g[0].append_relative(Vector(13, 0))
            g[0].append_relative(Vector(1.8, -1.8))

        # second column joins
        g.append(VectorPath())
        g[1].append(self.finger.colum_signal_vias_per_col[1][-1].pos)
        g[1].append(Vector(g[1][-1].x, g[0][-1].y - track_d))
        g.append_relative_to_all(Vector(direction_f, 0))

        # first indentation corner
        a = -(self.indentation_path[-2] - self.indentation_path[-1]).angle
        ml = Line(a=self.indentation_path[-1], b=self.indentation_path[-1].get_radial_point(-45, 1))
        if self.side == Sides.right:
            ml = ml.mirror(m.mirror_line)
        g.continue_to_interception(ml)

        # second indentation corner
        g.append_relative_to_all(Vector(0, -1).rotate(180 + a * direction_f))
        ml = Line(a=self.indentation_path[-2], b=self.indentation_path[-2].get_radial_point(-45, 1))
        if self.side == Sides.right:
            ml = ml.mirror(m.mirror_line)
        g.continue_to_interception(ml.translate(Vector(0, -1.5)))

        # third column joins
        g.append(VectorPath())
        g[2].append(self.finger.colum_signal_vias_per_col[2][-1].pos)
        g[2].append(Vector(g[2][-1].x, g[-2][-1].y - track_d))
        g.append_relative_to_all(Vector(direction_f, 0))

        # elongate until beginning of thumb cluster
        ml = Line(a=self.indentation_path[1], b=self.indentation_path[1] + Vector(0, 1).rotate(45)).translate(
            Vector(-1, 0)
        )
        if self.side == Sides.right:
            ml = ml.mirror(m.mirror_line)
        g.continue_to_interception(ml)

        track_points_thumb_entry = [p[-1] for p in g]

        # fourth column joins
        g.append(VectorPath())
        g[3].append(self.finger.colum_signal_vias_per_col[3][-1].pos)
        g[3].append(Vector(g[3][-1].x, g[-2][-1].y - track_d))

        # fifth column joins
        g.append(VectorPath())
        g[4].append(self.finger.colum_signal_vias_per_col[4][-1].pos)
        g[4].append(Vector(g[4][-1].x, g[-2][-1].y - track_d))
        g.append_relative_to_all(Vector(direction_f * 0.1, 0))

        # diagonal part
        diagonal_angle = 38
        if self.side == Sides.right:
            diagonal_angle = 180 - diagonal_angle
        diagonal_start = self.finger.columns[-1][-1].switch.pos + Vector(-12 * direction_f, g[-1][-1].y)
        if self.side == Sides.left:
            diagonal_start = diagonal_start + Vector(-3, 0)
        ml = Line(
            a=diagonal_start,
            b=diagonal_start + Vector(0, 1),
        )
        g.continue_to_interception(ml)
        g.append_relative_to_all(Vector(1, 0).rotate(diagonal_angle))
        diagonal_end = g[0][-1] + Vector(0, 0.2)
        if self.side == Sides.right:
            diagonal_end = diagonal_end + Vector(0, 0.4)
        ml = Line(
            a=diagonal_end,
            b=diagonal_end + Vector(0, 1).rotate(50 * direction_f),
        )
        ml = ml.translate(Vector(0, 4))
        g.continue_to_interception(ml)
        for p in g[::2]:
            p.append_relative(Vector(1.5, 0).rotate(diagonal_angle))

        # end regular upper layer part
        for ix in range(len(g)):
            self._tracks.extend(TrackPath(
                vectors=g[ix],
                net="Col{s}{r}".format(s=self.side.letter, r=ix),
                layer=Layer.front
            ).tracks)

        # start new path group for back layer and set vias
        g_back = VectorPathGroup()
        for ix, p in enumerate(g):
            g_back.append(VectorPath())
            g_back[-1].append(p[-1])
            self._vias.append(Via(
                pos=g_back[-1][-1],
                net="Col{s}{r}".format(s=self.side.letter, r=ix),
            ))
        for p in g_back[1::2]:
            p.append_relative(Vector(1.5, 0).rotate(diagonal_angle))
        diagonal_elongation = 0.8 if self.side == Sides.right else 1.2
        g_back.append_relative_to_all(Vector(diagonal_elongation, 0).rotate(diagonal_angle))
        g_back.append_relative_to_all(Vector(1 * direction_f, 0))

        # join the special sixth column
        p6_upper = VectorPath()
        p6_upper.append(self.finger.colum_signal_vias_per_col[5][-1].pos)
        p6_upper.append_relative(Vector(0, 10 if self.side == Sides.right else 12))
        self._tracks.extend(TrackPath(
            vectors=p6_upper,
            net="Col{s}{r}".format(s=self.side.letter, r=5),
            layer=Layer.front
        ).tracks)
        self._vias.append(Via(
            pos=p6_upper[-1],
            net="Col{s}{r}".format(s=self.side.letter, r=5),
        ))
        g_back.append(VectorPath())
        g_back[-1].append(p6_upper[-1])
        if self.side == Sides.left:
            g_back[-1].append(g_back[-2][-2] + Vector(2.5, -track_d_lower))
            g_back[-1].append_relative(Vector(1, 0))
        else:
            g_back[-1].append(Vector(g_back[-1][-1].x, g_back[-2][-1].y - track_d_lower))
            g_back[-1].append_relative(Vector(-1, 0))

        # controller section, 1st corner
        corner_anchor = Vector(
            x=self.controler_section.encoder.pos.x - 12.5 * direction_f,
            y=g_back[-1][-1].y
        )
        ml = Line(a=corner_anchor, b=corner_anchor + Vector(0, 1).rotate(-45 * direction_f))
        g_back.continue_to_interception(ml)

        # via section (next to the encoder)
        col_pin_bus_via: list[Vector] = [Vector(0, 0)] * 6
        # - col 5
        for p in g_back[0:-1]:
            p.append_relative(v=Vector(0.5 * direction_f, -0.5))
        col_pin_bus_via[4] = g_back[-2][-2] + (g_back[-2][-1] - g_back[-2][-2]) / 2
        # - col 6
        g_back[-1].append_relative(Vector(0, -0.6))
        col_pin_bus_via[5] = g_back[-1][-1]
        # - col 4
        g_back.append_relative_to_all(v=Vector(0, -0.1))
        g_back[-2].append_relative(Vector(0, -0.9))
        g_back[-2].append_relative(Vector(-0.5 * direction_f, -0.5))
        g_back[-2].append_relative(Vector(0, -0.1))
        g_back.continue_to_interception(Line(
            a=self.controler_section.encoder["sw2" if self.side == Sides.left else "sw1"] + Vector(0, -0.4),
            b=self.controler_section.encoder["sw2" if self.side == Sides.left else "sw1"] + Vector(0, -0.4) + Vector(1, 0),
        ))
        g_back[3].append_relative(v=Vector(-0.5 * direction_f, -0.5))
        col_pin_bus_via[3] = g_back[3][-2] + (g_back[3][-1] - g_back[3][-2]) / 2
        g_back[3].append_relative(Vector(0, -0.1))
        # - col 3
        g_back.continue_to_interception(Line(
            a=self.controler_section.col_pins[2].pos + Vector(0, 0.25),
            b=self.controler_section.col_pins[2].pos + Vector(0, 0.25) + Vector(1, 0),
        ))
        g_back[2].append_relative(v=Vector(-0.5 * direction_f, -0.5))
        col_pin_bus_via[2] = g_back[2][-2] + (g_back[2][-1] - g_back[2][-2]) / 2
        g_back[2].append_relative(Vector(0, -0.1))
        # - col 2
        g_back.continue_to_interception(Line(
            a=self.controler_section.col_pins[1].pos + Vector(0, 0.25),
            b=self.controler_section.col_pins[1].pos + Vector(0, 0.25) + Vector(1, 0),
        ))
        g_back[1].append_relative(v=Vector(-0.5 * direction_f, -0.5))
        col_pin_bus_via[1] = g_back[1][-2] + (g_back[1][-1] - g_back[1][-2]) / 2
        g_back[1].append_relative(Vector(0, -0.1))
        # - get the bus omni-distant again
        g_back.continue_to_interception(Line(
            a=g_back[1][-1] + Vector(0, -0.2),
            b=g_back[1][-1] + Vector(0, -0.2) + Vector(1, 0),
        ))
        for ix, p in enumerate(g_back[1:6]):
            p.append_relative(v=Vector(0, -0.2 * (ix + 1)))
            p.append_relative(v=Vector(0.5 * direction_f, -0.5))
        g_back.append_relative_to_all(Vector(0, -0.1))
        # - add the vias
        self._vias.extend([
            Via(
                pos=col_pin_bus_via[ix + 1],
                net="Col{s}{r}".format(s=self.side.letter, r=ix + 1)
            ) for ix in range(self.c.number_of_columns - 1)
        ])

        # controller section, 2nd corner
        corner_anchor = Vector(
            x=corner_anchor.x,
            y=self.controler_section.encoder.pos.y - 11
        )
        ml = Line(a=corner_anchor, b=corner_anchor + Vector(0, 1).rotate(130 * direction_f))
        g_back.continue_to_interception(ml)

        g_back_2 = VectorPathGroup()
        for p in g_back:
            g_back_2.append(VectorPath())
            g_back_2[-1].append(p[-1])

        g_back_2.append_relative_to_all(Vector(0, -1.5).rotate(45 * direction_f))
        g_back_2.append_relative_to_all(Vector(1 * direction_f, 0))
        corner_anchor = Vector(
            x=self.controler_section.jack.pos.x - 17.5 * direction_f,
            y=g_back_2[-1][-1].y
        )
        ml = Line(a=corner_anchor, b=corner_anchor + Vector(0, 1).rotate(135 * direction_f))
        g_back_2.continue_to_interception(ml)

        # connect column pin
        # 0
        self._tracks.append(Track(
            a=g_back_2[0][-1],
            b=self.controler_section.col_pins[0].pos,
            net="Col{s}0".format(s=self.side.letter),
            layer=Layer.back,
            width=0.3
        ))
        # 1
        self._tracks.append(Track(
            a=col_pin_bus_via[1],
            b=self.controler_section.col_pins[1].pos,
            net="Col{s}1".format(s=self.side.letter),
            layer=Layer.front
        ))
        # 2
        self._tracks.append(Track(
            a=col_pin_bus_via[2],
            b=self.controler_section.col_pins[2].pos,
            net="Col{s}2".format(s=self.side.letter),
            layer=Layer.front
        ))
        # 3
        self._tracks.append(Track(
            a=col_pin_bus_via[3],
            b=self.controler_section.encoder["sw2" if self.side == Sides.left else "sw1"],
            net="Col{s}3".format(s=self.side.letter),
            layer=Layer.front
        ))
        # 4
        self._tracks.append(Track(
            a=col_pin_bus_via[4],
            b=self.controler_section.col_pins[4].pos,
            net="Col{s}4".format(s=self.side.letter),
            layer=Layer.front
        ))
        # 5
        p = VectorPath()
        p.append(col_pin_bus_via[5])
        p.append_relative(Vector(2 * direction_f, 0))
        p.append(self.controler_section.col_pins[3].pos)
        self._tracks.extend(TrackPath(
            vectors=p,
            net="Col{s}4".format(s=self.side.letter),
            layer=Layer.front
        ).tracks)

        g_back_2.append_relative_to_all(Vector(0, -0.5).rotate(45 * direction_f))
        g_back_2.append_relative_to_all(Vector(0, -0.1))
        ml = Line(a=self.controler_section.jack.pos, b=self.controler_section.jack.pos + Vector(1, 0))
        g_back_2.continue_to_interception(ml)

        # now get close to the promic
        if self.side == Sides.left:
            corner_anchor = g_back_2[-1][-1] + Vector(0, -2)
            ml = Line(a=corner_anchor, b=corner_anchor + Vector(0, -1).rotate(45))
            g_back_2.continue_to_interception(ml)
            g_back_2.append_relative_to_all(Vector(-2, -2))
            g_back_2.append_relative_to_all(Vector(0, -0.1))
        else:
            corner_anchor = g_back_2[0][-1] + Vector(0, -3)
            ml = Line(a=corner_anchor, b=corner_anchor + Vector(0, -1).rotate(45))
            g_back_2.continue_to_interception(ml)
            g_back_2.append_relative_to_all(Vector(-2, -2))
            g_back_2.append_relative_to_all(Vector(-1, 0))
            corner_anchor = self.controler_section.promic["b6"] + Vector(5.8, 0)
            ml = Line(a=corner_anchor, b=corner_anchor + Vector(0, -1).rotate(45))
            g_back_2.continue_to_interception(ml)
            g_back_2.append_relative_to_all(Vector(-1, -1))
            g_back_2.append_relative_to_all(Vector(0, -0.1))

        # and connect that column signal bus to the promic
        next_pin = self.controler_section.promic["b2"]
        for c_ix in range(self.c.number_of_columns):
            for p_ix, p in enumerate(g_back_2[c_ix:] if self.side == Sides.right else g_back_2[::-1][c_ix:]):
                p.append(Vector(p[-1].x, next_pin.y + 1 - p_ix * 0.2))
                p.append_relative(Vector(-0.8, -1))
                if p_ix == 0:
                    p.append(next_pin)
            next_pin = Vector(next_pin.x, next_pin.y - m.r)

        # end lower layer part
        for ix in range(len(g_back)):
            self._tracks.extend(TrackPath(
                vectors=g_back[ix],
                net="Col{s}{r}".format(s=self.side.letter, r=ix),
                layer=Layer.back,
                width=0.3
            ).tracks)
        for ix in range(len(g_back_2)):
            self._tracks.extend(TrackPath(
                vectors=g_back_2[ix],
                net="Col{s}{r}".format(s=self.side.letter, r=ix),
                layer=Layer.back,
                width=0.26
            ).tracks)

        self.__wire_thumb_columns_0_and_1(track_points_thumb_entry=track_points_thumb_entry)
        if self.side == Sides.left:
            self.__wire_thumb_columns_2_to_5_left(g, diagonal_angle)
        else:
            self.__wire_thumb_columns_2_to_5_right(g, diagonal_angle)

    def __wire_thumb_columns_2_to_5_left(self, g: VectorPathGroup, diagonal_angle: float):
        # col 2
        a = (self.indentation_path[1] - self.indentation_path[0]).angle
        p = VectorPath()
        p.append(g[2][-5])
        p.append_relative(Vector(0, -6).rotate(a))
        p.append(self.thumb.cells[2].switch["a"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="ColL2",
            layer=Layer.back
        ).tracks)
        self._vias.append(Via(
            pos=p[0],
            net="ColL2",
        ))

        # col 3
        p = VectorPath()
        p.append(g[3][-1])
        p.append_relative(Vector(-2, 0).rotate(diagonal_angle))
        p.append_relative(Vector(-1, 0))
        p.append_relative(Vector(-0.5, 0.5))
        p.append_relative(Vector(0, 4))
        p.append(self.thumb.cells[3].switch["a"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="ColL3",
            layer=Layer.back
        ).tracks)

        # col 4
        p = VectorPath()
        p.append(self.controler_section.col_pins[-1].pos)
        p.append_relative(Vector(0, 2))
        p.append_relative(Vector(-2, 2))
        p.append_relative(Vector(-2, 0))
        p.append(self.thumb.cells[4].switch["a"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="ColL4",
            layer=Layer.back
        ).tracks)

        # col 5
        p = VectorPath()
        p.append(self.controler_section.col_pins[-2].pos)
        p.append_relative(Vector(2, 0))
        p.append_relative(Vector(2, 2))
        p.append_relative(Vector(0, 7))
        p.append_relative(Vector(2, 2))
        p.append_relative(Vector(2, 0))
        p.append_relative(Vector(2, 2))
        p.append(self.thumb.cells[5].switch["a"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="ColL5",
            layer=Layer.back
        ).tracks)

    def __wire_thumb_columns_2_to_5_right(self, g: VectorPathGroup, diagonal_angle: float):
        # col 2
        p = VectorPath()
        p.append(g[2][-1])
        p.append_relative(Vector(3, 0).rotate(diagonal_angle + 180))
        p.append_relative(Vector(4, 0))
        p.append_relative(Vector(1, 1))
        p.append(self.thumb.cells[2].switch["a"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="ColR2",
            layer=Layer.back
        ).tracks)

        # col 3
        p = VectorPath()
        p.append(g[3][-1])
        p.append_relative(Vector(-8, 0).rotate(diagonal_angle + 180))
        p.append_relative(Vector(-2, 0))
        p.append_relative(Vector(-1, 1))
        self._tracks.extend(TrackPath(
            vectors=p,
            net="ColR3",
            layer=Layer.front
        ).tracks)
        self._vias.append(Via(
            pos=p[-1],
            net="ColR3",
        ))
        self._tracks.append(Track(
            a=self.thumb.cells[3].switch["a"],
            b=p[-1],
            net="ColR3",
            layer=Layer.back
        ))

        # col 4
        p = VectorPath()
        p.append(self.controler_section.col_pins[-1].pos)
        p.append_relative(Vector(-2, 0))
        p.append_relative(Vector(-6, 6))
        p.append(self.thumb.cells[4].switch["a"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="ColR4",
            layer=Layer.back
        ).tracks)

        # col 5
        p = VectorPath()
        p.append(self.controler_section.col_pins[-2].pos)
        p.append_relative(Vector(-2, 0))
        p.append_relative(Vector(-10, 10))
        p.append(self.thumb.cells[5].switch.pos + Vector(7.4, -7.5).rotate(self.thumb.cells[5].angle))
        p.append(self.thumb.cells[5].switch.pos + Vector(-5.8, -7.5).rotate(self.thumb.cells[5].angle))
        p.append_relative(Vector(-1, 1).rotate(self.thumb.cells[5].angle))
        p.append(self.thumb.cells[5].switch["a"])
        self._tracks.extend(TrackPath(
            vectors=p,
            net="ColL5",
            layer=Layer.back
        ).tracks)

    def __wire_thumb_columns_0_and_1(self, track_points_thumb_entry: list[Vector]):
        a = (self.indentation_path[1] - self.indentation_path[0]).angle
        t_sw_0 = self.thumb.cells[0].switch
        t_sw_1 = self.thumb.cells[1].switch
        if self.side == Sides.right:
            a = -a

        # col 0
        p_c0 = VectorPath()
        p_c0.append(track_points_thumb_entry[0])
        p_c0.append_relative(Vector(0, -21).rotate(a))
        if self.side == Sides.left:
            p_c0.append(t_sw_0["a"] + Vector(0, -3).rotate(t_sw_0.angle))
            self._vias.append(Via(
                pos=p_c0[-1],
                net="Col{}0".format(self.side.letter),
            ))
            self._tracks.append(Track(
                a=p_c0[-1],
                b=t_sw_0["a"],
                net="Col{}0".format(self.side.letter),
                layer=Layer.back
            ))
        else:
            p_c0.append(t_sw_0.pos + Vector(8, -7.2).rotate(t_sw_0.angle))
            p_c0.append(t_sw_0.pos + Vector(-7, -7.2).rotate(t_sw_0.angle))
            self._vias.append(Via(
                pos=p_c0[-1],
                net="Col{}0".format(self.side.letter),
            ))
            self._tracks.append(Track(
                a=p_c0[-1],
                b=t_sw_0["a"],
                net="Col{}0".format(self.side.letter),
                layer=Layer.back
            ))

        self._tracks.extend(TrackPath(
            vectors=p_c0,
            net="Col{}0".format(self.side.letter),
            layer=Layer.front,
        ).tracks)

        # col 1
        self._vias.append(Via(
            pos=track_points_thumb_entry[1],
            net="Col{s}1".format(s=self.side.letter)
        ))
        p_c1_0 = VectorPath()
        p_c1_0.append(track_points_thumb_entry[1])
        p_c1_0.append_relative(Vector(0, -3).rotate(a))
        self._tracks.extend(TrackPath(
            vectors=p_c1_0,
            net="Col{}1".format(self.side.letter),
            layer=Layer.back,
        ).tracks)
        self._vias.append(Via(
            pos=p_c1_0[-1],
            net="Col{}1".format(self.side.letter)
        ))
        p_c1_1 = VectorPath()
        p_c1_1.append(p_c1_0[-1])
        p_c1_1.append_relative(Vector(0, -17).rotate(a))
        if self.side == Sides.left:
            p_c1_1.append(t_sw_0.pos + Vector(-6, -7.7).rotate(t_sw_0.angle))
            p_c1_1.append(t_sw_0.pos + Vector(8, -7.7).rotate(t_sw_0.angle))
            p_c1_1.append_relative(Vector(0, 5).rotate(-35 + t_sw_0.angle))
            self._vias.append(Via(
                pos=p_c1_1[-1],
                net="Col{}1".format(self.side.letter),
            ))
            self._tracks.append(Track(
                a=p_c1_1[-1],
                b=self.thumb.cells[1].switch["a"],
                net="Col{}1".format(self.side.letter),
                layer=Layer.back
            ))
        else:
            p_c1_1.append(t_sw_0.pos + Vector(7.8, -8.3).rotate(t_sw_0.angle))
            p_c1_1.append(t_sw_0.pos + Vector(-8, -8.3).rotate(t_sw_0.angle))
            p_c1_1.append(self.thumb.back_led["VCC"] + Vector(0, 4.5).rotate(self.thumb.back_led.angle))
            p_c1_1.append(t_sw_1.pos + Vector(7.8, -7.3).rotate(t_sw_1.angle))
            p_c1_1.append(t_sw_1.pos + Vector(-7, -7.3).rotate(t_sw_1.angle))
            self._vias.append(Via(
                pos=p_c1_1[-1],
                net="Col{}1".format(self.side.letter),
            ))
            self._tracks.append(Track(
                a=p_c1_1[-1],
                b=t_sw_1["a"],
                net="Col{}1".format(self.side.letter),
                layer=Layer.back
            ))
        self._tracks.extend(TrackPath(
            vectors=p_c1_1,
            net="Col{}1".format(self.side.letter),
            layer=Layer.front,
        ).tracks)

    def _calc_thumb_power_wire(self):
        """
        Irregular wiring of power connections to the thumb cluster.
        The regular power wires between thumb cells is in the Thumb class.
        """
        lower_net, upper_net = ("LGND", "LVCC") if self.side == Sides.left else ("RVCC", "RGND")
        lower_pin_name, upper_pin_name = ("GND", "VCC") if self.side == Sides.left else ("VCC", "GND")
        center_angle = self.c.thumb_row1_radial_positions[2] - self.c.thumb_row1_radial_positions[1]
        a_upper = center_angle + 4
        a_lower = center_angle + 1
        d_upper = self.c.thumb_row1_circle_radius - 7.1
        d_lower = self.c.thumb_row1_circle_radius - 3

        # upper power (VCC on left, GND on right side)
        # (connect back-side power track, parallel to the switch column which is closest to the promic, to the thumb cluster)
        p_start = self.lowest_finger_power_connection + Vector(0, 3)
        self._tracks.append(Track(
            a=self.lowest_finger_power_connection,
            b=p_start,
            net=upper_net,
            layer=Layer.back,
            width=0.7
        ))
        path = VectorPath()
        path.append(p_start)
        path.append_relative(Vector(0, 10))
        p_con = self.thumb.get_radial_point(angle=a_upper, radius=d_upper)
        if self.side == Sides.right:
            p_con = m.mirror_line.mirror_point(p_con)
        path.append(p_con)
        self._tracks.extend(TrackPath(
            vectors=path,
            net=upper_net,
            layer=Layer.front,
            width=0.6
        ).tracks)
        self._vias.append(Via(
            pos=path[0],
            net=upper_net
        ))
        self._vias.append(Via(
            pos=path[-1],
            net=upper_net
        ))

        # lower power (GND on left, VCC on right side)
        # (connects the front-side vertical power line of the promic-closest switch column to the thumb cluster)
        path = VectorPath()
        path.append(self.finger.power_columns_lower_endpoints[-1])
        if self.side == Sides.left:
            path.append_relative(Vector(0, 11))
        else:
            path.append_relative(Vector(0, 4))
            path.append_relative(Vector(-8, 1))
            path.append_relative(Vector(-5, 6))
        p_con = self.thumb.get_radial_point(angle=a_lower, radius=d_lower)
        if self.side == Sides.right:
            p_con = m.mirror_line.mirror_point(p_con)
        path.append(p_con)
        path.append(self.thumb.back_led.pos)
        path.append(
            self.thumb.back_led[lower_pin_name]
            +
            Vector(0, 2.5).rotate(self.thumb.back_led.angle + (180 if self.side == Sides.left else 0))
        )
        self._tracks.extend(TrackPath(
            vectors=path,
            net=upper_net,
            layer=Layer.front,
            width=0.6
        ).tracks)
        self._tracks.append(Track(
            a=path[-1],
            b=self.thumb.back_led[lower_pin_name],
            net=lower_net,
            layer=Layer.back,
            width=0.6
        ))
        self._vias.append(Via(
            pos=path[-1],
            net=lower_net
        ))
        # the via that connects to the thumb's switch LED power line
        source_line = Line(
            a=path[-2],
            b=path[-3]
        )
        intersection_point = source_line.intersection_with(self.thumb.center_upper_power_line)
        self._vias.append(Via(
            pos=intersection_point,
            net=lower_net
        ))

    def _calculate_left_power_wire(self):
        # GND finger <-> promic
        col_point = self.finger.power_columns_upper_endpoints[-1]
        pin_point = self.controler_section.promic["GND3"]
        con_point = Vector(col_point.x, pin_point.y)
        self._tracks.append(Track(
            a=pin_point,
            b=con_point + Vector(0.2, 0),
            net="LGND",
            layer=Layer.front,
            width=0.9
        ))

        # VCC finger connections
        self.highest_finger_power_connection = None
        # connecting uppest and lowest led in the columnd right next to the promic to VCC
        for cell in [self.finger.columns[-1][0], self.finger.columns[-1][-1]]:
            path = VectorPath()
            path.append(cell.led["VCC"])
            path.append(path[-1] + Vector(0, -1.5))
            path.append(path[-1] + Vector(7.5, 0))
            path.append(path[-1] + Vector(0.5, 0.5))
            path.append(path[-1] + Vector(5, 0))
            self.lowest_finger_power_connection = path[-1]
            if self.highest_finger_power_connection is None:
                self.highest_finger_power_connection = path[-1]
            self._tracks.extend(TrackPath(
                vectors=path,
                net="LVCC",
                layer=Layer.back,
                width=0.7
            ).tracks)
        # connect middle led of the column right next to the promic to VCC
        self.middle_finger_power_connection = Vector(
            self.lowest_finger_power_connection.x,
            self.finger.columns[-1][1].led["VCC"].y
        )
        self._tracks.append(Track(
            a=self.finger.columns[-1][1].led["VCC"],
            b=self.middle_finger_power_connection,
            net="LVCC",
            layer=Layer.back,
            width=0.7
        ))
        # connection to promic
        self._tracks.append(Track(
            a=self.highest_finger_power_connection,
            b=self.controler_section.promic["VCC"],
            net="LVCC",
            layer=Layer.back,
            width=0.8
        ))
        # the vertical connection, parallel to the switch columns right next to promic
        # (with the "power arcs" which provide some comfortable space to vias)
        self.second_power_arc = Vector(self.highest_finger_power_connection.x, self.controler_section.jack.pos.y - 1)
        p = VectorPath()
        p.append(self.highest_finger_power_connection)
        p.append(Vector(p[-1].x, self.middle_finger_power_connection.y - 6))
        p.append_relative(Vector(1, 1))
        p.append_relative(Vector(0, 2))
        p.append_relative(Vector(-1, 1))
        p.append(self.middle_finger_power_connection)
        p.append(self.second_power_arc + Vector(0, -2))
        p.append_relative(Vector(1.3, 1))
        p.append_relative(Vector(0, 2))
        p.append_relative(Vector(-1.3, 1))
        p.append(self.lowest_finger_power_connection)
        self._tracks.extend(TrackPath(
            vectors=p,
            net="LVCC",
            layer=Layer.back,
            width=0.8
        ).tracks)

    def _calculate_right_power_wire(self):
        # VCC finger <-> promic
        col_point = self.finger.power_columns_upper_endpoints[-1]
        pin_point = self.controler_section.promic["VCC"]
        con_point = Vector(pin_point.x + 2.5, col_point.y)
        self._tracks.extend(TrackPath(
            vectors=[
                col_point,
                con_point,
                Vector(con_point.x, pin_point.y),
                pin_point
            ],
            net="RGND",
            layer=Layer.front,
            width=0.8
        ).tracks)

        # GND finger <-> promic
        self.highest_finger_power_connection = None
        # first and last rows' leds
        for cell in [self.finger.columns[-1][0], self.finger.columns[-1][-1]]:
            corner_point = cell.led["GND"] + Vector(-7, 0)
            self.lowest_finger_power_connection = corner_point
            if self.highest_finger_power_connection is None:
                self.highest_finger_power_connection = corner_point
            self._tracks.append(Track(
                a=corner_point,
                b=cell.led["GND"],
                net="RGND",
                layer=Layer.back,
                width=0.7
            ))
        # middle led
        path = VectorPath()
        path.append(self.finger.columns[-1][1].led["GND"])
        path.append_relative(Vector(0, 2))
        self.middle_finger_power_connection = Vector(
            self.lowest_finger_power_connection.x,
            path[-1].y
        )
        path.append(self.middle_finger_power_connection)
        self._tracks.extend(TrackPath(
            vectors=path,
            net="RGND",
            layer=Layer.back,
            width=0.7
        ).tracks)
        # connection to promic
        path = VectorPath()
        path.append(self.highest_finger_power_connection)
        path.append(self.controler_section.promic["GND2"])
        path.append(self.controler_section.promic["GND1"])
        self._tracks.extend(TrackPath(
            vectors=path,
            net="RGND",
            layer=Layer.back,
            width=0.8
        ).tracks)
        # vertical connection
        self.second_power_arc = Vector(self.highest_finger_power_connection.x, self.controler_section.jack.pos.y - 1)
        p = VectorPath()
        p.append(self.highest_finger_power_connection)
        p.append(self.middle_finger_power_connection)
        p.append(self.second_power_arc + Vector(0, -2))
        p.append_relative(Vector(-1.3, 1))
        p.append_relative(Vector(0, 2))
        p.append_relative(Vector(1.3, 1))
        p.append(self.lowest_finger_power_connection)
        self._tracks.extend(TrackPath(
            vectors=p,
            net="RGND",
            layer=Layer.back,
            width=0.8
        ).tracks)

    def _calculate_finger_to_thumb_led_wiring(self):
        direction_f = 1 if self.side == Sides.left else -1
        # led signal
        path = VectorPath()
        path.append(self.finger.columns[0][-1].led["out"])
        if self.side == Sides.left:
            path.append_relative(Vector(0, 1.5))
        else:
            path.append_relative(Vector(5, 0))
            path.append_relative(Vector(0, 1.5 + 2*0.7))
            path.append_relative(Vector(-5, 0))
        path.append_relative(Vector(11 * direction_f, 0))
        path.append_relative(Vector(1.5 * direction_f, -1.5))
        self._tracks.extend(TrackPath(
            vectors=path,
            net=self.finger.columns[0][-1].led.ref + "out",
            layer=Layer.back
        ).tracks)
        path2 = VectorPath()
        path2.append(Vector(self.indentation_path[-1].x - 1.5, path[-1].y))
        path2.append_relative(self.indentation_path[-2] - self.indentation_path[-1])
        path2.append_relative(self.indentation_path[-3] - self.indentation_path[-2] + Vector(2.5, 0))
        path2.append_relative(Vector(0, 16).rotate(self.thumb.cells[2].angle * direction_f))
        if self.side == Sides.right:
            path2 = path2.mirror(m.mirror_line)
        path2.insert(0, path[-1])
        self._tracks.extend(TrackPath(
            vectors=path2,
            net=self.finger.columns[0][-1].led.ref + "out",
            layer=Layer.back
        ).tracks)
        self._tracks.append(Track(
            a=path2[-1],
            b=self.thumb.cells[2].led["in"],
            net=self.finger.columns[0][-1].led.ref + "out",
            layer=Layer.back
        ))

    def _calculate_borders(self):
        """
        The border is calculated for the left side.
        If we are the right side, the points are just mirrored in the end.
        """
        h = m.switch_width / 2  # half switch width
        thumb_keys = [c.switch for c in self.left_board.thumb.cells]

        # some important points and lines we need later
        corner_thumb_key_r1_4_upper_left = (thumb_keys[5].pos + Vector(-h, -h)).rotate_around(
            thumb_keys[5].angle,
            thumb_keys[5].pos
        )
        corner_thumb_key_r1_4_upper_right = (thumb_keys[5].pos + Vector(h, -h)).rotate_around(
            thumb_keys[5].angle, thumb_keys[5].pos
        )
        corner_thumb_key_r1_1_lower_left = (
                thumb_keys[2].pos + Vector(-h - m.extra_border_1st_thumb_key_outer, h)).rotate_around(
            thumb_keys[2].angle, thumb_keys[2].pos
        )
        corner_thumb_key_r1_1_upper_left = (
                thumb_keys[2].pos + Vector(-h - m.extra_border_1st_thumb_key_outer, -h)).rotate_around(
            thumb_keys[2].angle, thumb_keys[2].pos
        )
        corner_finger_key_c2_c_lower_left = Vector(
            m.switch_width,  # on purpose without the gap, so this points actually hits the first column
            sum(self.c.switch_y_widths) + self.c.column_offsets[1]
        )
        corner_finger_key_c2_c_lower_right = Vector(
            2 * m.switch_width + self.c.column_extra_gaps[0],
            sum(self.c.switch_y_widths) + self.c.column_offsets[1]
        )
        corner_finger_key_c1_c_lower_left = Vector(
            0,
            sum(self.c.switch_y_widths) + self.c.column_offsets[0]
        )
        corner_finger_key_c1_c_lower_right = Vector(
            m.switch_width,
            sum(self.c.switch_y_widths) + self.c.column_offsets[0]
        )

        # Here we go... calculating the outline polygon...
        p = Polygon()

        # first point is the upper left corner of the upper left key
        p.append(Vector(0, self.c.column_offsets[0]))

        # going along the upper switch border, two points for each column-neighbor pair
        for x, dy in self.finger.x_and_dy_for_next_neighbor:
            last_point = p[-1]
            y = last_point.y
            p.append(Vector(x, y))
            p.append(Vector(x, y + dy))

        # upper right corner
        upper_right_corner = Vector(
            x=self.x_inner_border,
            y=p[-1].y
        )
        p.append(upper_right_corner)
        self.inner_upper_corner = upper_right_corner

        # thumb cluster, key 1/4, upper right, two points
        right_border_base_vector = Line(upper_right_corner, upper_right_corner + Vector(0, 1))
        line_thumb_key_r1_4_upper = Line(corner_thumb_key_r1_4_upper_left, corner_thumb_key_r1_4_upper_right)
        thumb_upper_hit_point = right_border_base_vector.intersection_with(line_thumb_key_r1_4_upper)
        p.append(thumb_upper_hit_point)
        p.append(corner_thumb_key_r1_4_upper_right)

        # same key, lower right, with extra segment for pcb-connection
        segment_length = self.c.pcb_connection_width_outer / (math.sin(math.radians(90 - thumb_keys[5].angle)))
        p.append((thumb_keys[5].pos + Vector(h, -h + segment_length)).rotate_around(
            thumb_keys[5].angle, thumb_keys[5].pos
        ))
        p.append((thumb_keys[5].pos + Vector(h, h)).rotate_around(
            thumb_keys[5].angle, thumb_keys[5].pos
        ))

        # thumb key 2/2, lower right
        p.append((thumb_keys[1].pos + Vector(h, h)).rotate_around(
            thumb_keys[1].angle, thumb_keys[1].pos
        ))

        # thumb key 2/2, lower left
        p.append((thumb_keys[1].pos + Vector(-h, h)).rotate_around(
            thumb_keys[1].angle, thumb_keys[1].pos
        ))

        # thumb key 2/1, lower right
        p.append((thumb_keys[0].pos + Vector(h, h)).rotate_around(
            thumb_keys[0].angle, thumb_keys[0].pos
        ))

        # thumb key 2/1, lower left
        p.append((thumb_keys[0].pos + Vector(-h, h)).rotate_around(
            thumb_keys[0].angle, thumb_keys[0].pos
        ))

        # thumb key 1/1, lower left, with extra segment for pcb connection
        segment_to_break_down = Line(p[-1], corner_thumb_key_r1_1_lower_left)
        factor = self.c.pcb_connection_width_inner / segment_to_break_down.length
        assert 0 < factor < 1
        side_factor = (1 - factor) / 2
        p.append(segment_to_break_down.waypoint(side_factor))
        p.append(segment_to_break_down.waypoint(1 - side_factor))
        p.append(corner_thumb_key_r1_1_lower_left)

        # thumb key 1/1, left intersection with horizontal bottom line of finger key 2C
        line_finger_key_c2_c_lower = Line(corner_finger_key_c2_c_lower_left, corner_finger_key_c2_c_lower_right)
        line_thumb_key_r1_1_left = Line(corner_thumb_key_r1_1_lower_left, corner_thumb_key_r1_1_upper_left)
        thumb_left_hit_point = line_finger_key_c2_c_lower.intersection_with(line_thumb_key_r1_1_left)
        p.append(thumb_left_hit_point)

        # bottom indentation
        last_vector = p[-1] - p[-2]
        elogation_f = last_vector.y / -self.c.bottom_outline_indenting_depth
        indentation_vector = last_vector / elogation_f
        p.append_relative(indentation_vector)
        indentation_end = Vector(self.left_board.finger.columns[2][-1].position.x - 7, line_finger_key_c2_c_lower.a.y)
        p.append_relative(Vector(-(thumb_left_hit_point.x - indentation_end.x + 2 * indentation_vector.x), 0))
        p.append(indentation_end)
        self.indentation_path = VectorPath()
        self.indentation_path.extend(p[-4:])

        # finger key 2C, lower left
        p.append(corner_finger_key_c2_c_lower_left)

        # finger key 1C, lower right and lower left with extra segment for pcb connection
        p.append(corner_finger_key_c1_c_lower_right)
        p.append(corner_finger_key_c1_c_lower_left + Vector(self.c.pcb_connection_width_outer, 0))
        p.append(corner_finger_key_c1_c_lower_left)

        # that's it; now, if we're doing the right side, just mirror all points
        if self.side == Sides.right:
            p2 = Polygon()
            mirror_x = m.x0r / 2
            mirror_line = Line(Vector(mirror_x, 0), Vector(mirror_x, 100))
            for point in p:
                p2.append(mirror_line.mirror_point(point))
            p = p2

        self.pcb_outline = p
        self._polys.append(DrawPolygon(polygon=p, layer=Layer.cuts, width=0.05))


class Holes(Arrangement):

    @property
    def placements(self) -> list[Placement]:
        return self._placements

    @property
    def vias(self) -> list[Via]:
        return []

    @property
    def tracks(self) -> list[Track]:
        return []

    @property
    def polygons(self) -> list[DrawPolygon]:
        return []

    @staticmethod
    def _middle(key1: SwitchDiodeArrangement, key2: SwitchDiodeArrangement) -> Vector:
        d = key2.position - key1.position
        return key1.position + d * 0.5

    def factor_placement(self, ix: int, position: Vector):
        ref = "{s}H{ix}".format(s=self.side.letter, ix=ix)
        return Placement(ref, position, 0)

    def __init__(self, c: Configuration, s: Sides, left_board: HalfBoard):
        self.c = c
        self.side = s

        radius_thumb_screws = 1 * (
                c.thumb_row2_circle_radius
                +
                (
                        c.thumb_row1_circle_radius
                        -
                        c.thumb_row2_circle_radius
                ) / 2
        )

        self.switch_field_screws = [
            self.factor_placement(0, self._middle(
                left_board.finger.columns[0][0],
                left_board.finger.columns[1][1]
            )),
            self.factor_placement(1, self._middle(
                left_board.finger.columns[0][1],
                left_board.finger.columns[1][2]
            )),
            self.factor_placement(2, self._middle(
                left_board.finger.columns[2][0],
                left_board.finger.columns[3][1]
            )),
            self.factor_placement(3, self._middle(
                left_board.finger.columns[2][1],
                left_board.finger.columns[3][2]
            )),
            self.factor_placement(4, self._middle(
                left_board.finger.columns[4][0],
                left_board.finger.columns[5][1]
            )),
            self.factor_placement(5, self._middle(
                left_board.finger.columns[4][1],
                left_board.finger.columns[5][2]
            )),
            self.factor_placement(6, left_board.thumb.get_radial_point(
                c.thumb_row2_radial_positions[0], radius_thumb_screws
            )),
            self.factor_placement(7, left_board.thumb.get_radial_point(
                c.thumb_row2_radial_positions[1], radius_thumb_screws
            ))
        ]
        self.controller_field_screws = [
            self.factor_placement(8, left_board.inner_upper_corner + m.upper_inner_to_h8_vector),
            self.factor_placement(9, left_board.inner_upper_corner + m.upper_inner_to_h9_vector),
            self.factor_placement(10, left_board.controler_section.jack.pos + m.jack_to_h10_vector),
        ]

        self._placements = []
        self._placements.extend(self.switch_field_screws)
        self._placements.extend(self.controller_field_screws)

        if s == Sides.right:
            for p in self.placements:
                p.pos = Vector(m.x0r - p.pos.x, p.pos.y)


class Board(ComposedArrangement):
    def __init__(self, c: Configuration):
        ComposedArrangement.__init__(self)
        self.c = c
        self.left_board = HalfBoard(c, Sides.left)
        self.right_board = HalfBoard(c, Sides.right, left_board=self.left_board)
        self.left_holes = Holes(c, s=Sides.left, left_board=self.left_board)
        self.right_holes = Holes(c, s=Sides.right, left_board=self.left_board)
        self.add(self.left_board)
        self.add(self.right_board)
        self.add(self.left_holes)
        self.add(self.right_holes)
