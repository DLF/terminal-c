import math
import unittest

from terminal_c import (
    Line,
    Vector
)


class TestLine(unittest.TestCase):

    def test_mirror_point(self):
        # given
        line = Line(
            Vector(0, 5),
            Vector(5, 0)
        )
        # when
        p = line.mirror_point(Vector(1, 2))
        # then
        self.assertEqual(Vector(3, 4), p)


class TestVector(unittest.TestCase):

    def test_rotate(self):
        # given
        v = Vector(0, 5)
        # when
        v2 = v.rotate(90)
        # then
        self.assertAlmostEqual(-5, v2.x)
        self.assertAlmostEqual(0, v2.y)

    def test_length(self):
        v = Vector(1, 3)
        self.assertAlmostEqual(math.sqrt(10), v.length)

    def test_angle(self):
        v = Vector(1, 3)
        self.assertAlmostEqual(18.43494882, v.angle)

    def test_angle_2(self):
        v = Vector(5, 5)
        self.assertAlmostEqual(45, v.angle)

    def test_rotate_around(self):
        # given
        v = Vector(3, 0)
        a = Vector(3, 3)
        # when
        v2 = v.rotate_around(90, a)
        self.assertAlmostEqual(6, v2.x)
        self.assertAlmostEqual(3, v2.y)

    def test_get_radial_point(self):
        # given
        v = Vector(1, 1)
        # when
        v2 = v.get_radial_point(270, 4)
        print(v2)
        # then
        self.assertAlmostEqual(5, v2.x)
        self.assertAlmostEqual(1, v2.y)

