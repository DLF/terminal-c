from terminal_c import geometry as g
from terminal_c import schematic as s
from terminal_c.configuration.default import Configuration
from terminal_c import Sides

c = Configuration()

board = g.Board(c)


def get_annotated_led_list_for_side(side: Sides) -> list[s.LEDMeta]:
    schematic = s.Board(c, side)
    half_board = board.left_board if side == Sides.left else board.right_board
    for led in schematic.led_list[:-1]:
        if led.key[1] >= c.number_of_rows:
            # thumb
            led.pos = half_board.thumb.cells[led.key[0]].led.pos
        else:
            led.pos = half_board.finger.columns[led.key[0]][led.key[1]].led.pos
    schematic.led_list[-1].pos = half_board.thumb.back_led.pos
    if side == Sides.right:
        for led in schematic.led_list:
            led.key = (c.number_of_columns - led.key[0] - 1, led.key[1] + c.number_of_rows + 1)
    return schematic.led_list


def get_annotated_led_list() -> list[s.LEDMeta]:
    leds = get_annotated_led_list_for_side(Sides.left) + get_annotated_led_list_for_side(Sides.right)
    for ix, led in enumerate(leds):
        led.ix = ix
    return leds


def get_row_col_index_dict(leds: list[s.LEDMeta]) -> dict[int, dict[int, s.LEDMeta]]:
    result = {}
    for led in leds:
        if not led.is_key:
            continue
        col = led.key[0]
        row = led.key[1]
        if row not in result:
            result[row] = {}
        result[row][col] = led
    return result


def get_row_col_index_array(leds: list[s.LEDMeta]) -> list[list[s.LEDMeta]]:
    d = get_row_col_index_dict(leds)
    result = []
    for row_ix in range(2*c.number_of_rows + 2):
        result.append([])
        for col_ix in range(c.number_of_columns):
            result[-1].append(d[row_ix][col_ix])
    return result


def get_qmk_string(leds: list[s.LEDMeta]) -> str:
    """
led_config_t g_led_config = { {
    {  24,  23,  18,  17,  10,   9 },
    {  25,  22,  19,  16,  11,   8 },
    {  26,  21,  20,  15,  12,   7 },
    { NO_LED, NO_LED, NO_LED,  14,  13,   6 },
    {  51,  50,  45,  44,  37,  36 },
    {  52,  49,  46,  43,  38,  35 },
    {  53,  48,  47,  42,  39,  34 },
    { NO_LED, NO_LED, NO_LED,  41,  40,  33 }
}, {
    {  85,  16 }, {  50,  13 }, {  16,  20 }, {  16,  38 }, {  50,  48 }, {  85,  52 }, {  95,  63 },
    {  85,  39 }, {  85,  21 }, {  85,   4 }, {  68,   2 }, {  68,  19 }, {  68,  37 }, {  80,  58 },
    {  60,  55 }, {  50,  35 }, {  50,  13 }, {  50,   0 }, {  33,   3 }, {  33,  20 }, {  33,  37 },
    {  16,  42 }, {  16,  24 }, {  16,   7 }, {   0,   7 }, {   0,  24 }, {   0,  41 }, { 139,  16 },
    { 174,  13 }, { 208,  20 }, { 208,  38 }, { 174,  48 }, { 139,  52 }, { 129,  63 }, { 139,  39 },
    { 139,  21 }, { 139,   4 }, { 156,   2 }, { 156,  19 }, { 156,  37 }, { 144,  58 }, { 164,  55 },
    { 174,  35 }, { 174,  13 }, { 174,   0 }, { 191,   3 }, { 191,  20 }, { 191,  37 }, { 208,  42 },
    { 208,  24 }, { 208,   7 }, { 224,   7 }, { 224,  24 }, { 224,  41 }
}, {
    2, 2, 2, 2, 2, 2, 1,
    4, 4, 4, 4, 4, 4, 1,
    1, 4, 4, 4, 4, 4, 4,
    4, 4, 4, 1, 1, 1, 2,
    2, 2, 2, 2, 2, 1, 4,
    4, 4, 4, 4, 4, 1, 1,
    4, 4, 4, 4, 4, 4, 4,
    4, 4, 1, 1, 1
} };
    """

    def row_batch(leds):
        result = []
        last_row = None
        for led in leds:
            if led.key[1] != last_row:
                result.append([])
                last_row = led.key[1]
            result[-1].append(led)
        for row in result:
            yield row

    result = "led_config_t g_led_config = { {\n"
    index_array = get_row_col_index_array(leds)
    for row in index_array:
        result += "  {{ {} }},\n".format(",".join([str(i.ix).rjust(5, " ") for i in row]))
    result += "}, {\n"
    for row in row_batch(leds):
        row_string = "  "
        for led in row:
            row_string += "{{{:>3}, {:>3}}}, ".format(int(224 * led.pos.x / 500), int(64 * led.pos.y / 150))
        row_string += "\n"
        result += row_string
    result += "}, {\n"
    for row in row_batch(leds):
        row_string = "  "
        for led in row:
            row_string += "{:>2}, ".format(4 if led.is_key else 2)
        row_string += "\n"
        result += row_string
    result += "}};\n"
    return result


if __name__ == "__main__":
    leds = get_annotated_led_list()
    print(get_qmk_string(leds))
