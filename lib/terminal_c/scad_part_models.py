from solid import (
    polygon,
    square,
    circle,
    translate,
    rotate,
    mirror,
    linear_extrude,
    offset,
    color,
    OpenSCADObject,
    cube,
    cylinder,
    scad_render_to_file,
)

from solid.utils import (
    up,
    down,
    forward,
    back,
    left,
    right,
)

from solid.utils import (
    down
)

from terminal_c import (
    MagicNumbers as m,
    parts,
    geometry,
    Polygon,
    Vector,
)


class RotaryEncoderConfig:
    base_dimension = (14.2, 12.2, 6.5)
    shaft_length = 20
    shaft_base_length = 8
    shaft_diameter = 6
    shaft_base_diameter = 7.8
    flange_dimension = (1.6, 4.2, 5)
    flange_x_offset = 0.5
    flange_z_offset = 3
    segments = 50


class USBPlugCutout:
    class Config:
        nose_height = 3 + 0.4
        nose_width = 7.8
        plug_height = 7 + 0.4
        plug_width = 12
        nose_length = 2
        plug_distance = 1

    def __call__(self):
        c = USBPlugCutout.Config
        return color("red")((
            left(c.nose_width / 2)(back(c.nose_length + 0.01)(
                cube((c.nose_width, c.nose_length * 2, c.nose_height))
            ))
            +
            left(c.plug_width / 2)(back(c.plug_distance + 10)(
                down(c.plug_height / 2 - c.nose_height / 2)(
                    cube((c.plug_width, 10, c.plug_height))
                )
            ))
        ))


class JackCutoutUpwardsOpen:
    class Config:
        inner_diameter = 5.2
        outer_diameter = 7
        lengt_inner_part = 3

    def __call__(self) -> OpenSCADObject:
        c = JackCutoutUpwardsOpen.Config
        return color("red")(
            up(c.inner_diameter / 2)(
                rotate((0, 90, 0))(
                    cylinder(d=c.inner_diameter, h=10, segments=60)
                    +
                    left(10)(
                        back(c.inner_diameter / 2)(
                            cube((10, c.inner_diameter, 10))
                        )
                    )
                )
            )
        )


class JackCutoutDownwardsOpen:
    class Config:
        inner_diameter = 5.2
        outer_diameter = 7
        lengt_inner_part = 3

    def __call__(self) -> OpenSCADObject:
        c = JackCutoutUpwardsOpen.Config
        return color("red")(
            up(c.inner_diameter / 2)(
                rotate((0, 90, 0))(
                    cylinder(d=c.inner_diameter, h=10, segments=60)
                    +
                    right(0)(
                        back(c.inner_diameter / 2)(
                            cube((10, c.inner_diameter, 10))
                        )
                    )
                )
            )
        )


class RotaryEncoder:

    def __init__(self):
        self.c = RotaryEncoderConfig()

    @property
    def cutout_2d(self):
        d2d = m.SCAD.extension_cutout_dimensions[:2]
        return (
            square(d2d, center=True)
        )

    def __call__(self):
        c = self.c
        c_inner = color("LawnGreen")(
            down(0.1)(
                cylinder(d=c.shaft_diameter, h=c.shaft_length + 0.1, segments=c.segments)
            )
        )
        c_outer = color("MediumOrchid")(
            down(0.1)(
                cylinder(d=c.shaft_base_diameter, h=c.shaft_base_length + 0.1, segments=c.segments)
            )
        )
        base = color("DarkSalmon")(
            down(c.base_dimension[2]/2)(
                cube(c.base_dimension, center=True)
            )
        )
        flange = color("MediumVioletRed")(
            back(c.flange_dimension[1]/2)(
                left(c.base_dimension[0]/2 + c.flange_x_offset)(
                    down(c.flange_z_offset)(
                        cube(c.flange_dimension)
                    )
                )
            )
        )
        return up(c.base_dimension[2])(
            c_inner + c_outer + base + flange
        )


class Promic:

    @property
    def cutout_2d(self):
        d2d = m.SCAD.promic_cutout_dimensions[:2]
        return (
            square(d2d, center=True)
        )

    def __call__(self) -> OpenSCADObject:
        h = m.SCAD.promic_cutout_dimensions[-1]
        return (
            linear_extrude(h)(
                self.cutout_2d
            )
        )


class Jack:

    @property
    def cutout_2d(self):
        d2d = m.SCAD.jack_cutout_dimensions[:2]
        h = m.SCAD.jack_cutout_dimensions[2]
        return back(d2d[1] / 2)(
            left(d2d[0])(
                square(d2d)
                +
                right(d2d[0])(
                    #forward((m.SCAD.jack_cutout_dimensions[1] - m.SCAD.jack_cutout_dimensions[2]) / 2)(
                    forward((d2d[1] - h) / 2)(
                            square(h)
                    )
                )
            )
        )

    def __call__(self) -> OpenSCADObject:
        h = m.SCAD.jack_cutout_dimensions[2]
        return linear_extrude(h)(
            self.cutout_2d
        )


class Switch:
    pole_d = 3.9
    pole_h = 2.85
    bottom_block_l = 13.95
    bottom_block_h = 5
    middle_block_l = 15.6
    middle_block_h = 0.8
    upper_block_l = 14
    upper_block_h = 5.8
    upper_block_f = 0.8
    pin_l = 4
    pin_h = 3.6

    def __call__(self) -> OpenSCADObject:
        return (
            down(Switch.pole_h)(
                cylinder(d=Switch.pole_d, h=Switch.pole_h)
            )
            +
            up(Switch.bottom_block_h/2)(
                cube((Switch.bottom_block_l, Switch.bottom_block_l, Switch.bottom_block_h), center=True)
                +
                up(Switch.bottom_block_h/2 + Switch.middle_block_h/2)(
                    cube((Switch.middle_block_l, Switch.middle_block_l, Switch.middle_block_h), center=True)
                    +
                    up(Switch.middle_block_h/2)(
                        linear_extrude(Switch.upper_block_h, scale=Switch.upper_block_f)(
                            square((Switch.upper_block_l, Switch.upper_block_l), center=True)
                        )
                        +
                        up(Switch.upper_block_h + Switch.pin_h/2)(
                            cube((Switch.pin_l, Switch.pin_l, Switch.pin_h), center=True)
                        )
                    )
                )
            )
        )


class SwitchSectionPCBParts2D:
    """
    Cut-out dice for the switch cell PCB parts as seen from below.
    """
    def get(self) -> OpenSCADObject:
        socket = parts.Switch(ref="foo", pos=Vector(0, 0), angle=180)
        return (
            forward(7.4 / 2)(
                right(0.8)(square((12, 7.4), center=True))
            )
            +
            translate((-7.65, 0.4))(
                square((2.6, 6.6), center=True)
            )
            +
            translate((0, -5))(
                square((8.2, 3.6), center=True)
            )
            +
            translate(socket["a"].as_tuple)(
                square(3.5, center=True)
            )
            +
            translate(socket["b"].as_tuple)(
                square(3.5, center=True)
            )
        )


if __name__ == "__main__":
    usb_plug = USBPlugCutout()()
    scad_render_to_file(usb_plug, "usb-plub.scad")

