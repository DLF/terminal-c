import json
import os
from solid import scad_render_to_file, mirror

from terminal_c.configuration.default import Configuration
from terminal_c import geometry, kicad_path, scad_path, scad

outfile = os.path.join(kicad_path, "pcb_layout.json")


def build_pcb_layout_table(board: geometry.Board):
    layout_dict = board.dict()
    with open(outfile, 'w') as f:
        json.dump(layout_dict, f, indent=2)


def build_scad(board: geometry.Board):
    scad_board = scad.Board(board)
    scad_render_to_file(scad_board.layout_test, os.path.join(scad_path, "layout_test.scad"))
    scad_render_to_file(scad_board.big_switch_plate_2d, os.path.join(scad_path, "switch_plate_2d.scad"))
    scad_render_to_file(scad_board.switch_plate_3d, os.path.join(scad_path, "switch_plate_3d.scad"))
    scad_render_to_file(scad_board.model, os.path.join(scad_path, "model.scad"))
    scad_render_to_file(scad_board.big_bottom_plate_3d, os.path.join(scad_path, "bottom_plate_3d.scad"))
    scad_render_to_file(
        scad_board.wall_segment_bottom, os.path.join(scad_path, "wall_bottom_right.scad")
    )
    scad_render_to_file(
        mirror((0, 1, 0))(scad_board.wall_segment_bottom), os.path.join(scad_path, "wall_bottom_left.scad")
    )
    scad_render_to_file(
        scad_board.switch_plate_spacer, os.path.join(scad_path, "switch_plate_spacer_right.scad")
    )
    scad_render_to_file(
        mirror((0, 1, 0))(scad_board.switch_plate_spacer), os.path.join(scad_path, "switch_plate_spacer_left.scad")
    )


if __name__ == "__main__":
    c = Configuration()
    board = geometry.Board(c)
    build_pcb_layout_table(board)
    build_scad(board)
