from __future__ import annotations
from typing import Optional
import math
import os
import pathlib
from enum import Enum

_this_path = pathlib.Path(__file__).parent.resolve()
scad_path = pathlib.Path(os.path.join(_this_path, "../..", "scad")).resolve()
kicad_path = pathlib.Path(os.path.join(_this_path, "../..", "kicad")).resolve()
kicad_libpath = pathlib.Path(os.path.join(kicad_path, "lib")).resolve()


class Layer(Enum):
    back = 1
    front = 2
    cuts = 3


class Sides(Enum):
    left = 1
    right = 2

    @property
    def letter(self):
        if self == Sides.left:
            return 'L'
        else:
            return 'R'


class Placement:
    def __init__(self, ref: str, pos: Vector, angle: float):
        self.ref = ref
        self.pos = pos
        self.angle = angle
        self.relative_coordinates: dict[str, Vector] = {}

    def get_relative(self, element: str) -> Vector:
        return self.relative_coordinates[element].rotate(self.angle)

    def get_absolute(self, element: str) -> Vector:
        return self.get_relative(element) + self.pos

    def __getitem__(self, item: str):
        return self.get_absolute(item)


class Track:
    def __init__(self, a: Vector, b: Vector, net: str, layer: Layer, width: float = None):
        self.a = a
        self.b = b
        self.net = net
        self.layer = layer
        self.width = 0.4 if width is None else width


class TrackPath:
    def __init__(self, vectors: list[Vector], net: str, layer: Layer, width: float = None):
        self.vectors = vectors
        self.net = net
        self.layer = layer
        self.width = width

    @property
    def tracks(self):
        return [
            Track(a, b, self.net, self.layer, self.width)
            for a, b in zip(self.vectors[:-1], self.vectors[1:])
        ]


class Via:
    def __init__(self, pos: Vector, net: str):
        self.pos = pos
        self.net = net


class Vector:

    @staticmethod
    def from_tuple(v: tuple[float, float]) -> Vector:
        return Vector(v[0], v[1])

    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def clone(self) -> Vector:
        return Vector(self.x, self.y)

    def __add__(self, other: Vector) -> Vector:
        return Vector(
            x=self.x + other.x,
            y=self.y + other.y
        )

    def __sub__(self, other: Vector) -> Vector:
        return Vector(
            x=self.x - other.x,
            y=self.y - other.y
        )

    def __mul__(self, factor: float) -> Vector:
        return Vector(
            x=self.x * factor,
            y=self.y * factor
        )

    def __truediv__(self, factor: float) -> Vector:
        return self.__mul__(1/factor)

    def __ne__(self, other):
        if self.x != other.x:
            return True
        if self.y != other.y:
            return True
        return False

    def __eq__(self, other):
        return not self != other

    def __repr__(self):
        return "V({}, {})".format(self.x, self.y)

    def __str__(self):
        return self.__repr__()

    @property
    def as_tuple(self) -> tuple[float, float]:
        return self.x, self.y

    @property
    def unit_vector(self) -> Vector:
        """
        Return vector with same direction but length of 1.
        """
        return self / self.length

    def rotate(self, angle: float) -> Vector:
        """
        Rotates this vector around (0, 0).

        When y is going downwards, this rotates clock-wise.
        Otherwise counter-clockwise.
        """
        p = self
        a = math.radians(angle)
        s = math.sin(a)
        c = math.cos(a)
        xnew = p.x * c - p.y * s
        ynew = p.x * s + p.y * c
        return Vector(xnew, ynew)

    def rotate_around(self, angle: float, center: Vector) -> Vector:
        """
        Rotates this vector around some other point.

        When y is going downwards, this rotates clock-wise.
        Otherwise counter-clockwise.
        """
        p = self - center
        p2 = p.rotate(angle)
        return p2 + center

    def get_radial_point(self, angle, radius) -> Vector:
        """
        Returns a new point, having a defined distance (radius) to this,
        with a certain angle.

        The angle counts clockwise and starts in direction of y (so (0, 1) has an angle 0).
        """
        p = Vector(0, radius)
        p = p.rotate(angle)
        return p + self

    def angle_to(self, v: Vector):
        """
        Returns the angle between this and another vector.

        Results is in [0 .. 180[.
        """
        dot_product = self.x * self.y + v.x * v.y
        angle = dot_product / (self.length * v.length)
        return math.degrees(math.acos(angle))

    @property
    def length(self) -> float:
        return math.sqrt(self.x ** 2 + self.y ** 2)

    @property
    def angle(self):
        """
        This vector's angle to the y-vector (0, 1).

        The returned angle is measures clockwise.

        Results is in [0 ... 360[.
        """
        return math.degrees(
            math.acos(self.y / self.length)
        )


def factor_circular_arc(center: Vector, radius: float, start: float, end: float, segments: int = None) -> list[Vector]:
    if end == start:
        raise AttributeError("start and end are equal")
    if radius <= 0:
        raise AttributeError("radius must be greater than 0")
    end = end % 360.0
    start = start % 360.0
    if start > end:
        start = start - 360
    assert start < end
    if segments is None:
        segments = math.floor(end - start)
    path = [center.get_radial_point(start, radius)]
    seg_size = (end - start) / float(segments)
    for seg_ix in range(segments - 1):
        angle = start + seg_size * (seg_ix + 1)
        path.append(center.get_radial_point(angle, radius))
    path.append(center.get_radial_point(end, radius))
    return path


class Line:
    def __init__(self, a: Vector, b: Vector):
        self.a = a
        self.b = b

    @property
    def as_svg_coordinate_list(self):
        """
        :return: a list [a.x, a.y, b.x, b.y]
        """
        return [self.a.x, self.a.y, self.b.x, self.b.y]

    @property
    def vector(self):
        return self.b - self.a

    @property
    def length(self) -> float:
        return self.vector.length

    def translate(self, v: Vector) -> Line:
        return Line(
            a=self.a + v,
            b=self.b + v
        )

    def waypoint(self, f: float) -> Vector:
        d = self.b - self.a
        return self.a + d * f

    def intersection_with(self, line: Line) -> Vector:
        def det(a: Vector, b: Vector):
            return a.x * b.y - a.y * b.x

        line1 = self
        line2 = line
        xdiff = Vector(line1.a.x - line1.b.x, line2.a.x - line2.b.x)
        ydiff = Vector(line1.a.y - line1.b.y, line2.a.y - line2.b.y)

        div = det(xdiff, ydiff)
        if div == 0:
            raise Exception('lines do not intersect')

        d = Vector(det(line1.a, line1.b), det(line2.a, line2.b))
        x = det(d, xdiff) / div
        y = det(d, ydiff) / div
        return Vector(x, y)

    def mirror_point(self, p: Vector) -> Vector:
        dx = self.b.x - self.a.x
        dy = self.b.y - self.a.y
        a = (dx * dx - dy * dy) / (dx * dx + dy*dy)
        b = 2 * dx * dy / (dx*dx + dy*dy)

        x = a * (p.x - self.a.x) + b * (p.y - self.a.y) + self.a.x
        y = b * (p.x - self.a.x) - a * (p.y - self.a.y) + self.a.y

        return Vector(x, y)

    def mirror(self, line: Line) -> Line:
        return Line(
            a=line.mirror_point(self.a),
            b=line.mirror_point(self.b)
        )

    def shorten(self, a: Optional[float] = None, b: Optional[float] = None, both: Optional[float] = None) -> Line:
        assert not (a is None and b is None and both is None)
        if both is None:
            if a is None:
                a = 0
            if b is None:
                b = 0
        else:
            assert a is None and b is None
            a = both
            b = both
        unit_v = self.vector.unit_vector
        a_v = unit_v * a
        b_v = unit_v * b
        return Line(
            a=self.a + a_v,
            b=self.b - b_v
        )



class VectorPath(list[Vector]):

    def append_relative(self, v: Vector):
        if len(self) == 0:
            raise AttributeError("Relative vectors can only appended when the path contains at least one element")
        self.append(self[-1] + v)

    def mirror(self, line: Line) -> VectorPath:
        result = VectorPath()
        for v in self:
            result.append(line.mirror_point(v))
        return result

    def continue_to_interception(self, intersection_line: Line):
        p = self
        if len(p) < 2:
            raise AttributeError("Each path in a VectorPathGroup must have at least two elements for continuation.")
        last_line = Line(p[-1], p[-2])
        p_i = last_line.intersection_with(intersection_line)
        p.append(p_i)

    @property
    def lines(self):
        return [
            Line(a, b) for a, b in zip(self[0:-1], self[1:])
        ]

    def rotate_around(self, angle: float, position: Vector) -> VectorPath:
        p = VectorPath()
        for v in self:
            p.append(v.rotate_around(angle, position))
        return p
    
    def translate(self, v: Vector) -> VectorPath:
        return VectorPath([
            o + v for o in self
        ])



class VectorPathGroup(list[VectorPath]):

    def continue_to_interception(self, intersection_line: Line):
        for p in self:
            p.continue_to_interception(intersection_line)

    def append_relative_to_all(self, v: Vector):
        for p in self:
            p.append_relative(v)


class Polygon(VectorPath):

    @property
    def lines(self):
        result = []
        for ix in range(len(self) - 1):
            result.append(Line(
                a=self[ix],
                b=self[ix + 1]
            ))
        result.append(Line(
            a=self[-1],
            b=self[0]
        ))
        return result


class DrawPolygon:
    def __init__(self, polygon: Polygon, layer: Layer, width: float):
        self.polygon: Polygon = polygon
        self.layer = layer
        self.width = width


class Arrangement:
    @property
    def placements(self) -> list[Placement]:
        raise NotImplementedError()

    @property
    def vias(self) -> list[Via]:
        raise NotImplementedError()

    @property
    def tracks(self) -> list[Track]:
        raise NotImplementedError()

    @property
    def polygons(self) -> list[DrawPolygon]:
        raise NotImplementedError()

    def dict(self):
        for p in self.placements:
            assert p.ref is not None
            assert len(p.ref) > 0
        result = {
            "placement": [
                {
                    "ref": p.ref,
                    "pos": p.pos.as_tuple,
                    "angle": p.angle
                } for p in self.placements
            ],
            "tracks": [
                {
                    "a": t.a.as_tuple,
                    "b": t.b.as_tuple,
                    "net": t.net,
                    "width": t.width,
                    "layer": t.layer.name
                } for t in self.tracks
            ],
            "vias": [
                {
                    "pos": v.pos.as_tuple,
                    "net": v.net
                } for v in self.vias
            ],
            "polygons": [
                {
                    "layer": p.layer.name,
                    "width": p.width,
                    "segments": [(line.a.as_tuple, line.b.as_tuple) for line in p.polygon.lines]
                } for p in self.polygons
            ]
        }
        return result


class ComposedArrangement(Arrangement):
    def __init__(self):
        self.sub_arrangements: list[Arrangement] = []

    def add(self, arrangement: Arrangement):
        self.sub_arrangements.append(arrangement)

    @property
    def placements(self) -> list[Placement]:
        return [p for sub_arrangement in self.sub_arrangements for p in sub_arrangement.placements]

    @property
    def vias(self) -> list[Via]:
        return [p for sub_arrangement in self.sub_arrangements for p in sub_arrangement.vias]

    @property
    def tracks(self) -> list[Track]:
        return [p for sub_arrangement in self.sub_arrangements for p in sub_arrangement.tracks]

    @property
    def polygons(self) -> list[DrawPolygon]:
        return [p for sub_arrangement in self.sub_arrangements for p in sub_arrangement.polygons]


class MagicNumbers:

    class SCAD:
        extension_cutout_dimensions = (19, 20, 6)
        promic_cutout_dimensions = (19, 34, 6)
        jack_cutout_dimensions = (12, 7, 5.2)

    r = 2.54  # std pin spacing
    x0r = 320  # the mirrored x0 for the right half

    # Switches
    switch_width = 19.05  # mx switch x/y width

    # Diodes
    diode_offset = Vector(7.65, -0.4)  # offset between switch and diode center
    diode_orientation = 270

    # Finger field GND and VCC vias and tracks
    finger_gnd_column_track_y_offset = 5
    finger_gnd_column_track_overlength = 13.5

    # Switch LEDs
    switch_led_offset = Vector(0, 5.05)
    switch_led_orientation = 180

    # Backlight LEDs
    finger_keys_with_back_led = [
        (1, 0), (3, 0), (5, 0), (2, 2), (5, 2)
    ]
    finger_back_led_offset = Vector(0, 9.65)
    back_led_orientation = 0

    # ProMicro
    promic_switch_distance = 2
    promic_dimensions = Vector(18.5, 33.5)
    promic_connector_to_footprint_offset = Vector(0, 18.3)  # USB-connector to footprint origin
    promic_connector_to_pin_offset = Vector(7.62, 3.85)
    promic_y_indent = 1.5

    # Encoder
    encoder_from_promic_connector = Vector(3, 55)

    # Jack
    jack_distance_from_promic = 5
    jack_orientation = 90

    #  Border
    extra_inner_border = 5
    extra_border_1st_thumb_key_outer = 1

    # Screw Holes
    inner_control_screws_to_switch_distance = 4
    outer_control_screws_to_border_distance = 4
    outer_lower_control_screw_extra_y = 5
    upper_inner_to_h8_vector = Vector(-2.7, 5)
    upper_inner_to_h9_vector = Vector(-2.7, 70)
    jack_to_h10_vector = Vector(-20, 0)

    mirror_line = Line(Vector(x0r / 2, 0), Vector(x0r / 2, 1))

    # Wiring
    pin_cols = [1, 2, 3, 6, 5]
    encoder_col = 4
