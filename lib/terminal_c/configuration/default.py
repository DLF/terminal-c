class Configuration:
    column_offsets = [23, 21, 4, 0, 5, 7]
    column_extra_gaps = [0, 2, 0, 2, 0]
    switch_x_width = 19.05
    switch_y_widths = [19.05, 19.05, 19.05]
    thumb_outer_circle_peak_point = (93, 57)
    thumb_outer_circle_radius = 88
    thumb_row1_circle_radius = 67
    thumb_row2_circle_radius = 45
    thumb_inner_circle_radius = 36
    bottom_outline_indenting_depth = 9
    pcb_connection_width_outer = 5
    pcb_connection_width_inner = 5

    # The angle for each of the radial arranged thumb keys in the first thumb row
    thumb_row1_radial_positions = [-7 + 17 * i for i in range(4)]
    # The angle for each of the radial arranged thumb keys in the second thumb row
    thumb_row2_radial_positions = [-7 + 17/2 + i*17*2 for i in range(2)]

    # SCAD
    class SCAD:
        top_plate_thickness = 1.5

        class LayoutTest:
            switch_hole_width = 14.2
            screw_hole_diameter = 2.5

        class Housing:
            screw_head_d = 3.3
            border_width = 4.4
            border_rounding_radius = 1
            screw_distance = 2.4
            screw_hole_diameter = 2.4
            screw_hole_diameter_extended = 2.6
            screw_hole_segments = 40
            pcb_switch_plate_distance = 3.6
            pcb_bottom_plate_distance = 3  # 3.2
            pcb_thickness = 1.55
            switch_plate_thickness = 1.5
            top_plate_thickness = 1.5
            switch_plate_top_plate_distance = 1.6
            cover_plate_thickness = 1.5
            top_plate_cover_plate_distance = 1.6
            bottom_plate_thickness = 2
            controller_cover_cutout_radius = 79
            controller_cover_overlap = 2.4
            wall_to_pcb_clearance = 0.4
            spacer_feet_height = 0.2
            reset_switch_cutout_width = 4.5 + 1 + 1.5

    def __init__(self):
        assert len(self.column_offsets) == 6
        assert len(self.column_offsets) - 1 == len(self.column_extra_gaps)
        assert len(self.switch_y_widths) == 3
        assert \
            self.thumb_inner_circle_radius \
            < self.thumb_row2_circle_radius \
            < self.thumb_row1_circle_radius \
            < self.thumb_outer_circle_radius
        assert len(self.thumb_row1_radial_positions) == 4
        assert len(self.thumb_row2_radial_positions) == 2

    @property
    def number_of_columns(self):
        return len(self.column_offsets)

    @property
    def number_of_rows(self):
        return len(self.switch_y_widths)

    def number_of_keys_in_column(self, column_ix):
        return self.number_of_rows
        # if column_ix >= self.number_extra_row_keys:
        #     return self.number_of_rows
        # else:
        #     return self.number_of_rows + 1

    def row_ref(self, row_ix):
        """
        Returns capital letter for number.
        (1 -> A, 2 -> B, ...)
        :param row_ix: The integer to convert to a character
        :return: The character corresponding to `row_ix`
        """
        if row_ix > self.number_of_rows:
            raise AttributeError('row_ix {} greater than number of rows'.format(self.number_of_rows))
        if row_ix == self.number_of_rows:
            return 'T'
        return chr(ord('@') + row_ix + 1)
