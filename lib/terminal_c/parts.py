from terminal_c import (
    Placement,
    Vector,
    MagicNumbers as m
)


class Switch(Placement):
    def __init__(self, ref: str, pos: Vector, angle: float):
        Placement.__init__(self, ref, pos, angle)
        self.relative_coordinates = {
            "a": Vector(-7, -2.5),
            "b": Vector(5.8, -5),
        }


class SwitchLed(Placement):
    def __init__(self, ref: str, pos: Vector, angle: float):
        Placement.__init__(self, ref, pos, angle)
        self.relative_coordinates = {
            "VCC": Vector(3, 0.7),
            "out": Vector(3, -0.7),
            "in": Vector(-3, 0.7),
            "GND": Vector(-3, -0.7),
        }


class BackLed(Placement):
    def __init__(self, ref: str, pos: Vector, angle: float):
        Placement.__init__(self, ref, pos, angle)
        self.relative_coordinates = {
            "VCC": Vector(-2.8, 1.6),
            "out": Vector(-2.8, -1.6),
            "in": Vector(2.8, 1.6),
            "GND": Vector(2.8, -1.6),
        }


class Diode(Placement):
    relative_coordinates = {
        "a": Vector(1.9, 0),
        "k": Vector(-1.9, 0),
    }

    def __init__(self, ref: str, pos: Vector, angle: float):
        Placement.__init__(self, ref, pos, angle)
        self.relative_coordinates = Diode.relative_coordinates


class Promic(Placement):
    d_x = m.r * 3
    d_y = m.r
    o_y = 0.762
    y0 = -6 * m.r + o_y
    pins_left = [
        "raw",
        "GND3",
        "rst",
        "VCC",
        "f4",
        "f5",
        "f6",
        "f7",
        "b1",
        "b3",
        "b2",
        "b6",
    ]
    pins_right = [
        "d3",
        "d2",
        "GND1",
        "GND2",
        "d1",
        "d0",
        "d4",
        "c6",
        "d7",
        "e6",
        "b4",
        "b5",
    ]

    def __init__(self, ref: str, pos: Vector, angle: float):
        Placement.__init__(self, ref, pos, angle)
        self.relative_coordinates = {}
        assert len(self.pins_right) == len(self.pins_left) == 12
        for ix in range(12):
            self.relative_coordinates[self.pins_left[ix]] = Vector(-self.d_x, self.y0 + ix * self.d_y)
            self.relative_coordinates[self.pins_right[ix]] = Vector(self.d_x, self.y0 + ix * self.d_y)


class Encoder(Placement):
    def __init__(self, ref: str, pos: Vector, angle: float):
        Placement.__init__(self, ref, pos, angle)
        self.relative_coordinates = {
            "sw1": Vector(7, m.r),
            "sw2": Vector(7, -m.r),
            "left": Vector(-7.5, m.r),
            "right": Vector(-7.5, -m.r),
            "GND": Vector(-7.5, 0),
        }


class Jack(Placement):
    def __init__(self, ref: str, pos: Vector, angle: float):
        Placement.__init__(self, ref, pos, angle)
        x = 3 * m.r
        self.relative_coordinates = {
            "2-1": Vector(-3, 3.5),
            "2-2": Vector(3, 3.5),
            "1": Vector(-2.2, 11),
            "2": Vector(2.2, 11),
        }


class SMDSwitch(Placement):
    def __init__(self, ref: str, pos: Vector, angle: float):
        Placement.__init__(self, ref, pos, angle)
        self.relative_coordinates = {
            "1L": Vector(-3.6, -1.7),
            "2L": Vector(-3.6, 1.7),
            "1R": Vector(3.6, -1.7),
            "2R": Vector(3.6, 1.7),
        }


class Pin(Placement):
    pass
