# needs kinet2pcb, skidl
from __future__ import annotations

import os
from terminal_c import MagicNumbers as m
from terminal_c import Vector

if "KICAD_SYMBOL_DIR" not in os.environ:
    os.environ["KICAD_SYMBOL_DIR"] = "/usr/share/kicad/library"

import skidl as sk

from terminal_c.configuration.default import Configuration
from terminal_c import Sides, kicad_path, kicad_libpath


class Switch:
    lib = 'terminal-c'
    name = 'SW_PUSH'
    footprint = 'terminal-c:CherryMX_Hotswap'


class Diode:
    lib = 'terminal-c'
    name = 'D'
    footprint = 'terminal-c:D3_SMD_v2'
    

class SwitchLED:
    lib = 'terminal-c'
    name = 'SK6812MINI-E'
    footprint = 'terminal-c:SK6812MINI-E'
    

class BackLED:
    lib = 'terminal-c'
    name = 'WS2812B'
    footprint = 'terminal-c:WS2812B'
    
    
class Promic:
    lib = 'terminal-c'
    name = 'ProMicro'
    footprint = 'terminal-c:ProMicro_v3'
    

class RotaryEncoder:
    lib = 'terminal-c'
    name = 'RotaryEncoder_Switch'
    footprint = 'terminal-c:RotaryEncoder_Alps_EC11E_Switch_Vertical'


class Jack:
    lib = 'terminal-c'
    name = 'AudioJack2_Ground'
    footprint = 'terminal-c:1503_07'


class SMDSwitch:
    lib = 'terminal-c'
    name = 'SW_PUSH'
    footprint = 'terminal-c:SW_4.5_left_only'


class Hole:
    lib = 'terminal-c'
    name = 'blind_symbol'
    footprint = 'terminal-c:mounting_hole_m2'


class Pin:
    lib = 'terminal-c'
    name = 'Conn_01x01_MountingPin'
    footprint = 'terminal-c:PinHeader'


class Logo:
    lib = 'terminal-c'
    name = 'blind_symbol'
    footprint = 'terminal-c:terminal-c-logo'


sk.lib_search_paths[sk.KICAD].append(str(kicad_libpath))


def make_part(part, ref=None):
    part = sk.Part(part.lib, part.name, footprint=part.footprint)
    if ref is not None:
        part.ref = ref
    return part


class LEDMeta:
    def __init__(self, is_key: bool, key: (int, int)):
        self.is_key = is_key
        self.key = key
        self.pos: Vector = None
        self.ix: int = None

    def __str__(self):
        return "{}-{}-({},{}) - ({},{})".format(
            "K" if self.is_key else "B",
            self.ix,
            self.key[0],
            self.key[1],
            self.pos.x,
            self.pos.y
        )

    def __repr__(self):
        return str(self)


class Board:

    def __init__(self, c: Configuration, side: Sides):
        self.c = c
        self.side = side
        self.led_list: list[LEDMeta] = []
        self.nc = sk.NCNet()
        self.column_nets = [
            sk.Net(
                'Col{s}{i}'.format(s=self.side.letter, i=ix)
            ) for ix in range(c.number_of_columns)
        ]
        self.row_nets = [
            sk.Net(
                'Row{s}{i}'.format(s=self.side.letter, i=self.c.row_ref(ix))
            ) for ix in range(c.number_of_rows + 1)
        ]
        self.gnd_net = sk.Net('{}GND'.format(self.side.letter))
        self.vcc_net = sk.Net('{}VCC'.format(self.side.letter))
        self.led_pin_net = sk.Net('{}LED'.format(self.side.letter))
        self._make_and_wire_switches()
        self._make_and_wire_switch_leds()
        self.promic = make_part(Promic, "{s}Promic".format(s=self.side.letter))
        self.promic["VCC"].drive = sk.POWER
        self.gnd_net.drive = sk.POWER
        self.jack = make_part(Jack, "{s}Jack".format(s=self.side.letter))
        self.encoder = make_part(RotaryEncoder, "{s}Encoder".format(s=self.side.letter))
        self.thumb_back_led = make_part(BackLED, "{s}Bt".format(s=self.side.letter))
        self.reset_switch = make_part(SMDSwitch, "{s}RstSw".format(s=self.side.letter))
        self.holes = [make_part(Hole, "{s}H{ix}".format(s=self.side.letter, ix=ix)) for ix in range(11)]
        self._wire_thumb_led()
        self._wire_promic()
        self._make_and_wire_extension_pins()
        self._wire_encoder()
        self._wire_jack()
        self.led_list.append(LEDMeta(is_key=False, key=(0, 0))) #  Dummy for the thumg back LED
        self.logo = make_part(Logo, "{s}Logo".format(s=self.side.letter))
        self.logo.value = "V 1.0"

    def _wire_jack(self):
        self.serial_net = sk.Net('{}Ser'.format(self.side.letter))
        self.serial_net += self.promic["TX0/D3"]
        self.serial_net += self.jack["S"]
        self.gnd_net += self.jack["T"]
        self.vcc_net += self.jack["G"]

    def _wire_encoder(self):
        # switch
        self.column_nets[m.encoder_col - 1] += self.encoder["S2" if self.side == Sides.left else "S1"]
        b5net = sk.Net('{}PB5'.format(self.side.letter))
        b5net += self.encoder["S1" if self.side == Sides.left else "S2"]
        b5net += self.promic["9/B5"]
        # encoder
        self.gnd_net += self.encoder["C"]
        b6net = sk.Net("{}PB6".format(self.side.letter))
        b6net += self.encoder["A" if self.side == Sides.left else "B"]
        b6net += self.promic["B6/10"]
        d2net = sk.Net("{}PD2".format(self.side.letter))
        d2net += self.encoder["B" if self.side == Sides.left else "A"]
        d2net += self.promic["RX1/D2"]
        # MP
        self.nc += self.encoder["MP"]

    def _make_and_wire_extension_pins(self):
        pin_cols = m.pin_cols
        self.column_pins = [make_part(Pin, "{s}C{c}Pin".format(s=self.side.letter, c=c)) for c in pin_cols]
        for ix, c_ix in enumerate(pin_cols):
            self.column_nets[c_ix - 1] += self.column_pins[ix]
        self.free_pins = []
        for name, pin in [
            ("PD1", "2/D1/SDA"),
            ("PD0", "3/D0/SCL"),
            ("PC6", "5/C6"),
        ]:
            name = "{s}{n}".format(s=self.side.letter, n=name)
            self.free_pins.append(make_part(Pin, name))
            net = sk.Net(name)
            net += self.promic[pin]
            net += self.free_pins[-1]
        self.vcc_pin = make_part(Pin, "{}VCC".format(self.side.letter))
        self.vcc_net += self.vcc_pin

    def _wire_promic(self):
        """
        Encoder is wired to promic in `_wire_encoder`.

        Jack is wired to promic in `_wire_jack`.

        Reset switch is wired in here.
        """
        # GND and VCC
        self.gnd_net += self.promic[3]
        self.gnd_net += self.promic[4]
        self.gnd_net += self.promic[23]
        self.vcc_net += self.promic[21]
        # Columns
        col_pins = [
            "B2/16",
            "B3/14",
            "B1/15",
            "F7/A0",
            "F6/A1",
            "F5/A2",
        ]
        if self.side == Sides.left:
            col_pins.reverse()
        for net, pin in zip(self.column_nets, col_pins):
            net += self.promic[pin]
        # Rows
        row_pins = [
            "4/D4",
            "6/D7",
            "7/E6",
            "8/B4",
        ]
        for net, pin in zip(self.row_nets, row_pins):
            net += self.promic[pin]
        # LEDs
        self.led_pin_net += self.promic["F4/A3"]
        self.nc += self.promic["RAW"]
        # Reset Switch
        rst_net = sk.Net("{s}Rst".format(s=self.side.letter))
        rst_net += self.promic["RST"]
        rst_net += self.reset_switch["1"]
        self.gnd_net += self.reset_switch["2"]

    def _wire_thumb_led(self):
        self.gnd_net += self.thumb_back_led[3]
        self.vcc_net += self.thumb_back_led[1]
        self.last_led_signal_out_net += self.thumb_back_led[4]
        self.nc += self.thumb_back_led[2]

    def _make_and_wire_switch_leds(self):
        last_net = self.led_pin_net
        for row_ix in range(self.c.number_of_rows + 1):
            for col_ix in range(self.c.number_of_columns):
                # reversed col_ix for un-even rows
                patched_col_ix = col_ix if row_ix % 2 == 1 else self.c.number_of_columns - 1 - col_ix
                led = make_part(SwitchLED)
                ref = "{s}L{c}{r}".format(
                    s=self.side.letter,
                    c=patched_col_ix,
                    r=self.c.row_ref(row_ix)
                )
                led.ref = ref
                self.vcc_net += led[1]
                self.gnd_net += led[3]
                last_net += led[4]
                last_net = sk.Net(ref + "out")
                last_net += led[2]
                self.led_list.append(LEDMeta(is_key=True, key=(patched_col_ix, row_ix)))

                if (patched_col_ix, row_ix) in m.finger_keys_with_back_led:
                    back_ref = "{s}B{c}{r}".format(
                        s=self.side.letter,
                        c=patched_col_ix,
                        r=self.c.row_ref(row_ix)
                    )
                    back_led = make_part(BackLED, back_ref)
                    self.vcc_net += back_led[1]
                    self.gnd_net += back_led[3]
                    last_net += back_led[4]
                    last_net = sk.Net(back_ref + "out")
                    last_net += back_led[2]
                    self.led_list.append(LEDMeta(is_key=False, key=(patched_col_ix, row_ix)))
        self.last_led_signal_out_net = last_net

    def _make_and_wire_switches(self):
        for col_ix in range(self.c.number_of_columns):
            col_net = self.column_nets[col_ix]
            for row_ix in range(self.c.number_of_rows + 1):
                row_net = self.row_nets[row_ix]
                s = sk.Part(Switch.lib, Switch.name, footprint=Switch.footprint)
                d = sk.Part(Diode.lib, Diode.name, footprint=Diode.footprint)
                sd_net = sk.Net('SD{s}{c}{r}'.format(
                    s=self.side.letter,
                    c=col_ix,
                    r=self.c.row_ref(row_ix)
                ))
                sd_net += s[2]
                sd_net += d['A']
                s.ref = "{s}S{c}{r}".format(
                    s=self.side.letter,
                    c=col_ix,
                    r=self.c.row_ref(row_ix)
                )
                d.ref = "{s}D{c}{r}".format(
                    s=self.side.letter,
                    c=col_ix,
                    r=self.c.row_ref(row_ix)
                )
                col_net += s[1]
                row_net += d['K']


def get_boards(c: Configuration) -> (Board, Board):
    left_board = Board(c, Sides.left)
    right_board = Board(c, Sides.right)
    return left_board, right_board


def build_schematics(c: Configuration):
    _left_board, _right_board = get_boards(c)
    sk.ERC()
    sk.generate_netlist(file_=os.path.join(str(kicad_path), "terminal_c.out.net"))


if __name__ == "__main__":
    conf = Configuration()
    build_schematics(conf)
