EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR0101
U 1 1 618EF6F9
P 850 1300
F 0 "#PWR0101" H 850 1050 50  0001 C CNN
F 1 "GND" V 855 1172 50  0000 R CNN
F 2 "" H 850 1300 50  0001 C CNN
F 3 "" H 850 1300 50  0001 C CNN
	1    850  1300
	0    1    1    0   
$EndComp
Wire Wire Line
	1100 1300 950  1300
Wire Wire Line
	1100 1400 950  1400
Wire Wire Line
	950  1400 950  1300
Connection ~ 950  1300
Wire Wire Line
	950  1300 850  1300
$Comp
L power:GND #PWR0102
U 1 1 618F06CC
P 2900 1200
F 0 "#PWR0102" H 2900 950 50  0001 C CNN
F 1 "GND" V 2905 1072 50  0000 R CNN
F 2 "" H 2900 1200 50  0001 C CNN
F 3 "" H 2900 1200 50  0001 C CNN
	1    2900 1200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2900 1200 2500 1200
$Comp
L power:VCC #PWR0103
U 1 1 618F0E4E
P 2900 1400
F 0 "#PWR0103" H 2900 1250 50  0001 C CNN
F 1 "VCC" V 2915 1528 50  0000 L CNN
F 2 "" H 2900 1400 50  0001 C CNN
F 3 "" H 2900 1400 50  0001 C CNN
	1    2900 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	2900 1400 2500 1400
Text GLabel 2600 1300 2    50   Input ~ 0
L_RST
Wire Wire Line
	2600 1300 2500 1300
Text GLabel 2600 1500 2    50   Input ~ 0
L_C0
Text GLabel 2900 1600 2    50   Input ~ 0
L_C1
Text GLabel 2600 1700 2    50   Input ~ 0
L_C2
Text GLabel 2900 1800 2    50   Input ~ 0
L_C3
Text GLabel 2600 1900 2    50   Input ~ 0
L_C4
Wire Wire Line
	2600 1500 2500 1500
Wire Wire Line
	2900 1600 2500 1600
Wire Wire Line
	2600 1700 2500 1700
Wire Wire Line
	2900 1800 2500 1800
Wire Wire Line
	2600 1900 2500 1900
Text GLabel 750  1700 0    50   Input ~ 0
L_R1
Text GLabel 1000 1800 0    50   Input ~ 0
L_R2
Text GLabel 750  1900 0    50   Input ~ 0
L_R3
Text GLabel 1000 2000 0    50   Input ~ 0
L_R4
Text GLabel 750  2100 0    50   Input ~ 0
L_R5
Text GLabel 1000 2200 0    50   Input ~ 0
L_R6
Wire Wire Line
	1000 2200 1100 2200
Wire Wire Line
	750  2100 1100 2100
Wire Wire Line
	1000 2000 1100 2000
Wire Wire Line
	750  1900 1100 1900
Wire Wire Line
	1000 1800 1100 1800
Wire Wire Line
	750  1700 1100 1700
Text GLabel 2600 2100 2    50   Input ~ 0
L_E1
Text GLabel 2900 2200 2    50   Input ~ 0
L_E2
Wire Wire Line
	2600 2100 2500 2100
Wire Wire Line
	2900 2200 2500 2200
Text GLabel 2900 2000 2    50   Input ~ 0
L_DATA
Wire Wire Line
	2900 2000 2500 2000
NoConn ~ 2500 1100
$Comp
L Switch:SW_Push L_btn_rst1
U 1 1 618F904B
P 1150 2850
F 0 "L_btn_rst1" H 1150 3135 50  0000 C CNN
F 1 "Reset" H 1150 3044 50  0000 C CNN
F 2 "" H 1150 3050 50  0001 C CNN
F 3 "~" H 1150 3050 50  0001 C CNN
	1    1150 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 618F9D66
P 800 2850
F 0 "#PWR0104" H 800 2600 50  0001 C CNN
F 1 "GND" V 805 2722 50  0000 R CNN
F 2 "" H 800 2850 50  0001 C CNN
F 3 "" H 800 2850 50  0001 C CNN
	1    800  2850
	0    1    1    0   
$EndComp
Text GLabel 1500 2850 2    50   Input ~ 0
L_RST
Wire Wire Line
	950  2850 800  2850
Wire Wire Line
	1350 2850 1450 2850
$Comp
L Device:R R1
U 1 1 61923307
P 1600 3350
F 0 "R1" H 1670 3396 50  0000 L CNN
F 1 "R" H 1670 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_2512_6332Metric" V 1530 3350 50  0001 C CNN
F 3 "~" H 1600 3350 50  0001 C CNN
	1    1600 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 3200 1450 3200
Wire Wire Line
	1450 3200 1450 2850
Connection ~ 1450 2850
Wire Wire Line
	1450 2850 1500 2850
$Comp
L power:VCC #PWR0105
U 1 1 619248ED
P 1700 3850
F 0 "#PWR0105" H 1700 3700 50  0001 C CNN
F 1 "VCC" H 1715 4023 50  0000 C CNN
F 2 "" H 1700 3850 50  0001 C CNN
F 3 "" H 1700 3850 50  0001 C CNN
	1    1700 3850
	-1   0    0    1   
$EndComp
Wire Wire Line
	1600 3500 1600 3850
Wire Wire Line
	1600 3850 1700 3850
$Comp
L Device:Rotary_Encoder_Switch SW?
U 1 1 61CEE8CA
P 3950 4700
F 0 "SW?" H 3950 5067 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 3950 4976 50  0000 C CNN
F 2 "" H 3800 4860 50  0001 C CNN
F 3 "~" H 3950 4960 50  0001 C CNN
	1    3950 4700
	1    0    0    -1  
$EndComp
$Comp
L terminal-c:1503_07 X?
U 1 1 61CEDD62
P 4850 4050
F 0 "X?" H 5136 4030 50  0000 L CNN
F 1 "1503_07" H 5136 3939 50  0000 L CNN
F 2 "" H 4850 4050 50  0001 C CNN
F 3 "" H 4850 4050 50  0001 L BNN
	1    4850 4050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
