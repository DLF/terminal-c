from pcbnew import *
import json
import os
import sys

x_offset = 14
y_offset = 14


class Layouter:

    layer_name_mapping = {
        "back": "B.Cu",
        "front": "F.Cu",
        "cuts": "Edge.Cuts",
    }

    def __init__(self):
        self.layer_name_to_id = {}
        self.net_name_to_net = {}
        self.layout = None
        self.b = None
        self.back_layer = None
        self.front_layer = None
        self.tracks = []
        self.vias = []
        self.tracks_by_net_name = {}
        self.vias_by_net_name = {}

    def init(self):
        b = GetBoard()
        filename = b.GetFileName()
        print(filename)
        pathparts = filename.split(os.path.sep)
        base_ix = None
        expected_file = None
        for ix in range(len(pathparts)):
            if pathparts[ix] == "kicad":
                print("found kicad dir!")
                expected_file = os.path.join(*(pathparts[:ix + 1] + ["pcb_layout.json"]))
                if pathparts[0] == '':
                    expected_file = os.path.sep + expected_file
                print(expected_file)
                if os.path.exists(expected_file):
                    base_ix = ix
                    break
        if base_ix is None:
            print("pcb_layout.json not found in a parent directory with name kicad. Aborting.")
            return
        json_path = expected_file

        print("Using layout JSON file: " + json_path)

        numlayers = PCB_LAYER_ID_COUNT
        self.layer_name_to_id = {}
        for i in range(numlayers):
            layer_name = b.GetLayerName(i)
            assert layer_name not in self.layer_name_to_id
            self.layer_name_to_id[layer_name] = i

        self.net_name_to_net = {}
        netinfo = b.GetNetInfo()
        for ix in range(netinfo.GetNetCount()):
            ni = netinfo.GetNetItem(ix)
            net_name = ni.GetNetname()
            assert net_name not in self.net_name_to_net
            self.net_name_to_net[net_name] = ni

        with open(json_path) as f:
            self.layout = json.load(f)

        self.b = b
        self.back_layer = self.layer_name_to_id["B.Cu"]
        self.front_layer = self.layer_name_to_id["F.Cu"]
        self.update_tracks_and_vias()

    def update_tracks_and_vias(self):
        self.tracks = []
        self.vias = []
        self.tracks_by_net_name = {}
        self.vias_by_net_name = {}
        for t in self.b.GetTracks():
            net_name = t.GetNet().GetNetname()
            if t.GetClass() == 'PCB_TRACK':
                self.tracks.append(t)
                mapping = self.tracks_by_net_name
            else:
                print(t.GetClass())
                assert t.GetClass() == 'PCB_VIA'
                self.vias.append(t)
                mapping = self.vias_by_net_name
            if net_name not in mapping:
                mapping[net_name] = []
            assert t not in mapping[net_name]
            mapping[net_name].append(t)

    def delete_tracks_and_vias(self):
        for t in self.tracks:
            self.b.Remove(t)
        for v in self.vias:
            self.b.Remove(v)
        self.update_tracks_and_vias()

    def delete_drawings(self):
        for d in self.b.GetDrawings():
            self.b.Remove(d)

    def place_modules(self):
        for e in self.layout['placement']:
            module = self.b.FindFootprintByReference(e['ref'])
            assert module is not None
            coord = e['pos']
            module.SetPosition(wxPoint(FromMM(coord[0] + x_offset), FromMM(coord[1] + y_offset)))
            module.SetOrientation(-e['angle'] * 10)

    def add_vias(self):
        for v in self.layout["vias"]:
            via = PCB_VIA(self.b)
            self.b.Add(via)
            via.SetViaType(VIATYPE_THROUGH)
            via.SetBottomLayer(self.back_layer)
            via.SetTopLayer(self.front_layer)
            via.SetDrill(400000)
            via.SetWidth(800000)
            net = self.net_name_to_net[v["net"]]
            via.SetNet(net)
            coord = v["pos"]
            via.SetPosition(wxPoint(FromMM(coord[0] + x_offset), FromMM(coord[1] + y_offset)))

    def add_tracks(self):
        for t in self.layout["tracks"]:
            a = t["a"]
            b = t["b"]
            assert t["layer"] in ["back", "front"]
            track = PCB_TRACK(self.b)
            track.SetStart(wxPoint(
                FromMM(a[0] + x_offset),
                FromMM(a[1] + y_offset)
            ))
            track.SetEnd(wxPoint(
                FromMM(b[0] + x_offset),
                FromMM(b[1] + y_offset)
            ))
            track.SetWidth(FromMM(t["width"]))
            track.SetLayer(
                self.back_layer if t["layer"] == "back" else self.front_layer
            )
            self.b.Add(track)
            net = self.net_name_to_net[t["net"]]
            track.SetNet(net)

    def add_draw_segments(self):
        for p in self.layout["polygons"]:
            layer = self.layer_name_to_id[
                Layouter.layer_name_mapping[p["layer"]]
            ]
            width = p["width"]
            print("Adding Polygon to layer " + str(layer))
            for s in p["segments"]:
                a = s[0]
                b = s[1]
                print("  adding segment " + str(s))
                #edge = DRAWSEGMENT(self.b)
                edge = PCB_SHAPE(self.b)
                #segment.SetShape(STROKE_T.S_SEGMENT)
                self.b.Add(edge)
                edge.SetStart(wxPoint(
                    FromMM(a[0] + x_offset),
                    FromMM(a[1] + y_offset)
                ))
                edge.SetEnd(wxPoint(
                    FromMM(b[0] + x_offset),
                    FromMM(b[1] + y_offset)
                ))
                edge.SetLayer(layer)
                edge.SetWidth(FromMM(width))


class LayoutTerminalCPlugin(ActionPlugin, Layouter):
    def __init__(self, *args, **kwargs):
        ActionPlugin.__init__(self, *args, **kwargs)
        Layouter.__init__(self)

    def defaults(self):
        self.name = 'Layout Terminal C'
        self.category = 'Terminal C'
        self.description = 'Lays out a Terminal-C board accroding to the last base build'

    def Run(self):
        print(sys.version)
        print(os.getcwd())
        self.init()
        self.delete_tracks_and_vias()
        self.delete_drawings()
        self.place_modules()
        self.add_vias()
        self.add_tracks()
        self.add_draw_segments()


if __name__ == "plugin":
    LayoutTerminalCPlugin().register()


"""
v = pn.VIA(b)
b.Add(v)
v.SetViaType(pn.VIA_THROUGH)
v.SetBottomLayer(31) # back
v.SetTopLayer(0) # front
v.SetDrill(400000)
v.SetWidth(800000)
...set net
...set position

exec(open("/home/daniel/sandbox/keyboard/terminal-c/pcbnew_layout_terminal_c_plugin.py").read())
l = Layouter()
l.init()
"""