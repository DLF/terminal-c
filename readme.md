# Terminal-C – Just another Split Keyboard

This is work in progress and still in an experimental phase!

Terminal-C uses a lot of technical concepts and footprints from the
[Corne keyboard](https://github.com/foostan/crkbd).
The physical layout is coming from my former project,
[Terminal-B](https://gitlab.com/DLF/terminal-b),
which is a fully 3D printable, hand-wired split.

The most significant differences to the Corne in regards to the physical layout are:
* More aggressive stagger
* Some little extra space between the 2nd and 3rd, and the 4th and 5th column
* A bigger thumb cluster with 6 keys, arranged on two radial arcs

# KiCad Project
The project is developed with KiCad 6.

As this is in progress, one needs to clone a
[certain commit from the `kbd` project](https://github.com/foostan/kbd/tree/f90510c11b6df53bcdb55740b0d59183957e4036)
into this project’s directory `kicad/lib`.
This project also still depends on some symbols and footprints from KiCad’s standard library.

At the end, this project should be self-contained in regards to the KiCad libs.

# Build
The PCB is not designed visually but by code.

To update the PCB, an action script is used. If the net-list changes, the pcbnew-project must
be cleared (select all and delete/cut), and the net-list is re-imported.

In KiCad 6, add the file `pcbnew_layout_terminal_c_plugin.py` to the action plugins.

Have Python >= 3.9 (earlier might also work) installed with these libs from PyPi:
* `skidl` (creates net-lists)
* `solidpython` (creates OpenSCAD code)

Have the directory `lib/terminal-c` in your `PYTHONPATH`.

You may, of course, also setup a virtual environment and/or use an IDE for this stuff.

Then run `lib/terminal_c/schematic.py` to create the net-list and
`lib/terminal_c/pcb.py` to create a JSON file with the PCB layout information and some
SCAD code for other solid parts.

Now, if the pcbnew project has been cleared, import the generated net-list `terminal_c.out.net`.
If only the layout changes, this can be skipped.

Then run the action plugin which takes the information from the generated `pcb_layout.json`,
deletes all tracks, outlines and vias which may exist, arranges the footprints and re-creates
all tracks, vias and outlines.

The build process and project structure is also in an early stage and will change.

# Notes
* Use isolation tape on the promic's usb port up-side
